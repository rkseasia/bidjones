//
//  ProductData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/9/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

class ProductData {
    var itemType:Int?
    var sellerId:Int?
    var itemId:Int?
    var albumName:String?
    var artistName:String?
    var title:String?
    var noOfPages:String?
    var genre:String?
    var typeOfBook:String?
    var serviceType:String?
    var musicNames:[String]?
    var location:String?
    var genreId:Int?
    var is_escrow:Bool?
    var description:String?
    var bookUrl:String?
    var TC:String?
    var duration:String?
    var minBidAmount:Double?
    var lat:Float?
    var lng:Float?
    var minBidType:String?
    var minBidTypeId:Int?
    var bidAllowed:Int?
    var rating:Double?
    var keySearchWord:String?
    var createdDate:String?
    var arrImages:[ImagesData]?
    var arrWatermarkImages:[ImagesData]?
    var arrVideos:[VideoData]?
    var arrVideosDiscript:[VideoData]?
    var arrAudios:[AudioData]?
    var arrPdf:[PdfData]?
    var subCategoryName : String?
    var isEscrowShowButton : Int?
    var isTradedProduct : Int?
    var tradedAmount : Double?
    var status : Int?




    init?(json : [String:Any])
    {
        print(json)
       // Reinitilize()
        
        if let isEscrow = json["escrow_service"] as? Int {
            
            print("here is your Escrow : \(isEscrow)")
            self.isEscrowShowButton = isEscrow
        }
        
        if let sellerID = json["seller_id"] as? Int
        {
            sellerId = sellerID
        }
        if let itemID = json["id"] as? Int
        {
            itemId = itemID
        }
        if let Rating = json["avgrating"] as? Double
        {
            rating = Rating
        }
        if let categoryID = json["category_id"] as? Int
        {
            itemType = categoryID
        }
        if let genre = json["category_meta_id"] as? Int
        {
            genreId = genre
        }
        if let Genre = json["category_meta_name"] as? String, Genre != ""
        {
                genre = Genre

//            if(itemType == 1)
//            {
//            typeOfBook = TypeOfBook
//            }
//            else
//            {
            //}
        }
        if let Description = json["description"] as? String
        {
            description = Description
        }
      if let tradeItem = json["is_trade_item"] as? Int
      {
        isTradedProduct = tradeItem
      }
        if let keyW = json["keywords"] as? String
        {
            keySearchWord = keyW
        }
        if let latt = json["lat"]
        {
            lat = (latt as? Float)
        }
        if let long = json["lng"]
        {
            lng = (long as? Float)
        }
      if let latt = json["lat"] as? NSNumber
      {
        lat = Float(truncating: latt)
      }
      if let long = json["lng"] as? NSNumber
      {
        lng = Float(truncating: long)
      }
        if let Title = json["title"]
        {
            title = (Title as? String)
        }
        if let subcategory = json["service_type"] as? String
        {
//            if let name = subcategory["name"] as? String
//            {
//             serviceType = subcategory
//            }
            serviceType = subcategory

        }
        
        if let subcategoryName = json["subcategory"] as? [String : Any] {
            
            if let name = subcategoryName["name"]  as? String {
                
                print("you type name is : \(name)")
                self.subCategoryName = name
                
                
            }
            
            
        }
        
        if let bidTypeID = json["bid_type_id"] as? Int
        {
            minBidTypeId = bidTypeID
        }
        if let bidType = json["bid_type_name"] as? String
        {
            minBidType = bidType
        }
        if let BidAllowed = json["bid_allowed"] as? Int
        {
            bidAllowed = BidAllowed
        }
        if let minbidAmount = json["min_bid"] as? String
        {
            print(minbidAmount as Any)
            minBidAmount = Double(minbidAmount)
            print(minBidAmount as Any)
        }
        if let isEscrow = json["escrow_service"] as? Bool
        {
            is_escrow = isEscrow
        }
        if let Created_at = json["created_at"] as? String
        {
            createdDate = Created_at
        }
      if let Status = json["status"] as? Int
      {
        status = Status
      }
        if let meta = json["meta"] as? [String:Any]
        {
            if let Toc = meta["toc"] as? String
            {
                TC = Toc
            }
            if let Album_name = meta["album_name"] as? String
            {
                albumName = Album_name
            }
            if let Artist_name = meta["singer"] as? String
            {
                //print(artistName)
                artistName = Artist_name
            }
            if let Duration = meta["duration"] as? String
            {
                duration = Duration
            }
            if let total_pages = meta["total_pages"] as? String
            {
                noOfPages = total_pages
            }
            if let url = meta["url"] as? String
            {
                bookUrl = url
            }
            if let Location = json["address"] as? String
            {
                location = Location
            }
            if let imagesArr = meta["images"] as? [String]
            {
                arrImages = [ImagesData]()
                for item in imagesArr
                {
                    print(item)
                    let url = URL(string: item)
                    if let data = ImagesData(ImgUrl: url , Image: nil){
                        arrImages?.append(data)
                    }
                }
            }
            if let imagesWatermarkArr = meta["preview_images"] as? [String]
            {
                print(imagesWatermarkArr)
                arrWatermarkImages = [ImagesData]()
                for item in imagesWatermarkArr
                {
                    let url = URL(string: item)
                    if let data = ImagesData(ImgUrl: url , Image: nil){
                        arrWatermarkImages?.append(data)
                    }
                }
            }
            if let musicArr = meta["music"] as? [String]
            {
               if let musicNameArr = meta["music_name"] as? [String]
               {
                arrAudios = [AudioData]()
                musicNames = [String]()
                var index = 0
                for item in musicArr
                {
                    
                   if musicNameArr.count>index
                   {
                    let Title = musicNameArr[index]
                    let AlbumTitle = musicNameArr[index]
                    let url = URL(string: item)
                    if let data = AudioData(Title: Title, AudioURL: url , AlbumTitle: AlbumTitle, Artist: nil, AssetUrl: nil, IsRadio: false)
                     {
                        arrAudios?.append(data)
                        musicNames?.append(AlbumTitle)

                    }
                    index = index+1
                }
                }
                }
            }
            if let item = meta["videos"] as? String
            {
                arrVideos = [VideoData]()
                let url = URL(string: item)
                if let data = VideoData(VideoUrl: url, Image: nil)
                {
                    arrVideos?.append(data)
                }
               // arrVideos?.append(urlStr)
            }
            
            if let item1 = meta["videoDescript"] as? String
            {
                arrVideosDiscript = [VideoData]()
                let url = URL(string: item1)
                if let data = VideoData(VideoUrl: url, Image: nil)
                {
                    arrVideosDiscript?.append(data)
                }
                // arrVideos?.append(urlStr)
            }
            
            if let item = meta["pdf"] as? String
            {
                arrPdf = [PdfData]()
                let url = NSURL(string: item)
                if let data = PdfData(PdfUrl: url as! URL, Name: nil)
                {
                    arrPdf?.append(data)
                }
                // arrVideos?.append(urlStr)
            }
            print(arrImages ?? "Blank")
        }
    }
}
