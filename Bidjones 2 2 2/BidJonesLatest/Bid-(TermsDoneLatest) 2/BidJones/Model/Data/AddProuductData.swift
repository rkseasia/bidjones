//
//  AddProuductData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/2/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

class AddProuductData
{
    static let sharedInstance:AddProuductData? = AddProuductData()
    var itemID:Int?
    var itemType:Int?
    var id:Int?
    var albumName:String?
    var artistName:String?
    var title:String?
    var noOfPages:String?
    var genre:String?
    var typeOfBook:String?
    var serviceType:String?
    var musicNames:[String]?
    var location:String?
    var genreId:Int?
    var is_escrow:Bool?
    var description:String?
    var bookUrl:String?
    var TC:String?
    var duration:String?
    var minBidAmount:Double?
    var lat:Float?
    var lng:Float?
    var minBidType:String?
    var minBidTypeId:Int?
    var keySearchWord:String?
    var isItemForTrade:Bool?
    var arrImages:[ImagesData]?
    var arrVideos:[VideoData]?
    var arrVideosDiscript :[VideoData]?
    var arrAudios:[AudioData]?
    var arrPdf:[PdfData]?
    var arrLocalImages:[ImagesData]?
    var arrLocalVideos:[VideoData]?
    var arrlocalVideoDiscript : [VideoData]?
    var arrLocalAudios:[AudioData]?
    var arrLocalPdf:[PdfData]?
    var arrRemoveMedia:[String]?
    var arrRadioUrl:[String]?

    var IsEdited = false

    
    private init()
    {
    }
    deinit
    {
        print("Dealloc")
    }
    func Reinitilize()
    {
          AddProuductData.sharedInstance?.albumName = nil
          AddProuductData.sharedInstance?.artistName = nil
          AddProuductData.sharedInstance?.location = nil
          AddProuductData.sharedInstance?.title = nil
          AddProuductData.sharedInstance?.noOfPages = nil
          AddProuductData.sharedInstance?.genre = nil
          AddProuductData.sharedInstance?.is_escrow = nil
          AddProuductData.sharedInstance?.typeOfBook = nil
          AddProuductData.sharedInstance?.serviceType = nil
          AddProuductData.sharedInstance?.musicNames = nil
          AddProuductData.sharedInstance?.genreId = nil
          AddProuductData.sharedInstance?.description = nil
          AddProuductData.sharedInstance?.bookUrl = nil
          AddProuductData.sharedInstance?.duration = nil
          AddProuductData.sharedInstance?.minBidAmount = nil
          AddProuductData.sharedInstance?.minBidType = nil
          AddProuductData.sharedInstance?.minBidTypeId = nil
          AddProuductData.sharedInstance?.keySearchWord = nil
          AddProuductData.sharedInstance?.arrImages = nil
          AddProuductData.sharedInstance?.arrVideos = nil
          AddProuductData.sharedInstance?.arrAudios = nil
          AddProuductData.sharedInstance?.lat = nil
          AddProuductData.sharedInstance?.lng = nil
          AddProuductData.sharedInstance?.TC = nil
          AddProuductData.sharedInstance?.itemType = nil
          AddProuductData.sharedInstance?.id = nil
          AddProuductData.sharedInstance?.arrPdf = nil
          AddProuductData.sharedInstance?.arrLocalImages = nil
          AddProuductData.sharedInstance?.arrLocalVideos = nil
           AddProuductData.sharedInstance?.arrlocalVideoDiscript = nil
          AddProuductData.sharedInstance?.arrLocalAudios = nil
          AddProuductData.sharedInstance?.arrLocalPdf = nil
          AddProuductData.sharedInstance?.arrRemoveMedia = nil
          AddProuductData.sharedInstance?.arrRadioUrl = nil
          AddProuductData.sharedInstance?.IsEdited = false
          AddProuductData.sharedInstance?.isItemForTrade = false


    }
}
