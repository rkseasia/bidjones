//
//  RegisterData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/12/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

class RegisterData
{
    static var sharedInstance:RegisterData? = RegisterData()
    var firstName:String?
    var lastName:String?
    var email:String?
    var password:String?
    var username:String?
    var imageStrBase64:String?
    var address:String?
    var countryID:Int?
    var stateID:Int?
    var cityID:Int?
    var zipCode:String?
    var isSeller:Bool?
    var deviceTokken:String?
    var deviceType:String?
    var lat:String?
    var lng:String?


     private init()
    {
    }
    deinit {
        print("Dealloc")
    }
}
