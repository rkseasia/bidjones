//
//  RatingData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 8/1/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

struct RatingData {
    var buyerId:Int?
    var id:Int?
    var buyerName:String?
    var rating:Int?
    var comment:String?
    var date:String?


    init?(dict:[String:Any]) {
        guard let buyerID = dict["buyer_id"],let ID = dict["id"], let Rating = dict["rating"], let buyer = dict["buyer"] as? [String : Any], let dateStr = dict["created_at"] else
        {
            return nil
        }
        buyerId = (buyerID as! Int)
            buyerName = "\(buyer["first_name"] ?? "")" + " " + "\(buyer["last_name"] ?? "")"
            date = "\(dateStr)"
            id = (ID as! Int)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let Date = dateFormatter.date(from: self.date!)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let strDate = dateFormatter.string(from: Date!)
            date = strDate
            print(date as Any)
            comment = (dict["comment"] as! String)
            rating = (Rating as! Int)
            print(rating as Any)
    }
    
}
