//
//  ImagesData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/4/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


struct ImagesData {
  
    var imgUrl:URL?
    var image:UIImage?
    var fileName:String?

    init?(ImgUrl:URL?,Image:UIImage?)
    {
      
        guard let url = ImgUrl else
        {
            return nil
        }
      
        imgUrl = url
        image = Image
      
    }
}
