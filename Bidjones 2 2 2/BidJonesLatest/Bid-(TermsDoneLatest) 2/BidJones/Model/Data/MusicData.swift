//
//  MusicData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/9/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

struct AudioData {
    var title:String
    var albumTitle:String
    var audioURL:URL
    var artist:String?
    var assetUrl:URL?
    var fileName:String?
    var isRadio:Bool?

    
    init?(Title:String?,AudioURL:URL?,AlbumTitle:String?,Artist:String?,AssetUrl:URL?,IsRadio:Bool?)
    {
        guard let Title = Title,let AudioURL = AudioURL,let AlbumTitle = AlbumTitle else
        {
            return nil
        }
        title = Title
        audioURL = AudioURL
        albumTitle = AlbumTitle
        artist = Artist
        assetUrl = AssetUrl
        isRadio = IsRadio
    }
}

