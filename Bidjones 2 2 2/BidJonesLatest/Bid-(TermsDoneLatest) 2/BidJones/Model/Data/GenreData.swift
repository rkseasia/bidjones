//
//  GenreData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/17/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

struct GenreData {
    var id:Int
    var name:String
    init?(dict:[String:Any]) {
        guard let ID = dict[Kid], let Name = dict[Kname]else
        {
            return nil
        }
        
        
        id = ID as! Int
        name = Name as! String
    }
}
