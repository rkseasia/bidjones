//
//  CountryData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 3/30/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

struct CountryData {
    var id:Int
    var name:String
    var sortName:String
    init?(dict:[String:Any]) {
       guard let ID = dict[Kid], let Name = dict[Kname], let SortName = dict[Ksortname]else
       {
        return nil
       }
        id = ID as! Int
        name = Name as! String
        sortName = SortName as! String
}
}
