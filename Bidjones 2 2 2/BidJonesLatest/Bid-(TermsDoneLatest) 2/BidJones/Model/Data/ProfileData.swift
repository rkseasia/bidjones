//
//  ProfileData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 3/29/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


struct ProfieData {
    private init()
    {
    }
    static var sharedInstance = ProfieData()
     var id:Int?
    var is_account:Int?
     var Username:String?
     var address:String?
     var city_id:Int?
     var country_id:Int?
     var created_at:String?
     var device_token:String?
     var device_type:Int?
     var email:String?
     var first_name:String?
     var imageUrl:String?
     var last_name:String?
     var payment_platform_id:String?
     var payment_platform_type_id:Int?
     var state_id:Int?
     var username:String?
     var zip_code:String?
     var is_seller:Bool?
    var stripe_account_id:String?
    mutating func StoreData(json : [String:Any]) {
     
        if let UserName = json[Kusername]
        {
            Username = (UserName as? String)
        }
        if let is_Account = json[Kis_account]
        {
            is_account = (is_Account as? Int)
        }
        if let Address = json[Kaddress]
        {
            address = (Address as? String)
        }
        if let City_id = json[Kcity_id]
        {
            city_id = (City_id as? Int)
        }
        if let Country_id = json[Kcountry_id]
        {
            country_id = (Country_id as? Int)
        }
        if let Created_at = json[Kcreated_at]
        {
            created_at = (Created_at as? String)
        }
        if let Device_token = json[Kdevice_token]
        {
            device_token = (Device_token as? String)
        }
        if let Device_type = json[Kdevice_type]
        {
            device_type = (Device_type as? Int)
        }
        if let Email = json[Kemail]
        {
            email = (Email as? String)
        }
        if let First_name = json[Kfirst_name]
        {
            first_name = (First_name as? String)
        }
        if let ImageUrl = json[Kimage]
        {
            imageUrl = (ImageUrl as? String)
        }
        if let Last_name = json[Klast_name]
        {
            last_name = (Last_name as? String)
        }
        if let Payment_platform_idage = json[Kpayment_platform_id]
        {
            payment_platform_id = (Payment_platform_idage as? String)
        }
        if let Payment_platform_type_id = json[Kpayment_platform_type_id]
        {
            payment_platform_type_id = (Payment_platform_type_id as? Int)
        }
        if let State_id = json[Kstate_id]
        {
            state_id = (State_id as? Int)
        }
        if let Zip_code = json[Kzip_code]
        {
            zip_code = (Zip_code as? String)
        }
        if let Is_seller = json[Kis_seller]
        {
            is_seller = Is_seller as? Bool
        }
        if let ID = json[Kid] as? Int
        {
            id = ID 
        }
        if let stripe_Id = json[Kstripe_Id] as? String
        {
            stripe_account_id = stripe_Id
        }
    }
}
