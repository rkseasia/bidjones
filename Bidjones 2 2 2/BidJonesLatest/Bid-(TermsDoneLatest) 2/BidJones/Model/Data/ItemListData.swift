//
//  ItemListData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation


struct ItemListData:Codable
{
    var id:Int?
    var transactionID:Int?
    var sellerID:Int?
    var title:String?
    var ImageUrl:String?
    var date:String?
    var bidDate:String?
    var price:String?
    var bidAmount:String?
    var descrption:String?
    var categoryID:Int?
    var minBidType:String?
    var transcationNum:Int?
    var buyerID:Int?
    var bid_Id:Int?
    var isTradeItem:Int?
    var status_id:Int?
    var FreeSearch:Int?
    var myFreeItem:Int?
    var username : String?
    var userImage : String?
    var bid_status_id : Int?
    var address: String?
    

    init?(dict:[String:Any]) {
        print("your and own product item detail :\(dict)")
        guard let ID = dict[Kid],let Title = dict[Ktitle], let SellerID = dict[Kseller_id] else
        {
            return nil
        }
        if let Price = dict["bid_amount"] as? String
        {
           // let priceDob = Double(string:Price)
            bidAmount = "\(Price)".toDouble()?.cleanValue
            print(price as Any)
        }
        if let userImg = dict["userimage"] as? String {
            userImage = userImg
        }
        
        if let username1 = dict["username"] as? String {
            username = username1
        }
        
        
        
        if let bidType = dict["bid_type_name"] as? String
        {
            minBidType = bidType
        }
       if let Price = dict[Kmin_bid] as? String
       {
        price = "\(Price)".toDouble()?.cleanValue
        print(price as Any)
       }
        if let Status_id = dict["status_id"] as? Int
        {
            status_id = Status_id
        }
      if let CategoryID = dict["category_id"] as? Int
      {
        categoryID = CategoryID
      }
        if let BuyerId = dict["buyer_id"] as? Int
        {
            buyerID = BuyerId
        }
        if let dic = dict["amount"] as? [String:Any]
        {
            if let bidID = dic["id"] as? Int
            {
                bid_Id = bidID
            }
            if let bid_statusid = dic["bid_status_id"] as? Int
            {
                bid_status_id = bid_statusid
            }
            if let bidAddress = dic["address"] as? String
            {
                address = bidAddress
            }
        }
    
        
        
        if let createdDate = dict["created_at"] as? String
        {
            date = createdDate
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "dd-MM-yyyy"  //"MMM d, h:mm a" for  Sep 12, 2:11 PM
            let convertedDateFromStrDate = dateFormatterGet.date(from: self.date ?? "")
            let strDate = dateFormatterPrint.string(from: convertedDateFromStrDate ?? Date())
            date = strDate
            print(date as Any)

        }
        if let Date = dict["bid_date"] as? String
        {
            bidDate = "\(Date)"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let Date = dateFormatter.date(from: self.bidDate!)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let strDate = dateFormatter.string(from: Date!)
            bidDate = strDate
            print(date as Any)
        }
        if let Descrption = dict["description"]
        {
            descrption = "\(Descrption)"
            print(descrption as Any)
        }
        sellerID = SellerID as? Int
        id = ID as? Int
        title = Title as? String
        
        if let meta = dict["meta"] as? [String:Any]
        {
           if let arrImages = meta[Kimages] as? [String]
            {
                ImageUrl = arrImages[0]
            }
        }
        else
        {
            if let image  = dict["imgName"] as? String
            {
                ImageUrl = (image as! String)
            }
         }
    }
        
}
