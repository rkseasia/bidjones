//
//  BooksData.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/5/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation

struct BooksData {
    static var BooksArr = [BooksData]()
    var ImgStrBase64:String
    var image:UIImage?
    
    init?(ImgStr64:String?,Image:UIImage?)
    {
        guard let str = ImgStr64,let img = Image else
        {
            return nil
        }
        ImgStrBase64 = str
        image = img
    }
}
