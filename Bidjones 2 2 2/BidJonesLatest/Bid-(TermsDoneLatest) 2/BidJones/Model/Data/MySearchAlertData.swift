//
//  MySearchAlertData.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/2/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

struct MySearchAlertData {

    var shortTitle : String?
    var minBid : String?
    var status : String?
    var imgName : String?
    var productID : String?
    var sellerID : String?
    var categoryID : String?
    
    
   // var imag
    
    
    init(dict : [String : Any]){
        
        if let shortTitle = dict["short_title"] as? String {
            
            self.shortTitle = shortTitle
            
        }
       
        if let minBid = dict["min_bid"] as? String {
            
            self.minBid = minBid
            
        }
        
        
        if let status = dict["status"] as? String {
            
            self.status = status
            
            
        }
        
        
        if let imgName = dict["imgName"] as? String {
            
            self.imgName = imgName
            
        }
        
        
        if let productid = dict["id"] as? Int {
            
            self.productID = "\(productid)"
            
        }
        
        
        if let sellerID = dict["seller_id"] as? Int {
            
            self.sellerID = "\(sellerID)"
        }
        
        if let categoryID = dict["category_id"] as? Int {
            
            self.categoryID = "\(categoryID)"
        }
    
        
    }
    
    

}
