 
 
 //
//  SellerInfo.swift
//  BidJones
//
//  Created by Rakesh Kumar on 8/8/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation
import Alamofire
class SellerInfo {
    static let sharedManager = SellerInfo()
    private init()
    {
    }
    func PostApi(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping (SellerInfoData) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
          // MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            print("parameters seller:\(parameter)")
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            ////print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    if response.result.isSuccess
                    {
                        MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                        guard let data = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        let statusCode = data[Kstatus] as! Int
                        if statusCode == 200
                        {
                            DispatchQueue.main.async {
                                  MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                            }
                            
                            guard let result = data[Kresult] as? [String :Any]else{
                               // MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                                
                                completionnilResponse(data)
                                return
                            }
                            print("seller info data : \(result)")
                            
                            var arrSellerInfoData:SellerInfoData?
                           if let Item = SellerInfoData(dict: result)
                           {
                            arrSellerInfoData = Item
                            completionResponse(arrSellerInfoData!)
                            }
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(data)
                        }
                    }
                    else
                    {
                        completionError(response.error)
                        return
                    }
            }
        }
        else
        {
           // MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
            networkError(KNoInternetConnection)
        }
    }
    func PostApiForSoldItems(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([ItemListData]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
           // MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            print(parameter)
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            ////print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    if response.result.isSuccess
                    {
                       // MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                        guard let data = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        let statusCode = data[Kstatus] as! Int
                        if statusCode == 200
                        {
                            guard let array = data[Kresult] as? [[String :Any]]else{
                               // MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                                completionnilResponse(data)
                                return
                            }
                            var arrItemListData = [ItemListData]()
                            for item in array{
                                guard let Item =  ItemListData(dict: item)else{continue}
                                //arrItemListData.insert(Item, at: 0)
                                arrItemListData.append(Item)
                                //print(arrItemListData)
                            }
                          //  MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                            completionResponse(arrItemListData)
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(data)
                        }
                    }
                    else
                    {
                       // MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
        }
        else
        {
           // MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
            networkError(KNoInternetConnection)
        }
}
    func PostSellForAvailableItems(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([ItemListData]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
          //  MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            ////print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    if response.result.isSuccess
                    {
                      //  MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                        guard let data = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        let statusCode = data[Kstatus] as! Int
                        if statusCode == 200
                        {
                            guard let array = data["result"] as? [[String :Any]]else{
                              //  MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                                completionnilResponse(data)
                                return
                            }
                            var arrItemListData = [ItemListData]()
                            for item in array{
                                guard let Item =  ItemListData(dict: item)else{continue}
                                // arrItemListData.insert(Item, at: 0)
                                arrItemListData.append(Item)
                                //print(arrItemListData)
                            }
                          //  MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                            completionResponse(arrItemListData)
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(data)
                        }
                    }
                    else
                    {
                      //  MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
        }
        else
        {
          //  MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
            networkError(KNoInternetConnection)
        }
    }
    func PostApiFollow(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([String : Any]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            // MBProgressHUD.showAdded(to: KappDelegate.window, animated: true)
            let urlComplete = kBaseURL+url
            print(urlComplete)
            print(parameter)
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            //print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    let status = response.response?.statusCode
                    print("STATUS \(status)")
                    if response.result.isSuccess
                    {
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        //print(statusCode)
                        if statusCode == 200
                        {
                            completionResponse(responseData)
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(responseData)
                        }
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
            
        }
        else
        {
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            networkError(KNoInternetConnection)
        }
    }
}
