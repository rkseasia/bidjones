//
//  AddMusic.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/16/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

//
//  AddmMusic.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/16/18.
//  Copyright © 2018 Seasia. All rights reserved.
//



import Foundation
import Alamofire

class RefreshTokkenService
{
    static let sharedmanagerRfrshTkn = RefreshTokkenService()
    private init()
    {
    }
   
    func PostApi(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([String : Any]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            print(parameter)
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    if response.result.isSuccess
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        //print(statusCode)
                        if statusCode == 200
                        {
                            completionResponse(responseData)
                        }else if statusCode == 500{
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(responseData)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
            
        }
        else
        {
            networkError(KNoInternetConnection)
        }
    }
    
   
}


