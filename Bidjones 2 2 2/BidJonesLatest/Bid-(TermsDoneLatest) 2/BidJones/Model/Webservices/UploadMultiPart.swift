 //
//  UploadMultiPart.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/3/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation
import Alamofire

class UploadMultiPart
{
    static let sharedInstance = UploadMultiPart()
    static let sharedInstance1 = UploadMultiPart()
    private init()
    {
    }
    typealias RequestHandler = (_ error:Error?, _ url:String?, _ urlWatermark:String?)->(Void)

    func UploadData(awsType:AWSDataType,url:URL?,completionHandler:@escaping RequestHandler,completionnilResponse:  @escaping ([String : Any]) -> Void,Target : UIViewController,networkError: @escaping (String) -> Void){
        
        let parameters = [String:Any]()
        var mediaType = ""
            //(images,music,videos,video_thumb,pdf)
        //let myURL = url as NSURL
        print("import result : \(url)")
        let OrigiinalFileName = url?.lastPathComponent
        print(OrigiinalFileName ?? "")
        
        if (!(url?.absoluteString.contains("/Containers"))!)
        {
            completionHandler(nil,"\(url?.absoluteString ?? "")","")
             return
        }
        
        
        var randomFileName = ""
        var mimType = ""
        switch awsType {
        case .Image:
          mimType = "Image/jpg"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
            mediaType = "images"
        case .Song:
            mimType = "Audio/mp3"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).mp3"
            mediaType = "music"
        case .Video:
            mimType = "Video/mp4"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).mp4"
            mediaType = "videos"
        case .Pdf:
            mimType = "Doc/pdf"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).pdf"
            mediaType = "pdf"
        case .Graphic:
            mimType = "Image/jpg"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
            mediaType = "graphics"
        }
        
        let url1 = kBaseURL + KUploadMedia
        var url3 = ""
        if let sellerId = AddProuductData.sharedInstance?.id
        {
        let url2 = url1 + "\(sellerId)"
        url3 = url2 + "/" + mediaType
        print(url3)
        print(randomFileName)
        }
        let path1 = url!.path
        
        let url4 = kBaseURL + "/user/profileImage"

        print(url3)
        print(path1)

        var accessTokken = ""
        if let str = UserDefaults.standard.getAccessTokken()
        {
            accessTokken = str
        }
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"
        ]
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 1200
        let alamoManager = Alamofire.SessionManager(configuration: configuration)
        
        
        alamoManager.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            let testImage = NSData(contentsOf: url! as URL)
            //print("Upload DataUrl: \(String(describing: url))")
           // print("Upload Data: \(String(describing: testImage))")

            multipartFormData.append(url!, withName: "media", fileName: randomFileName, mimeType: mimType)
            
            //url3
        }, usingThreshold: UInt64.init(), to: url3 , method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    print(response)
                    
                    print(response.response?.statusCode ?? "")
                    print(response.request ?? "")  // original URL request
                    print(response.response ?? "") // URL response
                    print(response.data ?? "")     // server data
                    print(response.result)   // result of response serialization
                    print(response.result.value ?? "" )
                    if let error = response.error
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionHandler(error,nil,nil)
                        print("Upload failed with error: (\(error))")
                        print("Upload failed with error: (\(error.localizedDescription))")
                    }
                    else
                    {
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        print(statusCode)
                        if statusCode == 200
                        {
                            let mediaUrl = responseData["media_original_url"] as! String
                            let waterMark = responseData["media_preview_url"] as? String ?? ""
                            print("Uploaded to:\(mediaUrl)")
                            completionHandler(nil,"\(mediaUrl)","\(waterMark)")                        }
                        else
                        {
                            completionnilResponse(responseData)
                        }
                        }
                  //  }
                   // onCompletion?(nil)
                     alamoManager.session.invalidateAndCancel()
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                completionHandler(error,nil,nil)
                //onError?(error)
            }
        }
    }
    
    
    
    func UploadDataForImage(awsType:AWSDataType,url:URL?,completionHandler:@escaping RequestHandler,completionnilResponse:  @escaping ([String : Any]) -> Void,Target : UIViewController,networkError: @escaping (String) -> Void){
        
       MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
        let parameters = [String:Any]()
        var mediaType = ""
        //(images,music,videos,video_thumb,pdf)
        //let myURL = url as NSURL
        print("import result : \(url)")
        let OrigiinalFileName = url?.lastPathComponent
        print(OrigiinalFileName ?? "")
        
//        if (url?.absoluteString.contains("amazonaws"))!
//        {
//            completionHandler(nil,"\(url?.absoluteString ?? "")","")
//            return
//        }
        
        
        var randomFileName = ""
        var mimType = ""
        switch awsType {
        case .Image:
            mimType = "Image/jpg"
            //"Image/jpg"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
            //"/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
            mediaType = "images"
            //
        case .Song:
            mimType = "Audio/mp3"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).mp3"
            mediaType = "music"
        case .Video:
            mimType = "Video/mp4"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).mp4"
            mediaType = "videos"
        case .Pdf:
            mimType = "Doc/pdf"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).pdf"
            mediaType = "pdf"
        case .Graphic:
            mimType = "Image/jpg"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
            mediaType = "graphics"
        }
        
        let url1 = kBaseURL + KUploadMedia
        var url3 = ""
        if let sellerId = AddProuductData.sharedInstance?.id
        {
            let url2 = url1 + "\(sellerId)"
            url3 = url2 + "/" + mediaType
            print(url3)
            print(randomFileName)
        }
        let path1 = url!.path
        
        let url4 = kBaseURL + "/user/profileImage"
            //+ mediaType
        
        print(url3)
        print("your url: \(url4)")
        print(path1)
        
        var accessTokken = ""
        if let str = UserDefaults.standard.getAccessTokken()
        {
            accessTokken = str
        }
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"
        ]
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 1200
        let alamoManager = Alamofire.SessionManager(configuration: configuration)
        
        
        alamoManager.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            let testImage = NSData(contentsOf: url! as URL)
            //print("Upload DataUrl: \(String(describing: url))")
            // print("Upload Data: \(String(describing: testImage))")
            
            multipartFormData.append(url!, withName: "media", fileName: randomFileName, mimeType: mimType)
            //append(data: imageData, name: "file", fileName: "myImage.png", mimeType: "image/png")
                //.append(url!, withName: "media", fileName: randomFileName, mimeType: mimType)
            
            //url3
        }, usingThreshold: UInt64.init(), to: url4 , method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    print(response)
                    
                    print(response.response?.statusCode ?? "")
                    print(response.request ?? "")  // original URL request
                    print(response.response ?? "") // URL response
                    print(response.data ?? "")     // server data
                    print(response.result)   // result of response serialization
                    print(response.result.value ?? "" )
                  //   MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                    if let error = response.error
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionHandler(error,nil,nil)
                        print("Upload failed with error: (\(error))")
                        print("Upload failed with error: (\(error.localizedDescription))")
                    }
                    else
                    {
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        print(statusCode)
                        if statusCode == 200
                        {
                            
//                            DispatchQueue.main.async {
//
//                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
//                            }
//                             MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            guard let data = response.value else{return}
                            let responseData  = data as! [String : Any]
                            print("your response profile Image:\(responseData)")
                            let mediaUrl = responseData["media_original_url"] as! String
                            let waterMark = responseData["media_preview_url"] as? String ?? ""
                            print("Uploaded to:\(mediaUrl)")
                             MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            completionHandler(nil,"\(mediaUrl)","\(waterMark)")                        }
                        else
                        {
                              MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            completionnilResponse(responseData)
                        }
                    }
                    //  }
                    // onCompletion?(nil)
                    alamoManager.session.invalidateAndCancel()
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                completionHandler(error,nil,nil)
                //onError?(error)
            }
        }
    }
    
    
    
    
    func UploadDataWhole(awsType:AWSDataType,url:URL?, param : [String : Any],completionHandler:@escaping RequestHandler,completionnilResponse:  @escaping ([String : Any]) -> Void,networkError: @escaping (String) -> Void){
        
        
        MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
        var parameters = [String:Any]()
        parameters = param
        
        print("your parameters : \(parameters)")
        var mediaType = ""
        //(images,music,videos,video_thumb,pdf)
        //let myURL = url as NSURL
        print("import result : \(url)")
        let OrigiinalFileName = url?.lastPathComponent
        print(OrigiinalFileName ?? "")
        
//        if (url?.absoluteString.contains("amazonaws"))!
//        {
//            completionHandler(nil,"\(url?.absoluteString ?? "")","")
//            return
//        }
        
        
        var randomFileName = ""
        var mimType = ""
        switch awsType {
        case .Image:
            mimType = "Image/jpg"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
            mediaType = "images"
        case .Song:
            mimType = "Audio/mp3"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).mp3"
            mediaType = "music"
        case .Video:
            mimType = "Video/mp4"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).mp4"
            mediaType = "videos"
        case .Pdf:
            mimType = "Doc/pdf"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).pdf"
            mediaType = "pdf"
        case .Graphic:
            mimType = "Image/jpg"
            randomFileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
            mediaType = "graphics"
        }
        
        let url1 = kBaseURL + KUploadMedia
        var url3 = ""
        if let sellerId = AddProuductData.sharedInstance?.id
        {
            let url2 = url1 + "\(sellerId)"
            url3 = url2 + "/" + mediaType
            print(url3)
            print(randomFileName)
        }
        let path1 = url!.path
        let useUrl = "/user/updateProfile"
        let url4 = kBaseURL + useUrl
        print(url4)
        print(path1)
        
        var accessTokken = ""
        if let str = UserDefaults.standard.getAccessTokken()
        {
            accessTokken = str
        }
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"
        ]
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 1200
        let alamoManager = Alamofire.SessionManager(configuration: configuration)
        
        print(parameters)
        alamoManager.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            let testImage = NSData(contentsOf: url! as URL)
            //print("Upload DataUrl: \(String(describing: url))")
            // print("Upload Data: \(String(describing: testImage))")
            
         //   multipartFormData.append(url!, withName: "media", fileName: randomFileName, mimeType: mimType)
            
        }, usingThreshold: UInt64.init(), to: url4, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    
                    print(response)
                    
                    print(response.response?.statusCode ?? "")
                    print(response.request ?? "")  // original URL request
                    print(response.response ?? "") // URL response
                    print(response.data ?? "")     // server data
                    print(response.result)   // result of response serialization
                    print(response.result.value ?? "" )
                    if let error = response.error
                    {
                       
                        completionHandler(error,nil,nil)
                        print("Upload failed with error: (\(error))")
                        print("Upload failed with error: (\(error.localizedDescription))")
                    }
                    else
                    {
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        print(statusCode)
                        if statusCode == 200
                        {
                           MBProgressHUD.hide(for: KappDelegate.window, animated: true)
//                            let mediaUrl = responseData["media_original_url"] as! String
//                            let waterMark = responseData["media_preview_url"] as? String ?? ""
                            print("my response data for image : \(responseData)")
                            
                            let message = responseData["message"] as! String
                            
                          //  print("Uploaded to:\(mediaUrl)")
                            completionHandler(nil,message,nil)
                            
                        }
                        else
                        {
                            completionnilResponse(responseData)
                        }
                    }
                    //  }
                    // onCompletion?(nil)
                    alamoManager.session.invalidateAndCancel()
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                completionHandler(error,nil,nil)
                //onError?(error)
            }
        }
    }
    
    func UploadData(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([String : Any]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
   
    
     }
}
