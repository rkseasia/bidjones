//
//  MusicList.swift
//  BidJones
//
//  Created by Kuldeep Singh on 9/27/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation
import Alamofire

class MusicList {
    
   static let sharedManager = MusicList()
    
   private init() {
        
        
    }
    
    
    func PostApi(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([MusicListData]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
             MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            print(parameter)
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            //print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                   // print(response.description)
                 //   print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    let status = response.response?.statusCode
                    print("STATUS \(status)")
                    if response.result.isSuccess
                    {
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        print(statusCode)
                        
                        guard let data1 = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        
                        if statusCode == 200
                        {
                            
                            DispatchQueue.main.async {
                             
                    MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            
                            }
                            
                            
                            print("here is your :\(response)")
                            
                            
                            guard let array = data1[Kresult] as? [[String :Any]]else{
                                return
                            }

                                                        var arrItemListData = [MusicListData]()
                                                        for item in array{
                                                         //   guard let Item =  MusicListData(dict: item) else{continue}
                                                    let Item = MusicListData(dict: item)
                                                            //arrItemListData.insert(Item, at: 0)
                                                            arrItemListData.append(Item!)
                                                            //print(arrItemListData)
                                                        }
                            
                          //  MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            completionResponse(arrItemListData)
                            
                            
                            
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            completionnilResponse(responseData)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
            
        }
        else
        {
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            networkError(KNoInternetConnection)
        }
    }
    
    
    
    func PostApiForNotification(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([String : Any]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            print(parameter)
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            //print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    // print(response.description)
                    //   print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    let status = response.response?.statusCode
                    print("STATUS \(status)")
                    if response.result.isSuccess
                    {
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        print(statusCode)
                        
                        guard let data1 = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        
                        if statusCode == 200
                        {
                            
                            DispatchQueue.main.async {
                                
                                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                                
                            }
                            
                            
                            print("here is your :\(response)")
                            
                         //   re
//                            guard let array = data1[Kresult] as? [[String :Any]]else{
//                                return
//                            }
//
//                            var arrItemListData = [MusicListData]()
//                            for item in array{
//                                //   guard let Item =  MusicListData(dict: item) else{continue}
//                                let Item = MusicListData(dict: item)
//                                //arrItemListData.insert(Item, at: 0)
//                                arrItemListData.append(Item!)
//                                //print(arrItemListData)
//                            }
                            
                            //  MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            completionResponse(["Message": "Successfully Sent Notification"])
                            
                            
                            
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            completionnilResponse(responseData)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
            
        }
        else
        {
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            networkError(KNoInternetConnection)
        }
    }
    
    
    
    
}
