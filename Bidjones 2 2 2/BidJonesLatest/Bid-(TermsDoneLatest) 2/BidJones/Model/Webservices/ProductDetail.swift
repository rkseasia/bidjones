//
//  AddSellService.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/16/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

//
//  AddSellService.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/16/18.
//  Copyright © 2018 Seasia. All rights reserved.
//



import Foundation
import Alamofire

class ProductDetail{
    static let sharedManager = ProductDetail()
    private init()
    {
    }
    func PostApi(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([String : Any]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            print("your bidding parameter:\(parameter)")
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
                
                print("your device token : \(str)")
            }
            print("your access token : \(accessTokken)")
            //print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    let status = response.response?.statusCode
                    print("STATUS \(String(describing: status))")
                    
                    if response.result.isSuccess
                    {
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        //print(statusCode)
                        if statusCode == 200
                        {
                            completionResponse(responseData)
                        }else if statusCode == 500{
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(responseData)
                        }
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
        }
        else
        {
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            networkError(KNoInternetConnection)
        }
    }
    
    func GetApi(url : String, Target : UIViewController, completionResponse:  @escaping (ProductData) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print("URL : \(urlComplete)")
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            Alamofire.request(urlComplete, method: .get, parameters: nil, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    let status = response.response?.statusCode
                    print("STATUS \(String(describing: status))")
                    
                    if response.result.isSuccess
                    {
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        print(statusCode)
                        if statusCode == 200
                        {
                            let result = responseData["result"] as? [String:Any]
                            
                            print("This is my result songof : \(result!)")
                            
                            let obj = ProductData(json: result!)
                          completionResponse(obj!)
                            //   completionResponse(responseData)
                        }else if statusCode == 500{
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(responseData)
                        }
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
        }
        else
        {
            networkError(KNoInternetConnection)
        }
    }
    
    
    
    func PostApiForPayment(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([String : Any]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            print(parameter)
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            //print("Kaccess_token : \(accessTokken)")
            let headers    = [  "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            
            Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    let status = response.response?.statusCode
                    print("STATUS \(status)")
                    if response.result.isSuccess
                    {
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        print(statusCode)
                        
                        guard let data1 = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        
                        if statusCode == 200
                        {
                            
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            }
                            print("here is your :\(response)")
                            
                        
                            guard let array = response.value as? [String :Any] else{
                                return
                            }
                            
                            print("you are getting output: \(array)")
//
//                            var arrItemListData = [FollowerData]()
//                            for item in array{
//
//
//                                //     guard let Item1 = MySearchAlertData(dict: item) else {
//                                //                                                                continue
//                                //                                                            }
//                                //
//
//                                //      guard let Item =  MySearchAlertData(dict: item)  else {continue}
//                                //arrItemListData.insert(Item, at: 0)
//                                let Item = FollowerData(dict: item)
//                                arrItemListData.append(Item)
//                                //print(arrItemListData)
//                            }
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            completionResponse(array)
                            
                            
                            
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            completionnilResponse(responseData)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
            
        }
        else
        {
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            networkError(KNoInternetConnection)
        }
    }
    
    
}


