//
//  itemDetail.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/30/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation
import Alamofire


class  ItemDetail {
    
    static let sharedManager = ItemDetail()
    
    private init() {
    }
    
    
    func GetApi(url : String,Target : UIViewController, completionResponse:  @escaping ([ItemDetailData]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print("URL : \(urlComplete)")
            
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            Alamofire.request(urlComplete, method: .get, parameters: nil, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    if response.result.isSuccess
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        guard let data = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        let statusCode = data[Kstatus] as! Int
                        print("here is status code : \(statusCode)")
                        if statusCode == 200
                        {
                            
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            }
                            
                            print("your datta : \(response)")
                            // let userID = data[]
                            
                            guard let array = data[Kresult] as? [String: Any] else {
                                
                                return
                            }
                            
                            var dataModel = [ItemDetailData]()
                            print("This is data here item sold or purchase:\(array)")
                            
                            //                            for item in array {
                            //
                            //
                            let model = ItemDetailData.init(dict: array)
                            dataModel.append(model)
                            //
                            //
                            //
                            //                            }
                            
                            print("this is my data : \(dataModel)")
                            
                            completionResponse(dataModel)
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(data)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
        }
        else
        {
            networkError(KNoInternetConnection)
        }
    }
    
    
        
        
}
