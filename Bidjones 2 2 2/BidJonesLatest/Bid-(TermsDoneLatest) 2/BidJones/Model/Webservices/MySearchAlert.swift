//
//  MySearchAlert.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/2/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import Alamofire

class MySearchAlert {

   static let sharedManager = MySearchAlert()
    
    private init() {
        
        
        
    }
    

    
    func PostApi(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([ItemListData ]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
      if Target.checkInternetConnection()
      {
        MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
        let urlComplete = kBaseURL+url
        print(urlComplete)
        print(parameter)
        var accessTokken = ""
        if let str = UserDefaults.standard.getAccessTokken()
        {
          accessTokken = str
        }
        ////print("Kaccess_token : \(accessTokken)")
        let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
        
        Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
          .responseJSON { response in
            print(response.timeline)
            print(response.result)
            print(response.error ?? "Blank Error")
            print(response.debugDescription)
            print(response.description)
            print(response.data ?? "Blank Data")
            print(response.request ?? "Blank Request")
            print(response.value ?? "Blank value")
            if response.result.isSuccess
            {
              MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
              guard let data = response.value as? [String:Any] else {
                completionError(response.error)
                return
              }
              let statusCode = data[Kstatus] as! Int
              if statusCode == 200
              {
                print("respones of yes no : \(data)")
                guard let array = data[Kresult] as? [[String :Any]]else{
                  MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                  completionnilResponse(data)
                  return
                }
                var arrItemListData = [ItemListData]()
                for item in array{
                  guard let Item =  ItemListData(dict: item)else{continue}
                  //arrItemListData.insert(Item, at: 0)
                  arrItemListData.append(Item)
                  
                  //print(arrItemListData)
                }
                MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                completionResponse(arrItemListData)
              }else if statusCode == 500{
                  MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                  Target.sessionLogout()
              }
              else
              {
                completionnilResponse(data)
              }
            }
            else
            {
              MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
              completionError(response.error)
              return
            }
        }
        
      }
      else
      {
        MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
        networkError(KNoInternetConnection)
      }
  }
    
    
    
}
