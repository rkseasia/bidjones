//
//  Bids.swift
//  BidJones
//
//  Created by Rakesh Kumar on 6/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation
import Alamofire

class Bids
{
    static let sharedManager = Bids()
    private init()
    {
    }
    func GetSentBidsApi(url : String,Target : UIViewController, completionResponse:  @escaping ([ItemListData]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            ////print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            Alamofire.request(urlComplete, method: .get, parameters: nil, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    if response.result.isSuccess
                    {
                        MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                        guard let data = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        let statusCode = data[Kstatus] as! Int
                        if statusCode == 200
                        {
                            guard let array = data["sent"] as? [[String :Any]]else{
                                MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                                completionnilResponse(data)
                                return
                            }
                            var arrItemListData = [ItemListData]()
                            for item in array{
                                guard let Item =  ItemListData(dict: item)else{continue}
                              //  arrItemListData.insert(Item, at: 0)
                                arrItemListData.append(Item)

                                //print(arrItemListData)
                            }
                            MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                            completionResponse(arrItemListData)
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(data)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
        }
        else
        {
            MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
            networkError(KNoInternetConnection)
        }
    }
    func GetRecieveBidsApi(url : String,Target : UIViewController, completionResponse:  @escaping ([ItemListData]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            var accessTokken = ""
            if let str = UserDefaults.standard.getAccessTokken()
            {
                accessTokken = str
            }
            ////print("Kaccess_token : \(accessTokken)")
            let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
            
            Alamofire.request(urlComplete, method: .get, parameters: nil, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    if response.result.isSuccess
                    {
                        MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                        guard let data = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        let statusCode = data[Kstatus] as! Int
                        print(statusCode)
                        if statusCode == 200
                        {
                            
                            
                            guard let array = data["received"] as? [[String :Any]]else{
                                MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                                completionnilResponse(data)
                                return
                            }
                            
                            print("pickup response : \(array)")
                            var arrItemListData = [ItemListData]()
                            for item in array{
                                guard let Item =  ItemListData(dict: item)else{continue}
                               // arrItemListData.insert(Item, at: 0)
                                arrItemListData.append(Item)

                                //print(arrItemListData)
                            }
                            MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                            completionResponse(arrItemListData)
                        }else if statusCode == 500{
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(data)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
        }
        else
        {
            MBProgressHUD.hide(for:  KappDelegate.window, animated: true)
            networkError(KNoInternetConnection)
        }
    }


func PostApi(url : String, parameter : [String:Any],Target : UIViewController, completionResponse:  @escaping ([MerchPickupData]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
{
    if Target.checkInternetConnection()
    {
        MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
        let urlComplete = kBaseURL+url
        print(urlComplete)
        print(parameter)
        var accessTokken = ""
        if let str = UserDefaults.standard.getAccessTokken()
        {
            accessTokken = str
        }
        //print("Kaccess_token : \(accessTokken)")
        let headers    = [ "Content-Type" : "application/json","Authorization": "Bearer \(accessTokken)","Accept" : "application/json"]
        
        Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
            .responseJSON { response in
                print(response.timeline)
                print(response.result)
                print(response.error ?? "Blank Error")
                print(response.debugDescription)
                // print(response.description)
                //   print(response.data ?? "Blank Data")
                print(response.request ?? "Blank Request")
                print(response.value ?? "Blank value")
                let status = response.response?.statusCode
                print("STATUS \(status)")
                if response.result.isSuccess
                {
                    guard let data = response.value else{return}
                    let responseData  = data as! [String : Any]
                    let statusCode = responseData["status"] as! Int
                    print(statusCode)
                    
                    guard let data1 = response.value as? [String:Any] else {
                        completionError(response.error)
                        return
                    }
                    
                    if statusCode == 200
                    {
                        
                        DispatchQueue.main.async {
                            
                            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                            
                        }
                         MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        
                        print("here is your :\(response)")
                        
                        
                        guard let array = data1[Kresult] as? [[String :Any]] else{
                            return
                        }
                        
                        print("your array data : \(array)")
                        var arrItemListData = [MerchPickupData]()
                        for item in array  {
                           // guard let Item =  ItemListData(dict: item)else{continue}
                            // arrItemListData.insert(Item, at: 0)
                            print("your item pickup :\(item)")
                            
                          //  if   let item1 = item["item"] as? [String : Any] {
                            //"item"
                           // let Item =  ItemListData(dict: item as [String : Any])
                         //   guard let Item =  MerchPickupData(dict: item1)else{continue}
                                
                          let Item =  MerchPickupData(dict: item)
                            print(Item)
                            arrItemListData.append(Item)
                            }
                            //print(arrItemListData)
                       // }
                        print(arrItemListData)
                        //  MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionResponse(arrItemListData)
                        
                        
                        
                    }else if statusCode == 500{
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        Target.sessionLogout()
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionnilResponse(responseData)
                    }
                }
                else
                {
                    MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                    completionError(response.error)
                    return
                }
        }
        
    }
    else
    {
        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
        networkError(KNoInternetConnection)
    }
}

}
