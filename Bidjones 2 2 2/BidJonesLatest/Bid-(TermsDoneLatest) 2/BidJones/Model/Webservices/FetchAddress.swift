//
//  FetchAddresses.swift
//  BidJones
//
//  Created by Rakesh Kumar on 3/20/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation
import Alamofire


class FetchAddress
{
    static let sharedFetchAddress = FetchAddress()
    private init()
    {
    }
    static func PostApi(url : String, parameter : [String:Any], completionResponse:  @escaping ([String : Any]?) -> Void,completionError: @escaping (Error?) -> Void)
    {
        let urlComplete = kBaseURL+url
        print(urlComplete)
        print(parameter)
        let headers    = [ "Content-Type" : "application/json"]
        Alamofire.request(urlComplete, method: .post, parameters: parameter, encoding: JSONEncoding.default, headers : headers)
            .responseJSON { response in
                print(response.timeline)
                print(response.result)
                print(response.error ?? "Blank Error")
                print(response.debugDescription)
                print(response.description)
                print(response.data ?? "Blank Data")
                print(response.request ?? "Blank Request")
                print(response.value ?? "Blank value")
                if response.result.isSuccess
                {
                    var Data = [String:Any]()
                    Data = response.value as! [String : Any]
                    completionResponse(Data)
                }
                else
                {
                    completionError(response.error)
                    return
                }
        }
    }
    
    func GetApi(url : String,Target : UIViewController,completionResponse:  @escaping ([String : Any]) -> Void,completionnilResponse:  @escaping ([String : Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            let headers    = [ "Content-Type" : "application/json"]
            Alamofire.request(urlComplete, method: .get, parameters: nil, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
                    print(response.timeline)
                    print(response.result)
                    print(response.error ?? "Blank Error")
                    print(response.debugDescription)
                    print(response.description)
                    print(response.data ?? "Blank Data")
                    print(response.request ?? "Blank Request")
                    print(response.value ?? "Blank value")
                    if response.result.isSuccess
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        guard let data = response.value else{return}
                        let responseData  = data as! [String : Any]
                        let statusCode = responseData["status"] as! Int
                        //print(statusCode)
                        if statusCode == 200
                        {
                            completionResponse(responseData)
                        }else if statusCode == 500{
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(responseData)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
        }
        else
        {
            networkError(KNoInternetConnection)
        }
        
        
    }
    
    func GetAddressListApi(url : String,Target : UIViewController,completionResponse:  @escaping ([AddressData]) -> Void,completionnilResponse:  @escaping ([String:Any]) -> Void,completionError: @escaping (Error?) -> Void,networkError: @escaping (String) -> Void)
    {
        if Target.checkInternetConnection()
        {
            MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KLoading
            let urlComplete = kBaseURL+url
            print(urlComplete)
            let headers    = [ "Content-Type" : "application/json"]
            Alamofire.request(urlComplete, method: .get, parameters: nil, encoding: JSONEncoding.default, headers : headers)
                .responseJSON { response in
//                    print(response.timeline)
                    print(response.result)
//                    print(response.error ?? "Blank Error")
//                    print(response.debugDescription)
//                    print(response.description)
//                    print(response.data ?? "Blank Data")
//                    print(response.request ?? "Blank Request")
//                    print(response.value ?? "Blank value")
                    if response.result.isSuccess
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        guard let data = response.value as? [String:Any] else {
                            completionError(response.error)
                            return
                        }
                        let statusCode = data[Kstatus] as! Int
                        if statusCode == 200
                        {
                            guard let array = data[Kresult] as? [[String :Any]]else{
                                return
                            }
                            var addressList = [AddressData]()
                            for item in array{
                                guard let country =  AddressData(dict: item)else{continue}
                                addressList.append(country)
                            }
                            completionResponse(addressList)
                        }else if statusCode == 500{
                            Target.sessionLogout()
                        }
                        else
                        {
                            completionnilResponse(data)
                        }
                    }
                    else
                    {
                        MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                        completionError(response.error)
                        return
                    }
            }
        }
        else
        {
            networkError(KNoInternetConnection)
        }
        
        
    }
    
    
}

