//
//  UITextField.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation
import UIKit


extension UITextField{
    
    func addDoneButtonToKeyboard(target: Any,myAction:Selector?,Title:String){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 300, height: 40))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: Title, style: UIBarButtonItemStyle.done, target: target, action: myAction)
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    
    func RestrictMaxCharacter(maxCount: Int,range: NSRange,string: String) -> Bool {
        let currentText = self.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
        return updatedText.count <= maxCount
    }
}
