//
//  UserDefaults.swift
//  BidJones
//
//  Created by Rakesh Kumar on 3/22/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import Foundation
import UIKit

extension UserDefaults{
    
    //MARK: Check Login
    func setLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        //synchronize()
    }
    
    func setShowSellerInfo(value: Bool?) {
        set(value, forKey: UserDefaultsKeys.sellerINfo.rawValue)
        //synchronize()
    }
    func setShowEditButton(value: Bool?) {
        set(value, forKey: UserDefaultsKeys.Edit.rawValue)
        //synchronize()
    }
//    func getShowEditButton() -> Bool?{
//
//        if let value = self.value(forKey:UserDefaultsKeys.Edit.rawValue) as? Bool
//        {
//            return value
//        }
//        return false
//
//    }
    
    func isLoggedIn()-> Bool {
        return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue)
    }
    
    //MARK: Save User Data
    func setUserID(value: Int){
        set(value, forKey: UserDefaultsKeys.userID.rawValue)
        //synchronize()
    }
    func setStateID(value: Int){
        set(value, forKey: UserDefaultsKeys.stateID.rawValue)
        //synchronize()
       
    }
 
    func setItemID(value: Int){
        set(value, forKey: UserDefaultsKeys.sellerID.rawValue)
        //synchronize()
    }
    
    func setUserType(value: Bool){
        set(value, forKey: UserDefaultsKeys.userType.rawValue)
        //synchronize()
    }
    
    func setShowMessageType(value : String) {
        set(value, forKey: UserDefaultsKeys.MessageForStripAccount.rawValue)
    }
    
    
    func setAccessTokken(value: String?){
        set(value, forKey: UserDefaultsKeys.accessTokken.rawValue)
        //synchronize()
    }
    
    func setDeviceTokken(value: String?){
        set(value, forKey: UserDefaultsKeys.deviceTokken.rawValue)
        //synchronize()
    }
    
    
    func setRefreshTokken(value: String?){
        set(value, forKey: UserDefaultsKeys.refreshTokken.rawValue)
        //synchronize()
    }
    
    func setRememberMe(value:Bool) {
        set(value, forKey: UserDefaultsKeys.rememberMe.rawValue)
        
        
    }
    func setEmail(value:String) {
        set(value, forKey: UserDefaultsKeys.email.rawValue)
        
    }
    func setPassword(value:String) {
        set(value, forKey: UserDefaultsKeys.password.rawValue)
    }
    func setRatingData(value:Data?) {
        set(value, forKey: UserDefaultsKeys.ratingData.rawValue)
    }
    func setStripeExist(value:Bool?) {
        set(value, forKey: UserDefaultsKeys.ISStripeAccountExist.rawValue)
    }
    
    //MARK: Retrieve User Data
    func getStateID() -> Int?
    {
        if let value = self.value(forKey:UserDefaultsKeys.stateID.rawValue) as? Int
        {
            return value
        }
        return nil
    }
    func getUserID() -> Int?
    {
        if let value = self.value(forKey:UserDefaultsKeys.userID.rawValue) as? Int
        {
            return value
        }
        return nil
    }
//    func getItemID() -> Int?
//    {
//        if let value = self.value(forKey:UserDefaultsKeys.sellerID.rawValue) as? Int
//        {
//            return value
//        }
//        return nil
//    }
    
    
    func getRememberMe() -> Bool{
        
        if let value = self.value(forKey:UserDefaultsKeys.rememberMe.rawValue) as? Bool
        {
            return value
        }
        return false
        
    }
    
    func getShowSellerInfo() -> Bool?{
        
        if let value = self.value(forKey:UserDefaultsKeys.sellerINfo.rawValue) as? Bool
        {
            return value
        }
        return false
        
    }
    func getShowEditButton() -> Bool?{
        
        if let value = self.value(forKey:UserDefaultsKeys.Edit.rawValue) as? Bool
        {
            return value
        }
        return false
        
    }
    func getRatingData() -> Data? {
        if let value = self.value(forKey:UserDefaultsKeys.ratingData.rawValue) as? Data
        {
            return value
        }
        return nil
    }
    func getEmail() -> String? {
        if let value = self.value(forKey:UserDefaultsKeys.email.rawValue) as? String
        {
            return value
        }
        return nil
    }
   
    func getPassword() -> String? {
       // return string(forKey:UserDefaultsKeys.password.rawValue)!
        if let value = self.value(forKey:UserDefaultsKeys.password.rawValue) as? String
        {
            return value
        }
        return nil
    }
    func getAccessTokken() -> String? {
       // return string(forKey:UserDefaultsKeys.accessTokken.rawValue)!
        
        if let value = self.value(forKey:UserDefaultsKeys.accessTokken.rawValue) as? String
        {
            return value
        }
        return nil
    }
    func getDeivceTokken() -> String? {
        // return string(forKey:UserDefaultsKeys.accessTokken.rawValue)!
        
        if let value = self.value(forKey:UserDefaultsKeys.deviceTokken.rawValue) as? String
        {
            return value
        }
        return nil
    }
    func getRefreshTokken() -> String? {
        // return string(forKey:UserDefaultsKeys.accessTokken.rawValue)!
        
        if let value = self.value(forKey:UserDefaultsKeys.refreshTokken.rawValue) as? String
        {
            return value
        }
        return nil
    }
    func getUserType() -> Bool? {
        
        //return bool(forKey: UserDefaultsKeys.userType.rawValue)
        if let value = self.value(forKey:UserDefaultsKeys.userType.rawValue) as? Bool
        {
            return value
        }
        return false
    }
    
    func getMessageType() -> String? {
        // return string(forKey:UserDefaultsKeys.accessTokken.rawValue)!
        
        if let value = self.value(forKey:UserDefaultsKeys.MessageForStripAccount.rawValue) as? String
        {
            return value
        }
        return nil
    }
    func getStripeExist() -> Bool? {
        // return string(forKey:UserDefaultsKeys.accessTokken.rawValue)!
        
        if let value = self.value(forKey:UserDefaultsKeys.ISStripeAccountExist.rawValue) as? Bool
        {
            return value
        }
        return nil
    }
    
}


enum UserDefaultsKeys : String {
    case isLoggedIn
    case stateID
    case userID
    case sellerID
    case rememberMe
    case email
    case password
    case userType
    case accessTokken
    case deviceTokken
    case refreshTokken
    case ratingData
    case sellerINfo
    case Edit
    case MessageForStripAccount
    case ISStripeAccountExist


}



