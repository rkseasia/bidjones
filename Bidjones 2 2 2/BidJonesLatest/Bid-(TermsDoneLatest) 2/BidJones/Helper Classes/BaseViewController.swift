
//
//  BaseViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Ashish on 21/09/15.
//  Copyright (c) 2015 Kode. All rights reserved.
//

import UIKit

var btnShowMenu = UIButton(type: UIButtonType.system)

class BaseViewController: UIViewController, SlideMenuDelegate {
  
  enum DrawerType{
    case Home
    case Trade
    case Free
  }
  
  var drawerType:DrawerType?
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
      
      switch(drawerType)
      {
      case .Home?:
        switch(index){
    //gurleen
        case 0:
            
        self.openViewControllerBasedOnIdentifier(kmyProfileVC)
        break
            
        case 1:
            
        self.openViewControllerBasedOnIdentifier(kstuffSalesVC)
        break
            
        case 2:
        self.openViewControllerBasedOnIdentifier(kserviceSalesVC)
        break
            
        case 3:
        self.openViewControllerBasedOnIdentifier(kmusicSalesVC)
        break
            
        case 4:
        self.openViewControllerBasedOnIdentifier(kbookSalesVC)
        break
            
        case 5:
        self.openViewControllerBasedOnIdentifier(kvideoSalesVC)
        break
//
//        case :
//          print("Home\n", terminator: "")
//          self.openViewControllerBasedOnIdentifier("settings")
//          break
       case 6:
          print("Play\n", terminator: "")
          self.openViewControllerBasedOnIdentifier("alertsVC")
          break
        case 7:
          print("Home\n", terminator: "")
          self.openViewControllerBasedOnIdentifier("bidsVC")
          break
        case 8:
          print("Home\n", terminator: "")
          self.openViewControllerBasedOnIdentifier("historyVC")
          break
        case 9:
          print("Play\n", terminator: "")
          self.openViewControllerBasedOnIdentifier("helpVC")
          break
        case 10:
          print("Play\n", terminator: "")
          self.showAlertMessage(titleStr: "AboutUs", messageStr: "Coming Soon")
          break
        case 11:
          print("Play\n", terminator: "")
          self.openViewControllerBasedOnIdentifier("changePasswordVC")
          break
        default:
          print("default\n", terminator: "")
        }
      case .Trade?:
        switch(index){
        case 0:
          print("Home\n", terminator: "")
          KCommonFunctions.SetRootViewController(rootVC:.HomeNavigation)
          break
        case 1:
          print("Play\n", terminator: "")
          //
          //self.openViewControllerBasedOnIdentifier("TradeRequestsVC")
          self.openViewControllerBasedOnIdentifier("TradeSearchAlertVC")
          
          break
           // gurleen
//        case 2:
//          print("Play\n", terminator: "")
//         //
//           //self.openViewControllerBasedOnIdentifier("TradeRequestsVC")
//          self.openViewControllerBasedOnIdentifier("MyTradeItemsVC")
//
//          break
        case 2:
          print("Home\n", terminator: "")
        //  self.openViewControllerBasedOnIdentifier("bidsVC")
          self.openViewControllerBasedOnIdentifier("TradeRequestsVC")

          break
        case 3:
          print("Home\n", terminator: "")
         // self.openViewControllerBasedOnIdentifier("historyVC")
          self.openViewControllerBasedOnIdentifier("TradeHistoryVC")

          break
        case 4:
          print("Play\n", terminator: "")
          self.openViewControllerBasedOnIdentifier("helpVC")
          break
        case 5:
          print("Play\n", terminator: "")
          self.showAlertMessage(titleStr: "AboutUs", messageStr: "Coming Soon")
          break
        case 6:
          print("Play\n", terminator: "")
          self.openViewControllerBasedOnIdentifier("changePasswordVC")
          break
        default:
          print("default\n", terminator: "")
        }
      case .Free?:
        switch (index) {
        case 0:
            print("Home\n", terminator: "")
            KCommonFunctions.SetRootViewController(rootVC:.HomeNavigation)
            break
        case 1:
            print("Play\n", terminator: "")
            //
            //self.openViewControllerBasedOnIdentifier("TradeRequestsVC")
            self.openViewControllerBasedOnIdentifier("FreeSearchAlertVC")
            
            break
//        case 2:
//            print("Play\n", terminator: "")
//            //
//            //self.openViewControllerBasedOnIdentifier("TradeRequestsVC")
//            self.openViewControllerBasedOnIdentifier("MyFreeItemsVC")
//
//            break
        case 2:
            print("Home\n", terminator: "")
            //  self.openViewControllerBasedOnIdentifier("bidsVC")
            self.openViewControllerBasedOnIdentifier("FreeRequestsVC")
            
            break
        case 3:
            print("Home\n", terminator: "")
            // self.openViewControllerBasedOnIdentifier("historyVC")
            self.openViewControllerBasedOnIdentifier("FreeHistoryVC")
            
            break
        case 4:
            print("Play\n", terminator: "")
            self.openViewControllerBasedOnIdentifier("helpVC")
            break
        case 5:
            print("Play\n", terminator: "")
            self.showAlertMessage(titleStr: "AboutUs", messageStr: "Coming Soon")
            break
        case 6:
            print("Play\n", terminator: "")
            self.openViewControllerBasedOnIdentifier("changePasswordVC")
            break
        default:
            print("default\n", terminator: "")
            
            
        }
        
      case .none:
        print("None")
      }
      
      
      

    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        if let topViewController : UIViewController = self.navigationController!.topViewController{
        
        if (topViewController.restorationIdentifier == destViewController.restorationIdentifier){
            print("Same VC")
        } else {
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
        }else{
             self.navigationController!.pushViewController(destViewController, animated: true)
        }
    }
    
    func addSlideMenuButton(){
        
        btnShowMenu = UIButton(type: UIButtonType.system)
        btnShowMenu.setImage(UIImage.init(named: "menu"), for: UIControlState())
        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnShowMenu.tintColor = .black
        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.leftBarButtonItem = customBarItem;
        
    }

    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
       
        return defaultMenuImage;
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if (sender.tag == 10 )
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            
            //sideBarOpen = false
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    viewMenuBack.removeFromSuperview()
            })
            
            return
        }
        
        
        
        //sideBarOpen = true
        sender.isEnabled = false
        sender.tag = 10
      
      switch drawerType {
      case .Home?:
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
          menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
          sender.isEnabled = true
        }, completion:nil)

      print("Home")
      case .Trade?:
       // let menuVC : MenuTradeVC = self.storyboard!.instantiateViewController(withIdentifier: "MenuTradeVC") as! MenuTradeVC
        
        let menuVC:MenuTradeVC = UIStoryboard(name: "Trade", bundle: nil).instantiateViewController(withIdentifier: "MenuTradeVC") as! MenuTradeVC
        // .instantiatViewControllerWithIdentifier() returns AnyObject! this must be downcast to utilize it
        
       // self.presentViewController(viewController, animated: false, completion: nil)
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
          menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
          sender.isEnabled = true
        }, completion:nil)

      print("Trade")
      case .none:
        print("None")

      case .some(.Free):
        
        let menuVC:MenuFreeVC = UIStoryboard(name: "Free", bundle: nil).instantiateViewController(withIdentifier: "MenuFreeVC") as! MenuFreeVC
        // .instantiatViewControllerWithIdentifier() returns AnyObject! this must be downcast to utilize it
        
        // self.presentViewController(viewController, animated: false, completion: nil)
        menuVC.btnMenu = sender
        menuVC.delegate = self
        self.view.addSubview(menuVC.view)
        self.addChildViewController(menuVC)
        menuVC.view.layoutIfNeeded()
        
        
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
        }, completion:nil)
        
        print("Trade")
      case .none:
        print("None")
        }
//
//        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
//        menuVC.btnMenu = sender
//        menuVC.delegate = self
//        self.view.addSubview(menuVC.view)
//        self.addChildViewController(menuVC)
//        menuVC.view.layoutIfNeeded()
//
//
//        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
//
//        UIView.animate(withDuration: 0.3, animations: { () -> Void in
//            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
//            sender.isEnabled = true
//            }, completion:nil)
    }
}
