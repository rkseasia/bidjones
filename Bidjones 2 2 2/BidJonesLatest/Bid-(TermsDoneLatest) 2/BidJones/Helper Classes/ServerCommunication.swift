//
//  ServerCommunication.swift
//  sowi
//
//  Created by Harpreet Singh on 24/08/16.
//  Copyright © 2016 Harpreet Singh. All rights reserved.
//

import UIKit
//typealias CompletionBlock = AnyObject? -> Void
//typealias ErrorBlock = NSError? -> Void

class ServerCommunication: NSObject {
    
    typealias CompletionBlock = (AnyObject?) -> Void
    typealias ErrorBlock = (NSError?) -> Void
    
    static let sharedInstance = ServerCommunication()
    typealias offlineBlock = (AnyObject?,AnyObject?) -> Void
    
    
    var data1 : NSData = NSData()
    var isInternetActive: Bool!
    var internetConnectionReach: Reachability!
    
}

/**
 Server communication is created to hit various kind of services in any class.
 
 This class contains POST , GET Method services
 
 :param:  Dictionaries That needs to be Posted.
 
 */

extension ServerCommunication : URLSessionDelegate {
    
    /**
     postService is used to hit service with "POST" method.
     
     :param:  completion_block This is used to notify that service has been completed.
     
     :param:  error_block This is used to notify that an error has occured during the operation.
     
     :param:  paramDict The dictionary(keys/values) which needs to be posted.
     
     :param:  is_synchronous A bool variable if "true" service will be hit on main thread otherwise on background.
     
     :param:  url It contains the url link which is used to hit service.
     */
    
    //MARK: post & get request
    func postService(_ completion_block:CompletionBlock?, error_block:ErrorBlock?,paramDict:AnyObject!,is_synchronous:Bool!,url:String){
        
        checkInternetConnection()
        
        if (isInternetActive == true) {
            
            let request : NSMutableURLRequest? = self.createPostRequest("POST", url_string:url, params:paramDict)
            
            self.makeURLRequest(request as URLRequest!, isSynchronous: is_synchronous, completion_block: {(responseObject) in
                
                let responseO = responseObject as! NSDictionary
                
                
               // print(responseO)
                
                if (responseO.value(forKey: "response") != nil){
                
                completion_block!(self.parseResponseForSuccessBlock((responseO.value(forKey: "response"))! as AnyObject, url: responseObject?.value(forKey: "requestUrl") as! String))
                }
                else{
                    
                    let obj = ""
                    
                    completion_block!(self.parseResponseForSuccessBlock(obj as AnyObject, url: responseObject?.value(forKey: "requestUrl") as! String))
                }
                
                return
                }, error_block: {(responseError) in
                    
                    error_block!(responseError as NSError!)
                    return
            })
        }
        else {
            //Global.sharedInstance.showToast("No internet connection")
            //SwiftNotice.clear()
            
        }
        
    }
    
    func checkInternetConnection() {
        
        internetConnectionReach = Reachability.reachabilityForInternetConnection()
        
        var netStatus: Reachability.NetworkStatus!
        
        netStatus = internetConnectionReach.currentReachabilityStatus
        
        if(netStatus == Reachability.NetworkStatus.notReachable) {
            isInternetActive = false;
        }
        else {
            isInternetActive = true
        }
    }
    
    /**
     getService is used to hit service with "GET" method.
     
     :param:  completion_block This is used to notify that service has been completed.
     
     :param:  error_block This is used to notify that an error has occured during the operation.
     
     :param:  paramDict In "GET" service this param is optional.
     
     :param:  is_synchronous A bool variable if "true" service will be hit on main thread otherwise on background.
     
     :param:  url It contains the url link which is used to hit service.
     */
    
    func getService(_ completion_block:CompletionBlock!,error_block:ErrorBlock!,is_synchronous:Bool!,url:String,paramDict:String){
        
        
        let request : URLRequest! = self.createGetRequest("GET", url_string: url, params:paramDict)
        
        self.makeURLRequest(request, isSynchronous: is_synchronous, completion_block: {(responseObject) in
            
            completion_block(self.parseResponseForSuccessBlock(responseObject!,url: responseObject?.value(forKey: "requestUrl") as! String))
            
            return
            }, error_block: {(responseError) in
                
                error_block(responseError)
                return
        })
        
    }
    
    
    /**
     postOfflineData is used to hit service with "POST" method.
     
     :param:  completion_block This is used to notify that service has been completed.
     
     :param:  error_block This is used to notify that an error has occured during the operation.
     
     :param:  paramDict In "GET" service this param is optional.
     
     :param:  is_synchronous A bool variable if "true" service will be hit on main thread otherwise on background.
     
     :param:  url It contains the url link which is used to hit service.
     */
    
    func postOfflineData(_ completion_block:offlineBlock!, error_block:ErrorBlock!,paramDict:AnyObject?,is_synchronous:Bool!,url:String){
        
        let request : CustomNSMutableURLRequest! = self.createOfflinePostRequest("POST", url_string: url, params:paramDict)
        request.requestDataInfo = paramDict
        self.makeURLOfflineRequest(request, isSynchronous: is_synchronous, completion_block: { (responseObject,
            inputData) -> Void in
            
            completion_block?(self.parseResponseForSuccessBlock((responseObject!.value(forKey: "response")) as AnyObject, url:
                responseObject!.value(forKey: "requestUrl") as! String),inputData)
            
        }) { (error) -> Void in
            
        }
    }
    
    //  MARK:Parse Api request
    
    /**
     parseResponseForSuccessBlock is used to parse the data which is received in response.
     
     :param:  responseDict contains the data which is received in response.
     
     :param:  url contains the url which we have used to hit the service.
     
     :returns: AnyObject it returns the data in dictionary(keys/values).
     */
    
    func parseResponseForSuccessBlock(_ responseDict:AnyObject, url:String)->AnyObject{
        
        var parsedDict = NSDictionary()
        
      // print(responseDict)

        
//        if(responseDict.allKeys.count != nil )
//        {
//             parsedDict = responseDict as! NSDictionary
//        }
        
        
        if responseDict is String {
            
            print("string return")
        }
        else{
            
            if(responseDict != nil )
            {
                
                parsedDict = responseDict as! NSDictionary
                
            }
                
            else{
                return responseDict
            }
        }
    
        
        
        return parsedDict
    }
    
    /**
     parseResponse is used to parse the data which is received in response.
     
     This response block works similar to the above one , the only difference is that you can apply checks of status code here only rather than in particular class.
     
     :param:  responseDict contains the data which is received in response.
     
     :param:  url contains the url which we have used to hit the service.
     
     :returns: AnyObject it returns the data in dictionary(keys/values).
     */
    
    func parseResponse(_ responseObject: AnyObject) -> (Int,NSDictionary?) {
        
        let statusCode = ((responseObject["response_code"])!! as AnyObject).doubleValue
        // (responseObject.valueForKey("response_code"))! as! Int
        var responseDict = NSDictionary()
        
        if (statusCode == 1)
        {
            if(responseObject.value(forKey: "result") != nil)
            {
                //if(responseObject["result"]!! as AnyObject)
                //{
                if(responseObject.value(forKey: "result") != nil){
                    
                    responseDict = responseObject.value(forKey: "result") as! NSDictionary
                    
                    //                        if(responseObject["result"] != ""){
                    //                            responseDict = responseObject["result"] as! NSDictionary
                    //                        }
                    
                }
                else {
                    
                }
                
                //}
            }
        }
        else {
            
            if(responseObject.value(forKey: "response_message") != nil && responseObject.value(forKey: "response_message") as? String != "") {
                let dict1 = NSMutableDictionary()
                dict1.setValue(responseObject.value(forKey: "response_message") as? String, forKey: "response_message")
                
                responseDict = dict1 as NSDictionary
            }
        }
        
        return (Int(statusCode!),responseDict)
    }
    
}



// MARK: URL requests

extension ServerCommunication{
    
    /**
     createGetRequest is used to make url request with "GET" method.
     
     :param:  httpMethod It can be "GET" or "POST".
     
     :param:  params The dictionary(keys/values) which needs to be posted.
     
     :param:  url_string It contains the url link which is used to hit service.
     
     :returns: URLRequest returns the created request.
     */
    
    func createGetRequest(_ httpMethod:String, url_string:String,params:String) ->URLRequest{
        let urlStr:String = url_string // Your Normal URL String
        
       // print(urlStr)
        
        
        let NSHipster = URL.init(string: "\(url_string.replacingOccurrences(of: " ", with: ""))")
        
        
        //print(NSHipster!)
        //let url:URL! = URL(string: urlStr)// Creating URL
        let request:NSMutableURLRequest = NSMutableURLRequest(url: NSHipster!)// Creating Http Request
        request.httpMethod = httpMethod
        // Creating NSOperationQueue to which the handler block is dispatched when the request completes or failed
        request.timeoutInterval = 30
        
        return request as URLRequest
    }
    
    /**
     createPostRequest is used to make url request with "POST" method.
     
     :param:  httpMethod It can be "GET" or "POST".
     
     :param:  params The dictionary(keys/values) which needs to be posted.
     
     :param:  url_string It contains the url link which is used to hit service.
     
     :returns: NSMutableURLRequest returns the created request.
     */
    
    func createPostRequest(_ httpMethod:String, url_string:String,params:AnyObject!) ->NSMutableURLRequest{
        
        let url : NSString = url_string as NSString
        let urlStr : NSString = url.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
        let searchURL : URL = URL(string: urlStr as String)!
      //  print(searchURL)
        
        //let url:NSURL! = NSURL(string: urlStr)// Creating URL
        let request:NSMutableURLRequest = NSMutableURLRequest(url: searchURL)// Creating Http Request
        request.httpMethod = httpMethod
        
        
        if (params != nil){
            if ((params as? NSArray) != nil){
                do {
                    var bodyData:Data!
                    bodyData = try JSONSerialization.data(withJSONObject: params, options: [])
                    request.httpBody = bodyData
                    
                } catch {
                    
                    request.httpBody = nil
                }
            }
            else if ((params as? NSDictionary) != nil){
                
                do {
                    var bodyData:Data!
                    bodyData = try JSONSerialization.data(withJSONObject: params, options: [JSONSerialization.WritingOptions(rawValue: 0)])
                    request.httpBody = bodyData
                    
                } catch {
                    
                    request.httpBody = nil
                }
            }
            else if (params is String){
                let bodyData: String! = params as! String;
                request.httpBody = bodyData.data(using: String.Encoding.utf8);
            }
        }
        request.timeoutInterval = 30
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        var postLength = ""
        
      ///  print(params)
        
        
        if(params != nil){
            
            postLength = String(format: "%d", urlStr.length)
        }else{
            postLength = String(format: "%d", urlStr.length)
        }
        
        
        //var postLength : NSString = NSString(StringInterpolationConvertible: request.httpBody?.count)
        request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
        
 
        // Creating NSOperationQueue to which the handler block is dispatched when the request completes or failed
        return request
        
    }
    
    /**
     makeURLRequest is used to create a request.
     
     :param:  request contains request which was created in create request method.
     
     :param:  isSynchronous bool variable to know wether request needs to be on main thread or background.
     
     :param:  completion_block completion callback if success.
     
     :param:  error_block error callback if an error is occured.
     */
    
    func makeURLRequest(_ request:URLRequest!, isSynchronous:Bool!, completion_block:CompletionBlock!, error_block:ErrorBlock!){
        
        DispatchQueue.main.async { 
            
            
            let session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil )
            
            session.dataTask(with: request) {(data, response, error) -> Void in
                if let data = data {
                    let json = try? JSONSerialization.jsonObject(with: data, options: [])
                    
                   // print(response)
                   // print(json)
                   
                    if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
                        
                        let finalDict = NSMutableDictionary()
                        finalDict.setValue(json, forKey:"response")
                        finalDict.setValue(request.url?.absoluteString, forKey:"requestUrl")
                        completion_block(finalDict)
                        
                    } else {
                        error_block(error as? NSError)
                    }
                }
                }.resume()
        }
        
        
        
        //sema.wait()
        
        /*
         let queue: OperationQueue = OperationQueue()
         
         NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: queue,completionHandler: { (response:URLResponse?, responseData:Data?, connection_error: NSError?) -> Void in
         
         if connection_error != nil{
         
         error_block(connection_error)
         return;
         }
         var jsonResult:AnyObject!
         do {
         let finalDict = NSMutableDictionary()
         jsonResult = try JSONSerialization.jsonObject(with: responseData!, options: []) as AnyObject
         finalDict.setValue(jsonResult, forKey:"response")
         finalDict.setValue(request.url?.absoluteString, forKey:"requestUrl")
         completion_block(finalDict)
         } catch {
         //              let error : NSError = NSError()
         
         //              error.description = "got css in response"
         error_block(connection_error)
         // failure
         return;
         }
         
         return;
         } as! (URLResponse?, Data?, Error?) -> Void)
         
         */
    }
    
    /**
     makeURLOfflineRequest is used to create a request.
     
     :param:  request contains request which was created in create request method.
     
     :param:  isSynchronous bool variable to know wether request needs to be on main thread or background.
     
     :param:  completion_block completion callback if success.
     
     :param:  error_block error callback if an error is occured.
     */
    
    func makeURLOfflineRequest(_ request:CustomNSMutableURLRequest!, isSynchronous:Bool!, completion_block:offlineBlock!, error_block:ErrorBlock!){
        
        let queue: OperationQueue = OperationQueue()
        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue: queue,completionHandler: { (response:URLResponse?, responseData:Data?, connection_error: NSError?) -> Void in
            
            if connection_error != nil{
                
                error_block(connection_error)
                return;
            }
            var jsonResult:AnyObject!
            do {
                let finalDict = NSMutableDictionary()
                jsonResult = try JSONSerialization.jsonObject(with: responseData!, options: []) as AnyObject
                finalDict.setValue(jsonResult, forKey:"response")
                finalDict.setValue(request.url?.absoluteString, forKey:"requestUrl")
                completion_block(finalDict,request.requestDataInfo)
            } catch {
                // failure
                return;
                
            }
            
            return;
            } as! (URLResponse?, Data?, Error?) -> Void)
    }
    
    /**
     createOfflinePostRequest is used to create a offline request.
     
     :param:  httpMethod contains string about method type "GET" or "POST".
     
     :param:  url_string contains the url string.
     
     :param:  params contains dictionary.
     
     :return: CustomNSMutableURLRequest class object
     */
    
    func createOfflinePostRequest(_ httpMethod:String, url_string:String,params:AnyObject!) ->CustomNSMutableURLRequest{
        let urlStr:String = url_string  // Your Normal URL String
        let url:URL! = URL(string: urlStr)// Creating URL
        let request:CustomNSMutableURLRequest = CustomNSMutableURLRequest(url: url)// Creating Http Request
        request.httpMethod = httpMethod
        
        
        if (params != nil){
            if ((params as? NSArray) != nil){
                do {
                    var bodyData:Data!
                    bodyData = try JSONSerialization.data(withJSONObject: params, options: [])
                    request.httpBody = bodyData
                    
                } catch {
                    
                    request.httpBody = nil
                }
            }
            else if ((params as? NSDictionary) != nil){
                
                do {
                    var bodyData:Data!
                    bodyData = try JSONSerialization.data(withJSONObject: params, options: [JSONSerialization.WritingOptions(rawValue: 0)])
                    request.httpBody = bodyData
                    
                } catch {
                    
                    request.httpBody = nil
                }
            }
            else if (params is String){
                let bodyData: String! = params as! String;
                request.httpBody = bodyData.data(using: String.Encoding.utf8);
            }
        }
        request.timeoutInterval = 30
        
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let postLength = String(format: "%d", params.data!.count)
        
        request.setValue(postLength as String, forHTTPHeaderField: "Content-Length")
        
        return request
    }
    
    
}

/**
 CustomNSMutableURLRequest class is used to create a offline request.
 */

class CustomNSMutableURLRequest: NSMutableURLRequest {
    
    var requestDataInfo:AnyObject!
    
}

