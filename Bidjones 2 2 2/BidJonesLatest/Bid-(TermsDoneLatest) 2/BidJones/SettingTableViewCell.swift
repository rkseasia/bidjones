//
//  SettingTableViewCell.swift
//  BidJones
//
//  Created by Harpreet Singh on 20/03/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {
    
    @IBOutlet var btn_ImgIcon: UIButton!
    // @IBOutlet var img_Icon: UIImageView!
    @IBOutlet var lbl_Title: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
