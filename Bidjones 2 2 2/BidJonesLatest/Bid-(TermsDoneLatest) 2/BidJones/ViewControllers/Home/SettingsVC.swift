//
//  SettingsVC.swift
//  BidJones
//
//  Created by Harpreet Singh on 20/03/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {
    
    @IBOutlet var tbl_Settings: UITableView!
    /**
     *  Array containing menu options
     */
    var arrayMenuOptions = [Dictionary<String,String>]()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = KSETTINGS
        self.navigationController?.navigationBar.topItem?.title = "";
        tbl_Settings.separatorColor = .clear
        tbl_Settings.tableFooterView = UIView.init()
        updateArrayMenuOptions()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func updateArrayMenuOptions(){
        arrayMenuOptions.append([Ktitle:KMyProfile, Kicon:Kmy_profile_ic])
        arrayMenuOptions.append([Ktitle:KStuffSales, Kicon:Kstuff_ic])
        arrayMenuOptions.append([Ktitle:KServicesSales, Kicon:Kservices_ic])
        arrayMenuOptions.append([Ktitle:KMusicSales, Kicon:Kmusic_ic])
        arrayMenuOptions.append([Ktitle:KBookSales, Kicon:Kbook_ic])
        arrayMenuOptions.append([Ktitle:KVideoSales, Kicon:Kvideo_ic])
        tbl_Settings.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
}

extension SettingsVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SettingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SettingTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        
        switch indexPath.row {
        case 0:
            cell.btn_ImgIcon.imageEdgeInsets = UIEdgeInsets(top: 13, left:14, bottom: 13, right: 14)
            break
        case 1:
            cell.btn_ImgIcon.imageEdgeInsets = UIEdgeInsets(top: 10, left:10, bottom: 10, right: 10)
            break
        case 2:
            cell.btn_ImgIcon.imageEdgeInsets = UIEdgeInsets(top: 12, left:12, bottom: 12, right: 12)
            break
        case 3:
            cell.btn_ImgIcon.imageEdgeInsets = UIEdgeInsets(top: 10, left:12, bottom: 10, right: 12)
            break
        case 4:
            cell.btn_ImgIcon.imageEdgeInsets = UIEdgeInsets(top: 11, left:14, bottom: 11, right: 14)
            break
        case 5:
            cell.btn_ImgIcon.imageEdgeInsets = UIEdgeInsets(top: 15, left:9, bottom: 15, right: 9)
            break
        default:
            break
        }
        cell.btn_ImgIcon.setImage(UIImage(named: arrayMenuOptions[indexPath.row][Kicon]!), for: .normal)
        cell.lbl_Title.text = arrayMenuOptions[indexPath.row][Ktitle]!
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
         KCommonFunctions.PushToContrller(from: self, ToController: .MyProfile, Data: nil)
        }
        if indexPath.row == 1 {
            
        KCommonFunctions.PushToContrller(from: self, ToController: .StuffSales, Data: nil)
            
        }
        
        if indexPath.row == 2 {
            
            KCommonFunctions.PushToContrller(from: self, ToController: .ServiceSales, Data: nil)
            
        }
        
        if indexPath.row == 3 {
            
            KCommonFunctions.PushToContrller(from: self, ToController: .MusicSales, Data: nil)
            
        }
        
        if indexPath.row == 4 {
            
            KCommonFunctions.PushToContrller(from: self, ToController: .BookSales, Data: nil)
            
        }
    
        
        if indexPath.row == 5 {
            
            KCommonFunctions.PushToContrller(from: self, ToController: .VideoSales, Data: nil)
            
        }

}

}
