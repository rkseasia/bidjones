//
//  ChatRoomVC.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/3/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import JSQMessagesViewController

import Firebase

import FirebaseDatabase
//import FirebaseDatabase
//import FirebaseFirestore
import FirebaseFirestore




struct User {
    let id: String
    let name: String
}

var anotherUserID_global : String?


class ChatRoomVC: JSQMessagesViewController {
    
    //1: kuldeep
    //2: Rakesh
    
    //MARK:- ALL VARIABLES
    var talkData               :           TalkData?
    var login_userID           :           String?
    var anotherUserID          :           String?
    var itemID                 :           String?
    let user1                  =           User(id: "236", name: "Rakesh")
    let user2                  =           User(id: "237", name: "kuldeep")
    var dictionary             =           Dictionary<String ,  Any>()
    var parm3                  :           [String : Any]?
    var db                     =           Firestore.firestore()
    var messageArray           =           Array<Any>()
    var retrivedArray          =           NSMutableArray()
    var anotherUserMsgArray    =           NSMutableArray()
    var anotherUserName        :           String?
    var imageAnotherUser       :           String?
    var totalMsgArray          =           NSMutableArray()
    var isNotificationEntry    =           false
    var retrivedArrayForAnother =          Array<Any>()
    var uid                    =           String()
    let def                    =           UserDefaults.standard
    var array                  =           [String]()
    var messagesSortedArray    =           [[String : AnyObject]]()
    var currentUser: User {
        return user1
    }
    var pairID1                =           ""
    var pairID2                =           ""
    var messages               =           [JSQMessage]()
}

extension ChatRoomVC {
    
    //MARK:- SEND NOTIFICATION METHOD
    func sendNotificationMassage(url: String, parm : [String: Any]) {
        
        ChatRoom.sharedManager.PostApiForNotification(url: url, parameter: parm, Target: self, completionResponse: {
            (response) in
            
            print("my Response is chat : \(response)")
            
        },  completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    
                    let url = "/notifyNewChat"
                    self.sendNotificationMassage(url: url, parm : self.parm3!)
                    // self.getDataMusicList(genreId: 0)
                    //print(resonse)
                    //   self.GetRecieveBidList()
                })
            }
            else if statusCode == 201
            {
                
                
            }
        }, completionError: { (error) in
            // self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            
        }, networkError: {(error) in
            //  self.showAlertMessage(titleStr: KMessage, messageStr: error)
            
        })

        
        
    }
    
    
    
    @IBAction func BackAction(_ sender: Any) {
        
        if self.isNotificationEntry == true {
            KCommonFunctions.SetRootViewController(rootVC:.HomeNavigation)
        }
        else {
           self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    // MARK: - DID PRESS SEND
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        
        let a = true
        if a == true {
            let db = Firestore.firestore()
            let settings = db.settings
            settings.areTimestampsInSnapshotsEnabled = true
            db.settings = settings
        }
        var db : DocumentReference!
        let date2 = FieldValue.serverTimestamp()
        print("this is my value date : \(date2)")
        let readDate = date2.hashValue
        print("here is read date : \(readDate)")
        let milisecondsDate = readDate / 1000
        let toffy = "\(readDate)"
        var com =  Double(milisecondsDate)
        com = NSDate().timeIntervalSince1970
        let currentDate = Date().toMillis()
        print("your current date : \(currentDate)")
        var myCom = Double(milisecondsDate)
        var newCom = Double(currentDate!)
        print("This is newCom:\(newCom)")
       // newCom = NSDate().timeIntervalSince1970
        let result = self.getDateStringFromUTC(time :com)
        print("your result : \(result)")
        print("date os messages :- \(result)")
        print("This send \(senderId!) and \(text!)")
        let timeStamp =  result
        print("Here is timeStamp : \(timeStamp)")
        let loginID1 = Int(login_userID!)
        
        
        //Login User
        
        
        print(loginID1!)
        print("\(pairID1)|\(pairID2)|\(itemID!)")

        var data = ["created_at": date2, "fromuser" : loginID1! , "is_seen" : 0 , "message" : text!, "pair_id": "\(pairID1)|\(pairID2)|\(itemID!)", "updated_at" : date2 ] as [String : Any]
        print(data)
        db = Firestore.firestore().collection("TestChat").document()
        db.setData(data, merge: true)
        let url = "/notifyNewChat"
        let loginID = Int(login_userID!)
        let loginID2 = NSNumber(value:loginID!)
        let itemID1 = Int(itemID!)
        let anotherUser_id = Int(anotherUserID!)
       let anotherUserid1 = NSNumber(value:anotherUser_id!)
        print("anotherUser Id1 : \(anotherUserid1) , \(itemID1) , \(loginID2)")
        self.parm3 = ["to" : "\(anotherUserid1)" ,"sender_id": "\(loginID2)", "item_id" : "\(itemID1!)" , "message" : text! ]
        sendNotificationMassage(url:url, parm : parm3!)
        print("This send \(senderId!) and \(text!)")
        let message = JSQMessage(senderId: login_userID!, senderDisplayName: "", date: result , text: text)
         messages.append(message!)
         finishSendingMessage()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForCellBottomLabelAt indexPath: IndexPath) -> CGFloat
    {
        
        //collectionViewLayout.messageBubbleSizeForItem(at: <#T##IndexPath!#>)
        print("This is the height of the bottom label ")
        return 20.0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        print("here you get \(message.senderId!) and  \(login_userID!)")
        var com =  collectionView.layoutAttributesForItem(at: indexPath)
        if (message.senderId! != anotherUserID!) {
            cell.textView?.textColor = UIColor.white
            let attributedString = NSAttributedString(attributedString: JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date))
            let dateString = attributedString.string
            cell.cellBottomLabel.text = dateString
            print("This is the value of the date String\(dateString)")
            cell.textView?.linkTextAttributes = [
                NSAttributedStringKey.foregroundColor.rawValue: UIColor.white,
                NSAttributedStringKey.underlineStyle.rawValue: NSUnderlineStyle.styleSingle.rawValue
            ]
            let oldOffsetReversed: CGFloat = collectionView.contentSize.height - collectionView.contentOffset.y
                    // let pageSize = fetchNextPage() //reloadData is called inside
            view.layoutIfNeeded()
            let offset: CGFloat = collectionView.contentSize.height - oldOffsetReversed
            collectionView.contentOffset = CGPoint(x: collectionView.contentOffset.x, y: offset)
            
        } else {
            
            cell.textView?.textColor = UIColor.white
            let attributedString = NSAttributedString(attributedString: JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: message.date))
            let dateString = attributedString.string
            cell.cellBottomLabel.text = dateString
            cell.cellBottomLabel.textInsets.right = 15.0
            print("This is the value of the date String\(dateString)")
            cell.textView?.linkTextAttributes = [
                NSAttributedStringKey.foregroundColor.rawValue: UIColor.white,
                NSAttributedStringKey.underlineStyle.rawValue: NSUnderlineStyle.styleSingle.rawValue
            ]
           let oldOffsetReversed: CGFloat = collectionView.contentSize.height - collectionView.contentOffset.y
               //  let pageSize = fetchNextPage() //reloadData is called inside
            view.layoutIfNeeded()
            let offset: CGFloat = collectionView.contentSize.height - oldOffsetReversed
             collectionView.contentOffset = CGPoint(x: collectionView.contentOffset.x, y: offset)
            
        }
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat
    {
        return messages[indexPath.item].senderId == senderId ? 0 : 15
    }
    
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
//        return 60.0
//    }
//    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
//        return 60.0
//    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let message = messages[indexPath.row]
        let messageUsername = message.senderDisplayName
        
        return NSAttributedString(string: messageUsername!)
    }
    
    //    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
    //        return 15
    //    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let bubbleFactory = JSQMessagesBubbleImageFactory()
        
        
        
      //  bubbleFactory?.compressImage(image:  , compressionQuality: 0.1)
        
        let message = messages[indexPath.row]
        
        if login_userID! == message.senderId! {
            return bubbleFactory?.outgoingMessagesBubbleImage(with: .gray)
        } else {
            return bubbleFactory?.incomingMessagesBubbleImage(with: .red)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.row]
    }
}

extension ChatRoomVC {
    
    @objc func didTapBackButton(sender: AnyObject){
      
        anotherUserID_global = nil
        if self.isNotificationEntry == true {
            notificationData = nil
            
            KCommonFunctions.SetRootViewController(rootVC:.HomeNavigation)
        }
        else {
            self.navigationController?.popViewController(animated: true)
        }
         notificationData = nil
        
    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        view.layoutIfNeeded()
//        collectionView.collectionViewLayout.invalidateLayout()
//
//        if autoScrollsToMostRecentMessage {
//            firstLayout = true
//            DispatchQueue.main.async(execute: {
//                self.firstLayout = false
//            })
//        }
//    }
//
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        if firstLayout && automaticallyScrollsToMostRecentMessage {
//            scrollToBottom(animated: false)
//        }
//    }
//
    func resizeImage(image: UIImage, withSize: CGSize) -> UIImage {
        
        var actualHeight: CGFloat = image.size.height
        var actualWidth: CGFloat = image.size.width
        let maxHeight: CGFloat = withSize.width
        let maxWidth: CGFloat = withSize.height
        var imgRatio: CGFloat = actualWidth/actualHeight
        let maxRatio: CGFloat = maxWidth/maxHeight
        let compressionQuality = 0.5//50 percent compression
        
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if(imgRatio < maxRatio) {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight
                actualWidth = imgRatio * actualWidth
                actualHeight = maxHeight
            } else if(imgRatio > maxRatio) {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth
                actualHeight = imgRatio * actualHeight
                actualWidth = maxWidth
            } else {
                actualHeight = maxHeight
                actualWidth = maxWidth
            }
        }
        
        let rect: CGRect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let image: UIImage  = UIGraphicsGetImageFromCurrentImageContext()!
        let imageData = UIImageJPEGRepresentation(image, CGFloat(compressionQuality))
        UIGraphicsEndImageContext()
        
        let resizedImage = UIImage(data: imageData!)
        return resizedImage!
        
    }
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
          self.automaticallyScrollsToMostRecentMessage = true
     //   print("This is mine : \(talkData?.username)")
        
        
        notificationData = nil
        if let data = notificationData {
            
            if let data1 = data as? [String : Any] {
                
                let newData = data1["data"] as? [String : Any]
                let item = newData!["item"] as? [[String : Any]]
            
                let anotheruser_id =  item![0]["sender_id"] as? Int
                    //item![0]["buyer_id"] as? Int
                
                let itemID_product = item![0]["id"] as? Int
                
                let imgName = item![0]["imgName"] as? String
               
                self.imageAnotherUser = imgName!
                
                print("get another user id :\(anotheruser_id!)")
                 self.anotherUserID = "\(anotheruser_id!)"
                
                let userId = newData!["user_id"] as? Int
                
                let id = UserDefaults.standard
                
                let loginUserID = id.getUserID()
                
                let loginID = loginUserID as! Any
                let login1 = loginID as? Int
                
                self.itemID = "\(itemID_product!)"
                
                self.login_userID = "\(userId!)"
                
                self.isNotificationEntry = true
                
            var img_Logo = UIImageView()
            
            // img_Logo = img_Logo.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            //UIImageView.init(frame: CGRect(x: 0, y: 0, width: 10 , height: 25))
            if let imageNameString = imageAnotherUser {
                let url = URL(string:imageNameString)
                
                //            if let data = try? Data(contentsOf: url!)
                //            {
                //                let imageName = UIImage(data: data)
                img_Logo.frame =  CGRect(x: 0, y: 0, width:40, height: 40)
                //   img_Logo.image = imageName!
                // img_Logo.image?.size.width = 25
                
                if let url1 = url as? URL {
                    
                    print("my url for profile : \(url1)")
                    
                    img_Logo.sd_setImage(with: url1)
                    img_Logo.layer.borderWidth = 1
                    img_Logo.layer.masksToBounds = false
                    img_Logo.layer.borderColor = UIColor.black.cgColor
                    img_Logo.layer.cornerRadius = img_Logo.frame.width/2
                    img_Logo.clipsToBounds = true
                    
                    if let image = img_Logo.image {
                        
                        img_Logo.image = resizeImage(image: image, withSize: CGSize(width: 40, height: 40))
                    }
                    
                }
            }
            
            
            
            let radioiImg:UIImage = #imageLiteral(resourceName: "back_arrow")
            
            let radioView:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: radioiImg.size.width + 4, height: radioiImg.size.height))
            radioView.addTarget(self, action:#selector(self.didTapBackButton), for: .touchUpInside)
            radioView.setImage(radioiImg, for: .normal)
            let radioButton:UIBarButtonItem = UIBarButtonItem(customView: radioView)
            
            //   let profileimage =
            
            let NotifyButton = UIBarButtonItem(customView: img_Logo)
            
            let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            fixedSpace.width = 20.0
            
            
            navigationItem.leftBarButtonItems = [radioButton,fixedSpace,NotifyButton]
            
            self.title = "Rakesh"
            
           // UserDefaults.standard.set(nil, forKey: "NotificationData")
            
            notificationData = nil
            }
            
            
            
            
        }
        else {
            let notifiImg:UIImage = UIImage(named: Kadd)!
            var img_Logo = UIImageView()
            
            // img_Logo = img_Logo.image!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            //UIImageView.init(frame: CGRect(x: 0, y: 0, width: 10 , height: 25))
            if let imageNameString = talkData?.userImage {
                let url = URL(string:imageNameString)
                
                //            if let data = try? Data(contentsOf: url!)
                //            {
                //                let imageName = UIImage(data: data)
                img_Logo.frame =  CGRect(x: 0, y: 0, width:40, height: 40)
                //   img_Logo.image = imageName!
                // img_Logo.image?.size.width = 25
                
                if let url1 = url as? URL {
                    
                    print("my url for profile : \(url1)")
                    
                    img_Logo.sd_setImage(with: url1)
                    img_Logo.layer.borderWidth = 1
                    img_Logo.layer.masksToBounds = false
                    img_Logo.layer.borderColor = UIColor.black.cgColor
                    img_Logo.layer.cornerRadius = img_Logo.frame.width/2
                    img_Logo.clipsToBounds = true
                    
                    if let image = img_Logo.image {
                        
                        img_Logo.image = resizeImage(image: image, withSize: CGSize(width: 40, height: 40))
                    }
                    
                }
            }
            
            
            
            let radioiImg:UIImage = #imageLiteral(resourceName: "back_arrow")
            
            let radioView:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: radioiImg.size.width + 4, height: radioiImg.size.height))
            radioView.addTarget(self, action:#selector(self.didTapBackButton), for: .touchUpInside)
            radioView.setImage(radioiImg, for: .normal)
            let radioButton:UIBarButtonItem = UIBarButtonItem(customView: radioView)
            
            //   let profileimage =
            
            let NotifyButton = UIBarButtonItem(customView: img_Logo)
            
            let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            fixedSpace.width = 20.0
            
            
            navigationItem.leftBarButtonItems = [radioButton,fixedSpace,NotifyButton]
            
            if let name = talkData?.username {
            self.title = name
            }
            
        }
        
        
        
        
       // let notificationButton = SSBadgeButton()
      
        
        
        self.inputToolbar.contentView.leftBarButtonItem = nil
        
        print("FirstId login: \(login_userID!)")
        print("SeconedId Another: \(anotherUserID!)")
        
        let db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        
        self.navigationItem.setHidesBackButton(true, animated: false)
        
        
        var login = Int(login_userID!)
        var anotherUser = Int(anotherUserID!)
        
        
        
        if (login! < anotherUser!) {
            
            pairID1 = "\(login!)"
            pairID2 = "\(anotherUser!)"
        }
        else {
            pairID1 = "\(anotherUser!)"
            pairID2 = "\(login!)"
            
        }
   
        
        dictionary =  ["senderId": String(), "message": String()]
        // 1 : kuldeep
        // 2 : Rakesh
        // tell JSQMessagesViewController
        // who is the current user
        self.senderId = login_userID!
        self.senderDisplayName = "kuldeep"
        
        
//        let uid1 = def.value(forKey: "uid")
//        let uid2 = "\(uid1!)"
      //  messagesSortedArray.removeAll()
        
        
//        self.retrivedArray.removeAllObjects()
//        self.anotherUserMsgArray.removeAllObjects()
//        self.totalMsgArray.removeAllObjects()
//        self.messagesSortedArray.removeAll()
        
        anotherUserID_global = self.anotherUserID!
       
        print("now pairID1 :\(pairID1) and pairID2 : \(pairID2)  and itemID:\(itemID!)")
   //  self.retrivedArray.removeAllObjects()
        let login1 = Int(login_userID!)
        //db.collection(<#T##collectionPath: String##String#>)
        db.collection("TestChat").whereField("fromuser", isEqualTo: login1!).whereField("pair_id", isEqualTo: "\(pairID1)|\(pairID2)|\(itemID!)").addSnapshotListener { querySnapshot, err in
            
            
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                
                print("Here is entery level of chating of Rakesh")
                
                print("whole data is : \(querySnapshot?.documents)")
                
                for document in querySnapshot!.documents {
                    let docId = document.documentID
                    
                    print("retrived Data from : \(document)")
                    print("123456789")
                    
                    let message = document.get("message") as! String
                    var userid = document.get("fromuser")
                    let time = document.get("created_at")
                    
                    print("your message:\(time)")
                   let timestampMY = document.get("created_at", serverTimestampBehavior: .estimate)
                    
                    print("here my time and date : \(timestampMY)")
                    
                    print("your userID : \(userid!)")
                    print("login message : \(message)")
                  //  self.self.retrivedArray.removeAllObjects()
                    
                    
                    var string_Time = "\(timestampMY!)"
                    
                    var savedDate = Date()
                    
                   
                    if string_Time != "" {
                        
                        let array = string_Time.components(separatedBy: " ")

                        let firstArray = array[0]
                        let secondArray = array[1]
                        
                        print("now you get second Array: \(secondArray)")

                        let anothertimeStamp = secondArray.components(separatedBy: "=")
                        
                        let anotherone = anothertimeStamp[0]
                        let timestamp4 = anothertimeStamp[1]

                        print("see timeStamp : \(timestamp4)")
                        
                        
                        var com =  Double(timestamp4)
                        print("your com : \(com)")
                     // com = NSDate().timeIntervalSince1970
                        print("this is milliseconed \(com)")
                        //  let result = com?.getDateStringFromUTC()
                        let result = self.getDateStringFromUTC(time: com!)
                        print("you check here time : \(result)")
                        
                        savedDate = result
                        
                    }
                    
                    
                    print(" you id is here first block:\(userid!)")
                    
                    print("whole message : \(message)")
                    //                    let latMin = document.get("latMin") as! String
                    //                    let lonMax = document.get("lonMax") as! String
                    //                    let lonMin = document.get("lonMin") as! String
                    
                    // let  retrivedDictionary = NSMutableDictionary
                    
                    let  retrivedDictionary = ["senderId" : userid!, "message" :  message , "time" : savedDate]
                    
                    print("Retrived dictionary:- \(retrivedDictionary)")
                    
                    self.retrivedArray.add(retrivedDictionary)
                    
                  //  totalMsgArray.add()
                    //= retrivedDictionary
                    //
                    //.append(retrivedDictionary)
                    print("end of first block of Rakesh")
                    //    self.messages = self.getMessages()
                    //  self.collectionView.reloadData()
                }
                
                
                
            }
            
            
        }
        
        
        // Another User chat message retrived
        print("here another user id :\(anotherUser!)")
        print("\(pairID1)|\(pairID2)|\(itemID!)")
        let anotherUser1 = Int(anotherUserID!)
        
        db.collection("TestChat").whereField("fromuser", isEqualTo: anotherUser1!).whereField("pair_id", isEqualTo: "\(pairID1)|\(pairID2)|\(itemID!)").addSnapshotListener { querySnapshot, err in
            
            
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                
                print("here snapshot\(querySnapshot)")
                print("You are going to enter Kuldeep")
                print("whole data is : \(querySnapshot?.documents)")
                
                for document in querySnapshot!.documents {
                    let docId = document.documentID
                    
                    print("retrived Data from : \(document)")
                    
                    
                    let message = document.get("message") as! String
                    var userid = document.get("fromuser")
                   // let time = document.get("created_at")
                    
                    print("Another user message : \(message)")
                    
                    let timestampMY = document.get("created_at", serverTimestampBehavior: .estimate)
                    
                    print("here my time and date : \(timestampMY)")
                    
                    print("another userID : \(userid!)")
                    print("anotherUser message : \(message)")
                  //  self.retrivedArray.removeAllObjects()
                    
                    
                    var string_Time = "\(timestampMY!)"
                    
                    var savedDate = Date()
                    
                 //   self.anotherUserMsgArray.removeAllObjects()
                    if string_Time != "" {
                        
                        let array = string_Time.components(separatedBy: " ")

                        let firstArray = array[0]
                        let secondArray = array[1]

                        print("now you get second Array: \(secondArray)")

                        let anothertimeStamp = secondArray.components(separatedBy: "=")

                        let anotherone = anothertimeStamp[0]
                        let timestamp4 = anothertimeStamp[1]

                        print("see timeStamp : \(timestamp4)")
                        
                        
                        var com =  Double(timestamp4)
                        
                       //  com = NSDate().timeIntervalSince1970
                        print("this is milliseconed \(com)")
                        //   let result = com?.getDateStringFromUTC()
                        
                        let result = self.getDateStringFromUTC(time: com!)
                        print("you check here time : \(result)")
                        
                        savedDate = result
                        
                    }
                    
                    print(" you id is here :\(userid!)")
                    
                    print("whole message : \(message)")
                    //                    let latMin = document.get("latMin") as! String
                    //                    let lonMax = document.get("lonMax") as! String
                    //                    let lonMin = document.get("lonMin") as! String
                
                    let retrivedDictionary = ["senderId" : userid!, "message" :  message , "time" : savedDate]
                    
                    print("Retrived dictionary:- \(retrivedDictionary)")
                    
                    self.anotherUserMsgArray.add(retrivedDictionary)
                 //   self.retrivedArray.add(retrivedDictionary)
                    //.adding(retrivedDictionary)
                    //= retrivedDictionary
                    
                    
                    //  self.collectionView.reloadData()
                }
                
                print("here is array data : \(self.retrivedArray)")
                //            let sortedArray = self.retrivedArray.sortedArray(using: [NSSortDescriptor(key: "time", ascending: true)]) as! NSMutableArray
                
                //[[String:AnyObject]]
                //self.retrivedArray.sorted{ ($0["time"] as! Date)! > ($1["time"] as! Date)! }
                
                print("here is count : \(self.retrivedArray.count)")
                
                //                for i = 0 , i ...< self.retrivedArray.count {
                //
                //
                //
                //
                //                }
                
                
                for item in self.retrivedArray {
                    
                    let msg = item as! [String: Any]
                    self.totalMsgArray.add(msg)
                    
                }
                
                for item1 in self.anotherUserMsgArray {
                    
                    let msg = item1 as! [String : Any]
                    self.totalMsgArray.add(msg)
                    
                }
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
             //   retrivedArray
                let sortedResults = (self.totalMsgArray as NSArray).sortedArray(using: [NSSortDescriptor(key: "time", ascending: true)]) as! [[String : AnyObject]]
                
                //                let sortedArray = self.retrivedArray.sorted{[dateFormatter] one, two in
                //                    return dateFormatter.date(from: one["Time"] )! > dateFormatter.date(from: two["Time"] )! }
                
                //  let sortedArray =  self.retrivedArray.sorted{ ($0 as AnyObject) > ($1 as AnyObject)}
                
                print("all sort array is : \(sortedResults)")
                self.messagesSortedArray = sortedResults
                print("Sort array is : \(self.messagesSortedArray)")
                
                self.messages = self.getMessages()
                //                messagesSortedArray.append(sortedResults)
                self.scrollToBottom(animated: false)
                
                
            }
            
            
        }
        
        
        
       
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
      
//        if self.automaticallyScrollsToMostRecentMessage == true {
//            DispatchQueue.main.async(execute: {
//                self.scrollToBottom(animated: false)
//                self.collectionView.collectionViewLayout.invalidateLayout()
//            })
//        }
//
    }
//
}

extension ChatRoomVC {
    func getMessages() -> [JSQMessage] {
        var messages = [JSQMessage]()
        messages.removeAll()
        print("all data is : \(retrivedArray)")
        let orderedSet = NSOrderedSet(array: messagesSortedArray)
        var arrayWithoutDuplicates = orderedSet.array
            //as? NSMutableArray
        print("your output : \(arrayWithoutDuplicates)")
        if arrayWithoutDuplicates.count > 0 {
            
            print("message sorted Array : \(messagesSortedArray)")
            
            print("this is my dictionary \(dictionary)")
            for data in arrayWithoutDuplicates {
                
                let dict = data as! [String:Any]
                //               let com = data as Dictionary
                //                com
                
                let senderId =  dict["senderId"] as? Int //"\((data as AnyObject).key)"
                let message = dict["message"] as? String    //"\((data as AnyObject).value)"
                
                let time = dict["time"] as? Date
                //   time
                
                print("here date : \(time)")
                print("both key: \(senderId) and message of first if : \(message)")
                
                let senderId1 = "\(senderId!)"
                
                if senderId1 == login_userID! {
                    
                    let message1 = JSQMessage(senderId: login_userID!, senderDisplayName: "", date: time!, text: message!)
                    //JSQMessage(senderId: "1", displayName: "", text: message!)
                    //JSQMessage(senderId: senderId, senderDisplayName: "", date: , text: )
                    //JSQMessage(senderId: "1", displayName: "", text: message!)
                    messages.append(message1!)
                    
                    self.collectionView.reloadData()
                    
                }
                else {
                    
                    
                    let message2 = JSQMessage(senderId: anotherUserID!, senderDisplayName: "", date: time!, text: message!)
                    //JSQMessage(senderId: "2", displayName: "", text: message)
                    
                    
                    messages.append(message2!)
                    self.collectionView.reloadData()
                    
                  
                }
            }
            
            //  return messages
        }
        
        //        if messagesSortedArray.count > 0 {
        //
        //            print("here you enter into Another User Data")
        //            for data in retrivedArrayForAnother {
        //            let dict = data as! [String:Any]
        //            //               let com = data as Dictionary
        //            //                com
        //            print("you enter into UserAnother Chat left side")
        //            let senderId =  dict["senderId"] as? String //"\((data as AnyObject).key)"
        //            let message = dict["message"] as? String
        //            let time = dict["time"] as? Date
        //
        //
        //                if senderId! == anotherUserID {
        //
        //
        //                    let message2 = JSQMessage(senderId: "2", senderDisplayName: "", date: time!, text: message!)
        //            //JSQMessage(senderId: "2", displayName: "", text: message)
        //
        //
        //        messages.append(message2!)
        //         self.collectionView.reloadData()
        //                }
        //        }
        //
        //        }
        
        return messages
    }
}


extension UIViewController {
    func getDateStringFromUTC(time : Double) -> Date {
        
        
        let time1 = time.rounded()
        
   //   let newTime = time / 1000
        
        //
        var date = Date(timeIntervalSince1970: time)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss a"
        
        let strDate = dateFormatter.string(from: date as Date)
        
        
        
        let com = dateFormatter.date(from: strDate)
        print("aaaaa\(com)" as Any)
        
        print("here you in String : \(strDate)")
        
        
        return com!
    }
}

func getCurrentTimeStampWOMiliseconds(dateToConvert: NSDate) -> String {
    let objDateformat: DateFormatter = DateFormatter()
    objDateformat.dateFormat = "yyyy-MM-dd HH:mm:ss"
    let strTime: String = objDateformat.string(from: dateToConvert as Date)
    let objUTCDate: NSDate = objDateformat.date(from: strTime)! as NSDate
    let milliseconds: Int64 = Int64(objUTCDate.timeIntervalSince1970)
    let strTimeStamp: String = "\(milliseconds)"
    return strTimeStamp
}
extension Date {
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

//extension UIImage {
//
//    func resize(withWidth newWidth: CGFloat) -> UIImage? {
//
//        let scale = newWidth / self.size.width
//        let newHeight = self.size.height * scale
//        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
//        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
//        let newImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//
//        return newImage
//    }
//}

