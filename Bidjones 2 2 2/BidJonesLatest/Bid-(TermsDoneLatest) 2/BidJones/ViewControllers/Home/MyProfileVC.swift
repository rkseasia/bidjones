//
//  MyProfileVC.swift
//  BidJones
//
//  Created by Kuldeep Singh on 9/26/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import SDWebImage

class MyProfileVC : BaseViewController ,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate {
   
    //MARK: - TextField Outlets
    
    fileprivate var ImageData:Data?
    
    //@IBOutlet weak var bottomConstraint_CancelButton: NSLayoutConstraint!
    @IBOutlet weak var btnMySales: CustomButton!
    @IBOutlet var btnUpdateHeight: NSLayoutConstraint!
    @IBOutlet var txtFld_LastName: UITextField!
    @IBOutlet var txtFld_FirstName: UITextField!
    @IBOutlet var txtFld_UserName: UITextField!
    @IBOutlet var txtFld_Address: UITextField!
    @IBOutlet var txtFld_Country: UITextField!
    @IBOutlet var txtFld_State: UITextField!
    @IBOutlet var txtFld_City: UITextField!
    @IBOutlet var txtFld_EmailAddress: UITextField!
    @IBOutlet var txtFld_Password: UITextField!
    @IBOutlet var txtFld_forOpeningPickers: UITextField!
    @IBOutlet var txtFld_PostalCode: UITextField!
    //MARK: - TextView Outlets
    //MARK: - UItableView Outlets
    //MARK: - UISCrollView Outlets
    
    @IBOutlet var scrollView_main: UIScrollView!
    //MARK: - UIButton Outlets
//    @IBOutlet var btnNo: UIButton!
//    @IBOutlet var btnYes: UIButton!
    //MARK: - UIImageView Outlets
    @IBOutlet var ImgView_User: UIImageView!
    
    //MARK: - UIView Outlets
    /// @IBOutlet var viewPicker: UIView!
    //MARK: - UIPicker Outlets
    // @IBOutlet var pickerView: UIPickerView!
    @IBOutlet weak var btn_countrySelect: UIButton!
    
    @IBOutlet weak var btn_countrySelectDropDown: UIButton!
    
    @IBOutlet weak var btn_stateSelectTxtfield: UIButton!
    
    @IBOutlet weak var btn_DropDown_stateSelect: UIButton!
    
    @IBOutlet weak var btn_citySelecttxtfield: UIButton!
    
    @IBOutlet weak var btn_DropDown_citySelect: UIButton!
    
    @IBOutlet weak var btn_imgProfile: UIButton!
    
    //MARK: - Int Variable
    //MARK: - Bool Variable
    var is_seller = Bool()
    //MARK: - String Variable
    private var strCountryID = String()
    private var strStateID = String()
    private var strCityID = String()
    
    private var strImgBase64   = ""
    
    var imageUrl : URL?
    
    //MARK: - array Variable
    private var arrCity = [AddressData]()
    private var arrState = [AddressData]()
    // private var arrCountry = [[String : Any]]()
    private var arrPicker = [AddressData]()
    private var arrCountry = [AddressData]()
    private var countryID = ""
    //MARK: - UIPickerView Variable
    private var pickerView = UIPickerView()
    
    var profileInfo = [MyProfileData]()
    var userID = Int()
    var imageUrlGet : URL?
    //MARK: - dictionary Variable
    
    //MARK: - UIImagePickerController
    private var imagePicker =  UIImagePickerController()
    //MARK: - Enum Variable
    
    enum PickerTypes: Int {
        case countryPicker = 1
        case statePicker = 2
        case cityPicker = 3
        init() {
            self = .countryPicker
        }
    }
    private var pickerType: PickerTypes?
    
    enum PickerID : Int {
        case countryID = 1
        case stateID =  2
        case cityID  = 3
        init() {
            self = .countryID
            
        }
        
    }
     private var pickerID: PickerTypes?
    
    var zipcode = String()
    
    var body = NSMutableData()
    
    @IBOutlet weak var btn_update: CustomButton!
    
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
      //  bottomConstraint_CancelButton.constant = -160.0
        is_seller = true
        self.automaticallyAdjustsScrollViewInsets = false
        // self.view.insertSubview(scrollView_main, at: 0)
        let screenWidth = self.ViewWidth()
//        if(screenWidth == 320)
//        {
//            lbl_AreYouRegister.font = UIFont.systemFont(ofSize: 12.0)
//        }
        pickerView.delegate = self
        pickerView.dataSource = self
        txtFld_forOpeningPickers.inputView = pickerView
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 3/255, green: 95/255, blue: 253/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: KDone, style: UIBarButtonItemStyle.plain, target: self, action: #selector(DonePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: KCancel, style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFld_forOpeningPickers.inputAccessoryView = toolBar
        self.fetchCountries()
        
        self.scrollView_main.isScrollEnabled = false
        getUserDetail()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        ImgView_User.setRounded()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getUserDetail() {
        
        let url = "/user/detail"
        
        MyProfile.sharedManager.GetApi(url: url, Target: self, completionResponse: { (response) in
            
            
           // self.profileInfo = response
            //response
            
            print("now you get data profile : \(response)")
         if let userID1 = response[0].id {
                self.userID = userID1
            }
            
            if  let zipcode = response[0].postalCode {
            
            self.zipcode = zipcode
            
            }
            
            if let firstname =  response[0].firstName {
                
              self.txtFld_FirstName.text = firstname
            }
            
            if let lastname = response[0].lastName {
                
                self.txtFld_LastName.text = lastname
                
            }
            
            if let username = response[0].username {
            self.txtFld_UserName.text = username
            }
            
            if let address = response[0].address {
            self.txtFld_Address.text = address
                
            }
            
            if let country = response[0].country {
               self.txtFld_Country.text = country
            }
            
            if let state = response[0].state {
            self.txtFld_State.text = state
            }
            
            if let city = response[0].city {
            self.txtFld_City.text = city
                
            }
            
            if let postalcode = response[0].postalCode {
            self.txtFld_PostalCode.text = postalcode
            }
            
            if let emailAddress = response[0].emailAddress {
            self.txtFld_EmailAddress.text = response[0].emailAddress
            }
            
            if let countryID = response[0].countryID {
                
                self.strCountryID = "\(countryID)"
            }
            
            if let stateID = response[0].stateID {
                self.strStateID = "\(String(describing: stateID))"
            }
            
            
            if let cityID = response[0].cityID {
                print("There is city Id : \(cityID)")
            self.strCityID  = "\(cityID)"
            }
            
            if let profileImage = response[0].profileImage {
            let imageString = profileImage
                print("here is :\(imageString)")
                var url1 = URL(string: imageString)
                
                self.imageUrlGet = url1!
            }
            
            
          //let urlString =  kBaseURL + imageString
            //"https://bidjonesbucketprivate.s3.us-east-2.amazonaws.com/bidjones/kamal123/1530861079.jpg"
            
          
                //url!
            
//            if let metUrl = url1 as? URL {
//                print("get url : \(metUrl)")
//                 self.ImgView_User.sd_setImage(with: metUrl)
//
//            }
            
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            print("your url1 : \(self.imageUrlGet!)")
           // let data = Data.init(contentsOf: <#T##URL#>)
            if let data = try? Data(contentsOf:self.imageUrlGet!)
            {
                let image: UIImage = UIImage(data: data)!
                print("here is image where you get : \(image)")

                 self.ImgView_User.image = image
            }
            
            }
//
            
           
            
            
            
            
            
            
            }, completionnilResponse: { (Response) in
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        self.getUserDetail()
                    })
                }
                else if statusCode == 201
                {
                   let message =  Response["message"]
                    MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                    
                    
                }
                
                    
                
               
                
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
           
        }, networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
            
        })
        
        
        
    }
    

    
    
    
    
    //MARK: - IBActions
    
//    @IBAction func ActionNo(_ sender: Any)
//    {
//        is_seller = false
//        if let image  = UIImage(named: Kselected)
//        {
//            btnNo.setImage(image, for: .normal)
//        }
//        if let image  = UIImage(named: Kunselected)
//        {
//            btnYes.setImage(image, for: .normal)
//        }
//    }
//    @IBAction func ActionYes(_ sender: Any)
//    {
//        is_seller = true
//        if let image  = UIImage(named: Kselected)
//        {
//            btnYes.setImage(image, for: .normal)
//        }
//        if let image  = UIImage(named: Kunselected)
//        {
//            btnNo.setImage(image, for: .normal)
//        }
//    }
    @IBAction func ActionCountry(_ sender: Any)
    {
        if(arrCountry.count>0)
        {
            pickerType = PickerTypes(rawValue: 1)
            self.arrPicker = self.arrCountry
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(txtFld_Country.text != "")
            {
                if let value =  self.GetIndexFromArrayForString(arr: arrCountry, str: (txtFld_Country.text)!, key: Kname,type:.AddressData)
                {
                    pickerView.selectRow(value, inComponent: 0, animated: false)
                }
            }
        }
        else
        {
            fetchCountries()
        }
    }
    
    @IBAction func ActionMySales(_ sender: Any) {
        KCommonFunctions.PushToContrller(from: self, ToController: .MySale, Data: nil)
    }
    
    @IBAction func enable_Editing(_ sender: Any) {
        
        btnUpdateHeight.constant = 50
         self.scrollView_main.isScrollEnabled = true
        
        btn_update.isHidden = false
        txtFld_LastName.isUserInteractionEnabled = true
        txtFld_FirstName.isUserInteractionEnabled = true
       // txtFld_UserName.isUserInteractionEnabled = true
        txtFld_Address.isUserInteractionEnabled = true
        
         btn_countrySelect.isUserInteractionEnabled = true
         btn_countrySelectDropDown.isUserInteractionEnabled = true
         btn_stateSelectTxtfield.isUserInteractionEnabled = true
         btn_DropDown_stateSelect.isUserInteractionEnabled = true
         btn_citySelecttxtfield.isUserInteractionEnabled = true
         btn_DropDown_citySelect.isUserInteractionEnabled = true
        txtFld_PostalCode.isUserInteractionEnabled = true
        btn_imgProfile.isUserInteractionEnabled = true
        
        
        
    }
    
    
    @IBAction func ActionState(_ sender: Any) {
        guard let country  = txtFld_Country.text, !country.isEmpty, !country.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillCountryname)
            return
        }
        
        if(
            arrState.count>0)
        {
            pickerType = PickerTypes(rawValue: 2)
            self.arrPicker = self.arrState
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(txtFld_State.text != "")
            {
                if let value =  self.GetIndexFromArrayForString(arr: arrState, str: (txtFld_State.text)!, key: Kname,type:.AddressData)
                {
                    pickerView.selectRow(value, inComponent: 0, animated: false)
                }
            }
        }
        else
        {
            fetchState()
        }
    }
    @IBAction func ActionCity(_ sender: Any) {
        guard let state  = txtFld_State.text, !state.isEmpty, !state.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillStatename)
            return
        }
        if(arrCity.count>0)
        {
            pickerType = PickerTypes(rawValue: 3)
            self.arrPicker = self.arrCity
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(txtFld_City.text != "")
            {
                if let value =  self.GetIndexFromArrayForString(arr: arrCity, str: (txtFld_City.text)!, key: Kname,type:.AddressData)
                {
                    pickerView.selectRow(value, inComponent: 0, animated: false)
                }
            }
        }
        else
        {
            fetchCity()
        }
    }
    
    @IBAction func ActionSubmit(_ sender: Any) {
        self.view.endEditing(true)
        do {
            try Validations()
            RegisterData.sharedInstance?.firstName = txtFld_FirstName.text
            RegisterData.sharedInstance?.lastName  = txtFld_LastName.text
            RegisterData.sharedInstance?.email     =  txtFld_EmailAddress.text
          //  RegisterData.sharedInstance?.password  = txtFld_Password.text
            RegisterData.sharedInstance?.username  = txtFld_UserName.text
            RegisterData.sharedInstance?.imageStrBase64     = strImgBase64
            RegisterData.sharedInstance?.address      = txtFld_Address.text
            RegisterData.sharedInstance?.countryID    = Int(strCountryID)
            RegisterData.sharedInstance?.stateID      = Int(strStateID)
            RegisterData.sharedInstance?.cityID       = Int(strCityID)
            RegisterData.sharedInstance?.zipCode      = txtFld_PostalCode.text
            RegisterData.sharedInstance?.deviceTokken  = KDummyDeviceTokken
            
            if let city = strCityID as? String {
                print("your city is here : \(city)")
                if city == "" {
                    self.showAlertMessage(titleStr: "Bidjones", messageStr: "Please select city")
                    return
                }
                
            }
            else {
               self.showAlertMessage(titleStr: "Bidjones", messageStr: "Please select city")
                 return
            }
//            RegisterData.sharedInstance?.lat  = "\(Location.sharedInstance.GetCurrentLocation().latitude)"
//            RegisterData.sharedInstance?.lng  = "\(Location.sharedInstance.GetCurrentLocation().longitude)"
            
            
            if let deviceTokken = UserDefaults.standard.getDeivceTokken()
            {
                RegisterData.sharedInstance?.deviceTokken   = deviceTokken
            }
            RegisterData.sharedInstance?.deviceType    = KDeviceType
            RegisterData.sharedInstance?.isSeller      = is_seller
            
            let urlStr = KverifyUser + "/" + "\(String(describing: txtFld_EmailAddress.text!))" + "/" + "\(String(describing: txtFld_UserName.text!))"
            
            let urlwithPercentEscapes = urlStr.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            
            let url = "/user/updateProfile"
            
        
            zipcode = txtFld_PostalCode.text!
            
//            print("yes you see it : \(countryID) , state: \(stateID) , city : \(city_id)")
            
            //strImgBase64
            
         //  let body = self.body as Data
            
           
            
        //    print("Your file name : \(self.imageUrl!)")
            
//            UploadMultiPart.sharedInstance.UploadData(awsType: .Image, url: self.imageUrl!, completionHandler: { (response, get , met) in
//
//                print("\(response) and \(get) and your met \(met)")
//
//
//
//
//            }, completionnilResponse: {
//                (response)  in
//
//            }
//                , Target: self
//                , networkError: {(error) in
//                                    self.showAlertMessage(titleStr: KMessage, messageStr: error)
//
//                                })
//            UploadMultiPart.sharedInstance1.UploadData(awsType: .Image, url: imageUrl!, completionHandler: { (response, met , get) in
//
//            },  completionnilResponse: { (Response) in
//                //print(Response)
//                let statusCode = Response[Kstatus] as! Int
//                if statusCode == 500
//                {
//                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
//                        //print(resonse)
//                        //   self.GetRecieveBidList()
//                    })
//                }
//                else if statusCode == 201
//                {
//
//                }
//
//
//
//
//
//            }, networkError: {(error) in
//                self.showAlertMessage(titleStr: KMessage, messageStr: error)
//
//            })
//
            
            print("here is your selected Image : \(self.imageUrl)")
            
             if let url =  self.imageUrl as? URL {
                
                self.imageUrl = url
            }
             else {
                
                self.imageUrl = self.imageUrlGet
            }
            
            
            let myUrl = self.imageUrl!
            
                //URL.init(string: "")
            print("your myUrl: \(myUrl)")
            
            if !myUrl.absoluteString.contains("Containers")
                    {
                        let com = "\(myUrl)"
                        //let com = URL(String: myUrl)
                        print("your com : \(com)")
                        
                        self.uploadWholeData(myUrl: com)

                       // completionHandler(nil,"\(url?.absoluteString ?? "")","")
                        return
                    }
            
            
            UploadMultiPart.sharedInstance.UploadDataForImage(awsType: .Image, url: myUrl, completionHandler: {
                
                (response ,met, get) in
                
                print("you reach at response : \(response)")
                print("you met with : \(met)")
                print("you get with : \(get)")
                
                let url = URL(string: met!)
                
                self.imageUrl = nil
                
                if let url1 = met as? String {
                    
                    self.uploadWholeData(myUrl: url1)
                }
                
                
              
                    //self.navigationController.popViewController(animated: true)
                    
                    // self.onSlideMenuButtonPressed(btnShowMenu)
                    
                    //                let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
                    //                    let vc = storyboard.instantiateViewController(withIdentifier: "HomeNavigation")
                    //                    self.navigationController?.popToViewController(vc, animated: true)
                    
                    
               
                
                
            }, completionnilResponse: { (Response) in
                //print(Response)
                print("your response of upload image error")
                 MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        //print(resonse)
                        //   self.GetRecieveBidList()
                    })
                }
                else if statusCode == 201
                {
                     MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                    
                    
                    
                }
                
                
                
                
                
            }, Target: self, networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                
            })
            
            
            
            
            
            
          //  DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            
           
//            UploadMultiPart.sharedInstance.UploadDataWhole(awsType: .Image , url: myUrl, param: parm, completionHandler: { (response ,met, get) in
//
//                print("you reach at response : \(response)")
//                print("you met with : \(met)")
//                print("you get with : \(get)")
//
//
//
//
//
//
//            }, completionnilResponse: { (Response) in
//                                //print(Response)
//                                let statusCode = Response[Kstatus] as! Int
//                                if statusCode == 500
//                                {
//                                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
//                                        //print(resonse)
//                                        //   self.GetRecieveBidList()
//                                    })
//                                }
//                                else if statusCode == 201
//                                {
//
//                                }
//
//
//
//
//
//            }, networkError: {(error) in
//                                self.showAlertMessage(titleStr: KMessage, messageStr: error)
//
//                            })
            
            
            /// Here End old function update//////////////////////
            
            

            
            
            
            // }
        } catch let error {
            switch  error {
            case ValidationError.emptyFirstName:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillFirstname)
            case ValidationError.firstNameMinChar:
                self.showAlertMessage(titleStr: KMessage, messageStr:KFirstnameshouldbeof)
            case ValidationError.emptyLastName:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillLastname)
            case ValidationError.lastNameMinChar:
                self.showAlertMessage(titleStr: KMessage, messageStr:KLastNameShouldbeof)
            case ValidationError.emptyUsername:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillUsername)
            case ValidationError.userNameMinChar:
                self.showAlertMessage(titleStr: KMessage, messageStr:KUsernameshouldbeof)
            case ValidationError.emptyAddress:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefilladdreass)
            case ValidationError.addressMinChar:
                self.showAlertMessage(titleStr: KMessage, messageStr:KAddressShouldbeof)
            case ValidationError.emptyCountry:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillCountryname)
            case ValidationError.emptyState:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillStatename)
            case ValidationError.emptyCity:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillCityname)
            case ValidationError.emptyPostalCode:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillPostalCode)
            case ValidationError.postalCodeMinChar:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPostalCodeshouldbeof)
            case ValidationError.emptyEmailAddress:
                self.showAlertMessage(titleStr: KMessage, messageStr: KPleasefillEmailAddress)
            case ValidationError.invalidEmail:
                self.showAlertMessage(titleStr: KMessage, messageStr:KEnvalidemailaddress)
//            case ValidationError.emptyPassword:
//                self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillpassword)
//            case ValidationError.passwordMinChar:
//                self.showAlertMessage(titleStr: KMessage, messageStr:KPasswordshouldbeof)
//            case ValidationError.passwordSufficientComplexity:
//                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenteratleastonecaptialletter)
            case ValidationError.emptyImg:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseAddImg)
            default:
                self.showAlertMessage(titleStr: KMessage, messageStr:KUnknownerror)
            }
        }
    }
    
    
func uploadWholeData(myUrl: String) {
    
    
    let countryID =  Int(strCountryID)
    let stateID   =  Int(strStateID)
    let city_id   =    Int(strCityID)
    
    print("your myUrl : \(myUrl)")
   let url2 = "/user/updateProfile"
    let parm = ["user_id" :  self.userID,
                "first_name" : txtFld_FirstName.text!,
                "last_name" : txtFld_LastName.text!,
                "image" :  myUrl,
                "address" : txtFld_Address.text!,
                "country_id" :  countryID!,
                "state_id" : stateID!,
                "city_id" :city_id!  ,
                "zip_code" : zipcode,
                "lat" : "",
                "lng" : ""] as [String : Any]
//
//                            MyProfile.sharedManager.PostApi(url: url2, parameter: parm , Target: self, completionResponse: { (response) in
//
//
//                                print("Your data is uploaded")
//                                print("here is :\(response)")
//
//
//                                self.btn_update.isHidden = true
//                                self.btnUpdateHeight.constant = 0
//
//                                self.txtFld_LastName.isUserInteractionEnabled = false
//                                self.self.txtFld_FirstName.isUserInteractionEnabled = false
//                                self.txtFld_UserName.isUserInteractionEnabled = false
//                                self.txtFld_Address.isUserInteractionEnabled = false
//
//                                self.btn_countrySelect.isUserInteractionEnabled = false
//                                self.btn_countrySelectDropDown.isUserInteractionEnabled = false
//                                self.btn_stateSelectTxtfield.isUserInteractionEnabled = false
//                                self.btn_DropDown_stateSelect.isUserInteractionEnabled = false
//                                self.btn_citySelecttxtfield.isUserInteractionEnabled = false
//                                self.btn_DropDown_citySelect.isUserInteractionEnabled = false
//                                self.txtFld_PostalCode.isUserInteractionEnabled = false
//                                self.btn_imgProfile.isUserInteractionEnabled = false
//
//
//
//
//                                self.showAlertMessage(titleStr: KMessage, messageStr: "Profile updated successfully")
//
//                                let alert = UIAlertController(title: KMessage, message: "Profile updated successfully", preferredStyle: UIAlertControllerStyle.alert)
//                                let okAction = UIAlertAction(title: KOK, style: UIAlertActionStyle.default) {
//                                    UIAlertAction in
//                                    NSLog("OK Pressed")
//
//                                    //  self.uploadImage()
//                                }
//                                // Add the actions
//                                alert.addAction(okAction)
//                                self.present(alert, animated: true, completion: nil)
//
//
//
//
//
//                            }, completionnilResponse: { (Response) in
//                                //print(Response)
//                                let statusCode = Response[Kstatus] as! Int
//                                if statusCode == 500
//                                {
//                                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
//                                        //print(resonse)
//                                        //   self.GetRecieveBidList()
//                                    })
//                                }
//                                else if statusCode == 201
//                                {
//
//                                }
//
//
//
//
//
//                            }, completionError: { (error) in
//                                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
//
//                            }, networkError: {(error) in
//                                self.showAlertMessage(titleStr: KMessage, messageStr: error)
//
//                            })
    
    let myurl1 : URL?
    myurl1 = URL.init(string: "http")
    
    UploadMultiPart.sharedInstance.UploadDataWhole(awsType: .Image , url: myurl1! , param: parm, completionHandler: { (response ,met, get) in

            print("you reach at response : \(response)")
            print("you met with : \(met)")
            print("you get with : \(get)")

            
            self.btn_update.isHidden = true
            self.btnUpdateHeight.constant = 0
            
            self.txtFld_LastName.isUserInteractionEnabled = false
            self.self.txtFld_FirstName.isUserInteractionEnabled = false
            self.txtFld_UserName.isUserInteractionEnabled = false
            self.txtFld_Address.isUserInteractionEnabled = false
            
            self.btn_countrySelect.isUserInteractionEnabled = false
            self.btn_countrySelectDropDown.isUserInteractionEnabled = false
            self.btn_stateSelectTxtfield.isUserInteractionEnabled = false
            self.btn_DropDown_stateSelect.isUserInteractionEnabled = false
            self.btn_citySelecttxtfield.isUserInteractionEnabled = false
            self.btn_DropDown_citySelect.isUserInteractionEnabled = false
            self.txtFld_PostalCode.isUserInteractionEnabled = false
            self.btn_imgProfile.isUserInteractionEnabled = false
            
            
            
            
            self.showAlertMessage(titleStr: KMessage, messageStr: "Profile updated successfully")
            
            let alert = UIAlertController(title: KMessage, message: "Profile updated successfully", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: KOK, style: UIAlertActionStyle.default) {
                UIAlertAction in
                NSLog("OK Pressed")
                
                //  self.uploadImage()
            }
            // Add the actions
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)


        }, completionnilResponse: { (Response) in
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                
                MBProgressHUD.hide(for: KappDelegate.window, animated: true)
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    //   self.GetRecieveBidList()
                })
            }
            else if statusCode == 201
            {
 MBProgressHUD.hide(for: KappDelegate.window, animated: true)
            }





        }, networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)

        })

    
    }
        
        
        
   
    
    
    @IBAction func BackAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func ActionCancel(_ sender: Any) {
        self.view.endEditing(true)
        
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func AddImageAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.OpenGallaryCamera(pickerController: self.imagePicker)
    }
    
    
//
//    //MARK: Uplaod User Profile Pic
//    func uploadImageToServerFromApp(nameOfApi : NSString, parameters : NSString, uploadedImage : UIImage, withCurrentTask :RequestType, andDelegate :AnyObject)->Void {
//        if self.isConnectedToNetwork(){
//            currentTask = withCurrentTask
//            let myRequestUrl = NSString(format: "%@%@%@",GlobalConstants.KBaseURL,nameOfApi,parameters)
//            let url = (myRequestUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))!
//            var request : NSMutableURLRequest = NSMutableURLRequest()
//            request = URLRequest(url: URL(string:url as String)!) as! NSMutableURLRequest
//            request.httpMethod = "POST"
//            let boundary = generateBoundaryString()
//            //define the multipart request type
//            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//            let image_data = UIImagePNGRepresentation(uploadedImage)
//            if(image_data == nil){
//                return
//            }
//            let body = NSMutableData()
//            let fname = "image.png"
//            let mimetype = "image/png"
//            //define the data post parameter
//            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
//            body.append("Content-Disposition:form-data; name=\"image\"\r\n\r\n".data(using: String.Encoding.utf8)!)
//            body.append("hi\r\n".data(using: String.Encoding.utf8)!)
//            body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
//            body.append("Content-Disposition:form-data; name=\"image\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
//            body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
//            body.append(image_data!)
//            body.append("\r\n".data(using: String.Encoding.utf8)!)
//            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
//            request.httpBody = body as Data
//            let session = URLSession.shared
//            let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
//                guard let data = data, error == nil else {                                                 // check for fundamental networking error
//                    // print("error=\(String(describing: error))")
//                    self.showAlertMessage(title: "App name", message: "Server not responding, please try later")
//                    return
//                }
//                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
//                    // print("statusCode should be 200, but is \(httpStatus.statusCode)")
//                    // print("response = \(String(describing: response))")
//                    self.delegate?.internetConnectionFailedIssue()
//                }else{
//                    do {
//                        self.responseDictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSDictionary
//                        // self.Responsedata = data as NSData
//                        //self.responseDictionary = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String: AnyObject] as NSDictionary;
//
//                        self.delegate?.responseReceived()
//                    } catch {
//                        //print("error serializing JSON: \(error)")
//                    }
//                }
//            }
//            task.resume()
//        }
//        else{
//            // print("Internet Connection not Available!")
//            self.showAlertMessage(title: "App Name", message: "No Internet Connection..")
//        }
//    }
    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    
    //MARK: - UImagePickerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        ImageData = self.compressImage(image: info[UIImagePickerControllerEditedImage] as! UIImage, compressionQuality: 1.0)
        
        ImgView_User.image = info[UIImagePickerControllerEditedImage] as? UIImage
        
        
        
        
        let image_data = UIImagePNGRepresentation(info[UIImagePickerControllerEditedImage] as! UIImage)
        if(image_data == nil){
            return
        }
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        // choose a name for your image
        let fileName = "/\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
        // create the destination file url to save your image
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        
        
        if let data = UIImageJPEGRepresentation(info[UIImagePickerControllerEditedImage] as! UIImage, 1.0),
            !FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                // writes the image data to disk
                try data.write(to: fileURL)
                // RegisterVC.loader = false
                
                print("This is our before \(fileURL)")
                
                self.imageUrl = fileURL
                
              //  print("This is after \(self.imageFile)")
                
                
                //}
            }catch {
                
            }
        
//         let boundary = generateBoundaryString()
//
//        let fname = "image.png"
//        let mimetype = "image/png"
//        //define the data post parameter
//        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
//        body.append("Content-Disposition:form-data; name=\"image\"\r\n\r\n".data(using: String.Encoding.utf8)!)
//        body.append("hi\r\n".data(using: String.Encoding.utf8)!)
//        body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
//        body.append("Content-Disposition:form-data; name=\"image\"; filename=\"\(fname)\"\r\n".data(using: String.Encoding.utf8)!)
//        body.append("Content-Type: \(mimetype)\r\n\r\n".data(using: String.Encoding.utf8)!)
//        body.append(image_data!)
//        body.append("\r\n".data(using: String.Encoding.utf8)!)
//        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
//
        
        
        if (ImageData != nil)
        {
            strImgBase64 = (ImageData?.base64EncodedString(options: .lineLength64Characters))!
          //  strImgBase64 = KBase64Prefix + strImgBase64
        }
        dismiss(animated:true, completion: nil)
    }
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        //print("Cancel")
        dismiss(animated:true, completion: nil)
    }
    
    //MARK: - Other Methods
    
    @objc func DonePicker()
    {
        let row = pickerView.selectedRow(inComponent: 0)
        let dic = arrPicker[row]
        //print(dic)
        let title = dic.name
        let id = dic.id
        let ID:String = String(id)
        switch pickerType
        {
        case .countryPicker?:
            txtFld_Country.text = title
            txtFld_State.text = ""
            txtFld_City.text = ""
            strCountryID = ID
            fetchState()
        case .statePicker?:
            txtFld_State.text = title
            txtFld_City.text = ""
            strStateID = ID
            fetchCity()
        case .cityPicker?:
            txtFld_City.text = title
            strCityID = ID
        case .none:
            print("none")
        }
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    @objc func CancelPicker()
    {
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func Validations() throws
    {
        if(strImgBase64.isEmpty && strImgBase64.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && self.imageUrlGet! == nil)
        {
            throw ValidationError.emptyImg
        }
        guard let firstName = txtFld_FirstName.text,  !firstName.isEmpty, !firstName.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
        {
            throw ValidationError.emptyFirstName
        }
        if(!firstName.nameMinLength)
        {
            throw ValidationError.firstNameMinChar
        }
        guard let lastName  = txtFld_LastName.text, !lastName.isEmpty, !lastName.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyLastName
        }
        if(!lastName.nameMinLength)
        {
            throw ValidationError.lastNameMinChar
        }
        guard let userName  = txtFld_UserName.text, !userName.isEmpty, !userName.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyUsername
        }
        if(!userName.nameMinLength)
        {
            throw ValidationError.userNameMinChar
        }
        guard let address  = txtFld_Address.text, !address.isEmpty, !address.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyAddress
        }
        guard let country  = txtFld_Country.text, !country.isEmpty, !country.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyCountry
        }
        guard let state  = txtFld_State.text, !state.isEmpty, !state.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyState
        }
        guard let city  = txtFld_City.text, !city.isEmpty, !city.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyCity
        }
        guard let postalCode  = txtFld_PostalCode.text, !postalCode.isEmpty, !postalCode.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyPostalCode
        }
        if(!postalCode.postalCodeMinLength)
        {
            throw ValidationError.postalCodeMinChar
        }
        guard let emailAddress  = txtFld_EmailAddress.text, !emailAddress.isEmpty, !emailAddress.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyEmailAddress
        }
        if(!emailAddress.isEmail)
        {
            throw ValidationError.invalidEmail
        }
//        guard let password  = txtFld_Password.text, !password.isEmpty, !password.trimmingCharacters(in: .whitespaces).isEmpty else
//        {
//            throw ValidationError.emptyPassword
//        }
//        if(!password.passwordMinLength)
//        {
//            throw ValidationError.passwordMinChar
//        }
//        if(!password.checkTextSufficientComplexity())
//        {
//            throw ValidationError.passwordSufficientComplexity
//        }
    }
    
    //MARK: - Fetch Countries Name
    
    func  fetchCountries()
    {
        let urlStr = KCountryApi
        FetchAddress.sharedFetchAddress.GetAddressListApi(url: urlStr, Target: self, completionResponse: { (countries) in
            self.arrCountry = countries
            
            
            self.arrPicker = self.arrCountry
            self.pickerView.reloadAllComponents()
        }, completionnilResponse: { (Response) in
            if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        },completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: (error))
        })
    }
    
    //MARK: - Fetch City Names
    
    func  fetchState() {
        let urlStr = KStateApi + strCountryID
        FetchAddress.sharedFetchAddress.GetAddressListApi(url: urlStr, Target: self, completionResponse: { (states) in
            self.arrState = states
            self.arrPicker = self.arrState
            if(self.arrState.count>0)
            {
                self.pickerView.reloadAllComponents()
            }
        }, completionnilResponse: { (Response) in
            if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
            self.txtFld_State.text = self.txtFld_Country.text
            self.strStateID = KZero
            self.arrState.removeAll()
        },completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: (error))
        })
        
    }
    //MARK: - Fetch State Names
    
    func  fetchCity()
    {
        let urlStr = KCityApi + strStateID
        FetchAddress.sharedFetchAddress.GetAddressListApi(url: urlStr, Target: self, completionResponse: { (cities) in
            self.arrCity = cities
            self.arrPicker = self.arrCity
            if(self.arrCity.count>0)
            {
                self.pickerView.reloadAllComponents()
            }
        }, completionnilResponse: { (Response) in
            if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
            self.arrCity.removeAll()
            self.txtFld_City.text = self.txtFld_State.text
            self.strCityID = KZero
        },completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: (error))
        })
    }
    
    //MARK: - TextField delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if(textField == txtFld_forOpeningPickers)
        {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.scrollView_main.contentOffset.y = 150
                }, completion: nil)
            }
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        if(textField == txtFld_forOpeningPickers)
        {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.scrollView_main.contentOffset.y = 0
                }, completion: nil)
            }
            
            
        }
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (string == " ") && (textField.text?.count)! == 0
        {
            return false
        }
        if (textField == txtFld_UserName || textField == txtFld_FirstName || textField == txtFld_LastName)
        {
            if textField.RestrictMaxCharacter(maxCount: 30, range: range, string: string)
            {
                if (string == " ")
                {
                    return false
                }
            }
            else
            {
                return false
            }
        }
        if (textField == txtFld_FirstName || textField == txtFld_LastName )
        {
            let regex = try! NSRegularExpression(pattern: "[a-zA-Z\\s]+", options: [])
            let range = regex.rangeOfFirstMatch(in: string, options: [], range: NSRange(location: 0, length: string.count))
            return range.length == string.count
        }
        if textField == txtFld_PostalCode
        {
            if textField.RestrictMaxCharacter(maxCount: 8, range: range, string: string)
            {
                let regex = try! NSRegularExpression(pattern: "[A-Za-z0-9^]*", options: [])
                let range = regex.rangeOfFirstMatch(in: string, options: [], range: NSRange(location: 0, length: string.count))
                return range.length == string.count
            }
            else
            {
                return false
            }
        }
        if (textField == txtFld_EmailAddress || textField == txtFld_Address )
        {
            return textField.RestrictMaxCharacter(maxCount: 100, range: range, string: string)
        }
        if textField == txtFld_Password
        {
            return textField.RestrictMaxCharacter(maxCount: 20, range: range, string: string)
        }
        return true
    }
}
extension MyProfileVC : UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK: - Picker Datasource and delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrPicker.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let dic = arrPicker[row]
        let title = dic.name
        
       
        
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
    }
    
}


