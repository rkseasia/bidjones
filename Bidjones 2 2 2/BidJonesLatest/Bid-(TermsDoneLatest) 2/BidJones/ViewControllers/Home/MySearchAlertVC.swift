//
//  MySearchAlertVC.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/2/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class MySearchAlertVC: UIViewController ,  MySearchAlertTableViewCellDelegate {

    
    //MARK:- VARIABLES
    var array = [String]()
   // var arrList = [MySearchAlertData]()
    var arrList = [ItemListData]()
    var isScrolling:Bool?
    var dataLoaded:Bool?
    var isLoading:Bool?
    var count = 0

    //MARK:- ALL OUTLETS
    @IBOutlet weak var lbl_no_data_found: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- CLASSES OVERRIDE FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
      isLoading = false
      dataLoaded = false
      isScrolling = false
        self.automaticallyAdjustsScrollViewInsets = false
        tableView.separatorColor = nil
        tableView.backgroundView = nil
        tableView.backgroundColor = UIColor.clear
        tableView.separatorColor = UIColor.clear
        self.navigationItem.setHidesBackButton(true, animated: false)
        getAlertData()
      tableView.estimatedRowHeight = 1000
      tableView.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 
    // MARK:- IB ACTION
    @IBAction func Back_ButtonAction(_ sender: Any) {
        
     self.navigationController?.popViewController(animated: true)
    }
    
  //MARK: - UISCrollview delegate
  func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
    
  }
  
  func  scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
  {
  }
  func scrollViewDidScroll(_ scrollView: UIScrollView)
  {
    if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
      //reach bottom
      print(arrList.count)
      print(arrList.count)
      // print(isScrolling)
      //  print(isLoading)
      //print(dataLoaded)
      
      
      if (arrList.count>0 && arrList.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
      {
        print("222222222222")
        isScrolling = true
        print("reach bottom")
        isLoading = true
        self.count = self.count+KPaginationcount
        getAlertData()
      }
//      else if isDeleted!
//      {
//        isDeleted = false
//        print("222222222222")
//        isScrolling = true
//        print("reach bottom")
//        isLoading = true
//        GetSearchData()
//      }
    }
  }
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
  {
    print("11111111111")
    isScrolling = false
  }
    // MARK:- OTHER FUNCTIONS
    func getAlertData() {
    
    let url = "/listsearchpreferenceitems"
    
      var parm = [String : Any]()
      
      if let lattitude = Location.sharedInstance.lat
      {
        parm["current_lat"] = lattitude
      }
      if let longitude = Location.sharedInstance.lng
      {
        parm["current_lng"] = longitude
      }
        parm["category_type"]  =  "all"
      
      parm["offset"] = count
      parm["limit"]  = KPaginationcount
      
      
      
      MySearchAlert.sharedManager.PostApi(url: url, parameter: parm,Target: self, completionResponse: { (Response) in
        print(Response)
        print(self.arrList)
        
        if Response.count>0
        {
          if(self.count == 0)
          {
            //  self.arrStuff = Response
            for item in Response
            {
              let oldIds = self.arrList.map { $0.id } as? [Int]
              print(oldIds as Any)
              if !((oldIds?.contains(item.id!))!)
              {
                self.arrList.insert(item, at: self.arrList.count)
              }
            }
            self.tableView.reloadData()
          }
          else
          {
            for item in Response
            {
              let oldIds = self.arrList.map { $0.id } as? [Int]
              print(oldIds as Any)
              if !((oldIds?.contains(item.id!))!)
              {
                self.arrList.insert(item, at: self.arrList.count)
              }
            }
            self.tableView.reloadData()
            print(self.arrList)
          }
          print(self.count)
          self.lbl_no_data_found.isHidden = true
        }
        else
        {
          if(self.count != 0)
          {
            print("abcdefgh")
            self.dataLoaded = true
          }
          if (self.count == 0 || self.arrList.count == 0)
          {
            self.lbl_no_data_found.isHidden = false
            //self.viewNotify.isHidden = false
          }
        }
        self.isLoading = false
      }, completionnilResponse: { (Response) in
        //print(Response)
        let statusCode = Response[Kstatus] as! Int
        if statusCode == 500
        {
          KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
            //print(resonse)
            self.getAlertData()
          })
        }
          //          else if statusCode == 201
          //            {
          //                self.dataLoaded = true
          //            }
        else
        {
          if (self.count == 0 || self.arrList.count == 0)
          {
            self.arrList.removeAll()
            self.tableView.reloadData()
            self.lbl_no_data_found.isHidden = false
           // self.viewNotify.isHidden = false
            
          }
          
        }
        self.isLoading = false
        
      }, completionError: { (error) in
        self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        self.isLoading = false
      },networkError: {(error) in
        self.showAlertMessage(titleStr: KMessage, messageStr: error)
        self.isLoading = false
      })
  
}

    
    func view_Action(sender: MySearchAlertTableViewCell) {
      
      let data = arrList[sender.tag]
     // Index = sender.tag
      // let itemId = data.id
      CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
        
//        let storyboard = UIStoryboard.init(name: "Seller", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "detailVC") as! DetailVC
//        print("sender button count : \(sender.tag)")
//        print("you get productid and sellerID :\(arrList[sender.tag].productID!) and \(arrList[sender.tag].sellerID!)")
//
//       let categoryID =  arrList[sender.tag].categoryID!
//
//        if categoryID == "1" {
//
//            vc.idProduct = arrList[sender.tag].productID!
//            vc.idSeller = arrList[sender.tag].sellerID!
//              self.navigationController?.pushViewController(vc, animated: true)
//        }
//        else if categoryID == "2" {
//
//            vc.idProduct = arrList[sender.tag].productID!
//            vc.idSeller = arrList[sender.tag].sellerID!
//              self.navigationController?.pushViewController(vc, animated: true)
//
//        }
//       else if categoryID == "3" {
//            vc.idProduct = arrList[sender.tag].productID!
//            vc.idSeller = arrList[sender.tag].sellerID!
//              self.navigationController?.pushViewController(vc, animated: true)
//        }
//        else if categoryID == "4" {
//
//            vc.idProduct = arrList[sender.tag].productID!
//            vc.idSeller = arrList[sender.tag].sellerID!
//              self.navigationController?.pushViewController(vc, animated: true)
//        }
//        else if categoryID == "5" {
//
//            vc.idProduct = arrList[sender.tag].productID!
//            vc.idSeller = arrList[sender.tag].sellerID!
//              self.navigationController?.pushViewController(vc, animated: true)
//        }
//
//        else if categoryID == "6" {
//
//            vc.idProduct = arrList[sender.tag].productID!
//            vc.idSeller = arrList[sender.tag].sellerID!
//              self.navigationController?.pushViewController(vc, animated: true)
//        }
//
//
      
  }
    
    
}
  


   //MARK:- TABLE VIEW DELEGATE METHODS
extension MySearchAlertVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
    
}

extension MySearchAlertVC : UITableViewDataSource {
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MySearchAlertTableViewCell
        cell.tag = indexPath.row
        cell.delegate = self
        cell.selectionStyle = .none
        //cell.cellData(array: arrList, indexPath: indexPath)
        cell.LoadData(dic:arrList[indexPath.row], type: .Book)

        cell.backgroundColor = UIColor.clear
        cell.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
        
        return cell
    }
    
    
}
