//
//  MusicListVC.swift
//  BidJones
//
//  Created by Kuldeep Singh on 9/27/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import AVKit

class MusicListVC: UIViewController , MusicListTableViewCellDelegate{
    
    
    
    
    //MARK:- for Audio Player variables
    var player              =  AVPlayer()
    var stopped             =  false
    var playerController    = AVPlayerViewController()
    var timer               : Timer? = nil
    var timeToStop          : Int    = 10
    var playerObserver      : AnyObject!
    var audioPlayer         : AVAudioPlayer?
    var playerItem          : AVPlayerItem!
    var timerObserver:Any?
    var urlSong             : URL?
    var counter             = 0
    
    static  var isMusicVC = false
    
    @IBOutlet weak var lbl_no_data_found: UILabel!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var lbl_Genre: UILabel!
    @IBOutlet weak var txtfield_search: UITextField!
    @IBOutlet weak var view_forGenre: UIView!
    
    //MARK:- CLASS VARIABLES
    var array                  =               [String]()
    var musicArray             =               [MusicListData]()
    var musicArray1            =               [MusicListData]()
    var genreid                =               Int()
    var searchActive           =               false
    private var arrPicker      =               [GenreData]()
    var arrGenre               =               [GenreData]()
    var isGenrePicker          =               true
    static var isMusicList     =               false
    
    enum PickerType: Int {
        case GenrePicker = 1
       
        init() {
            self = .GenrePicker
        }
    }
    
     private var pickerType: PickerType?
     @IBOutlet weak var pickerView: UIPickerView!
     var filtered:[String] = []
     @IBOutlet weak var tableView: UITableView!
    
    //MARK:- OVERRIDE CLASS FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
        } catch let error {
            print(error.localizedDescription)
        }
        
        
        
        
        
        txtfield_search.delegate = self
        getDataMusicList(genreId: 0)
        filtered.removeAll()
        GetGener()
        
        self.hideKeyboardWhenTappedAround()
          txtfield_search.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
       // view.endEditing(true)
        
         self.navigationItem.setHidesBackButton(true, animated: false)

         tableView.separatorColor = nil
        tableView.backgroundView = nil
        tableView.backgroundColor = UIColor.clear
        tableView.separatorColor = UIColor.clear
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getDataMusicList(genreId : Int) {
        
        
        let url = "/buyer/listradioFiles"
        
        let param = ["gener_id" : genreId, "limit" : 10 , "offset" : 0]
        
        
        MusicList.sharedManager.PostApi(url: url, parameter: param, Target: self, completionResponse: { (response) in
            
            
            print("here you reached at your destination ")
            print("you get here : \(response)")
            
            self.musicArray = response
            
            if self.musicArray.count == 0 {
                
                
                self.lbl_no_data_found.isHidden = false
                
                self.tableView.reloadData()
            }
            
            if self.musicArray.count > 0 {
                
                self.lbl_no_data_found.isHidden = true
              print("your array data  : \(self.musicArray)")

            for each in self.musicArray {
            
            self.array.append(each.songTilte!)
                
                
            }
                        print("your array data  : \(self.array)")
            }
            self.tableView.reloadData()
            print("your count : \(self.musicArray.count)")
            
        } , completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    
                    self.getDataMusicList(genreId: 0)
                    //print(resonse)
                    //   self.GetRecieveBidList()
                })
            }
            else if statusCode == 201
            {
                
            }
            
            
            
            
            
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            
        }, networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
            
        })
        
        
        
        
    }
    
    //MARK:- IB ACTION
 
    @IBAction func Back_ButtonAction(_ sender: Any) {
        
        MusicListVC.isMusicVC = true
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func getGenreData(_ sender: Any) {
        
        view_forGenre.isHidden = false
        if(arrGenre.count>0)
        {
            isGenrePicker = true
            pickerType = PickerType(rawValue: 1)
        
            self.arrPicker = self.arrGenre
           // var dict : GenreData?
            
            let newAllValue = ["id"  : 0 , "name" : "Genre"] as [String : Any]
            
            
           let  dict = GenreData(dict: newAllValue)
 
            
            
            
            self.arrPicker.insert(dict!, at: 0)
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)

        }
        
        
    }
    
    
    @IBAction func btn_DoneAction(_ sender: Any) {
    
           view_forGenre.isHidden = true
           musicArray.removeAll()
           print("your genre Id : \(self.genreid)")
           array.removeAll()
           let row = pickerView.selectedRow(inComponent: 0)
           let dic = arrPicker[row]
           let title = dic.name
           let id = dic.id
    
           getDataMusicList(genreId : id )
           lbl_Genre.text = title
           txtfield_search.text = ""

    
    }
    
    @IBAction func btn_CancelAction(_ sender: Any) {
        
           view_forGenre.isHidden = true
           array.removeAll()
           txtfield_search.text = ""
           musicArray.removeAll()
           getDataMusicList(genreId : 0)
           lbl_Genre.text = "Genre"
           txtfield_search.text = ""

    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        
        if keyPath == "rate" && (change?[NSKeyValueChangeKey.newKey] as? Float) == 0{
            print("stop")
            stopped = true
            self.playerController.player?.removeObserver(self, forKeyPath: "rate")
          

           
        }
        else if keyPath == "rate" && (change?[NSKeyValueChangeKey.newKey] as? Float) == 1
        {
            print("play")
            if (self.playerController.player?.currentTime().seconds)! >= Double(10.0)
            {
                if detailType == .Detail
                {
                    playerController.player?.seek(to: CMTimeMake(Int64(0), Int32(10)), toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero, completionHandler: { (result) in
                        print("result", result)
                        
                        
                        
                    })
                }
            }
            stopped = false
        }
    }
    
    //MARK:- Functions
    @objc func stopPlaying()
    {
        player.pause()
    }
    
    func showAlert()
    {
        let alertController = UIAlertController(title: KMessage, message: "Purchase this item, to play full audio!", preferredStyle: .alert)
        
        
        let OKAction = UIAlertAction(title: "Back", style: .default) { (action:UIAlertAction!) in
            self.playerController.dismiss(animated: true, completion: nil)
            
            self.playerController.player?.removeObserver(self, forKeyPath: "rate")
            self.playerController.player?.currentItem?.removeObserver(self, forKeyPath: "timedMetadata")
        }
        alertController.addAction(OKAction)
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            print("Cancel button tapped");
        }
        alertController.addAction(cancelAction)
        
        
        self.playerController.present(alertController, animated: true, completion:nil)
    }
    
    func StarAudio(Url:URL)  {
        //let path = Bundle.main.path(forResource: "Qismat", ofType: "mp3")
        // let soundUrl = NSURL(fileURLWithPath: path!)
        let audiourl = Url
        //URL(string: "http://stgsd.appsndevs.com:9048/uploads/1538141075242_MyAudio.mp3")
        if timerObserver != nil
        {
            if detailType == .Detail
            {
                self.playerController.player?.removeTimeObserver(timerObserver ?? "Blank")
            }
            self.playerController.player?.removeObserver(self, forKeyPath: "rate")
            self.playerController.player?.currentItem?.removeObserver(self, forKeyPath: "timedMetadata")
        }
        
        let item = AVPlayerItem(url: audiourl)
        player = AVPlayer(playerItem: item)
        
        
        playerController.player = player
        playerController.allowsPictureInPicturePlayback = true
        
        // item.forwardPlaybackEndTime = CMTime(seconds: 10.0, preferredTimescale: 1)
        
        
        playerController.player?.play()
//        NotificationCenter.default.addObserver(self, selector: Selector(("playerDidFinishPlaying:")),
//               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        if (self.navigationController?.presentedViewController == self.playerController) {
            print("ssdf")

        }else{
        self.present(playerController,animated:true,completion:nil)
        }
        
        if detailType == .Detail
        {
            let times = [NSValue(time:CMTimeMake(Int64(timeToStop),1))]
            
            _ = self.playerController.player?.addBoundaryTimeObserver(forTimes: times, queue: DispatchQueue.main, using:
                {
                    [weak self] in
                    self?.stopPlaying()
                    
                    self?.showAlert()
            })
        }
        else
        {
            //  self.playerController.setValue(true, forKey: "requiresLinearPlayback")
            
        }
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)
        self.playerController.player?.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
        
        
    }

    @objc func playerDidFinishPlaying(note: NSNotification) {
        if ((counter + 1) == musicArray.count) {
            counter = 0
        }else{
         counter = counter + 1
        }
       self.playAudio(tag: counter)
    }
    
    //MARK:- PLAY ACTION BUTTION OF CELL
    func play_Action(_ sender: MusicListTableViewCell) {
        self.playAudio(tag: sender.tag)
    }
    
    func playAudio(tag: Int){
        print("your action is : \(tag)")
        let url1 = "/buyer/radioAlert"
        
        if let pUrl = musicArray[tag].audioSong {
            counter = tag
            let url = URL.init(string: pUrl)
            print(url as Any)
            StarAudio(Url:url!)
            let id = UserDefaults.standard
            let loginUserID = id.getUserID()
            var parm = [String : Any]()
            parm = ["listener_id" : loginUserID! , "radio_id" : musicArray[tag].id!]
            
            MusicList.sharedManager.PostApiForNotification(url: url1, parameter: parm, Target: self, completionResponse: {
                (response) in
                
                print("my Response is radio : \(response)")
                
            },  completionnilResponse: { (Response) in
                //print(Response)
                let statusCode = Response[Kstatus] as! Int
                if statusCode == 500
                {
                    KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                        
                        self.getDataMusicList(genreId: 0)
                        //print(resonse)
                        //   self.GetRecieveBidList()
                    })
                }
                else if statusCode == 201
                {
                    
                }
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                
            }, networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                
            })
            
        }
    }

       
    //MARK:- GET GENER DATA
    func GetGener()  {
        let urlStr = KGenerApi + String(KMusicType)
        AddProduct.sharedManager.GetApi(url: urlStr,Target: self, completionResponse: { (Genre,BidType,maxImages) in
            self.arrGenre = Genre
            self.arrPicker = self.arrGenre
            print(Genre)
            
            print("here your arrGener Data : \(self.arrGenre)")
            //print(BidType)
            //print(maxImages)
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    
                    self.GetGener()
                })
            }
            else if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
  }
//MARK:- UI TEXTFIELD DELEGATE
extension MusicListVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchActive = true;
    }
   
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchActive = false;
    }
    
    
    /*func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        print("range:- \(range)  string:-\(string)")
   
        if textField ==  txtfield_search {
            print("first text of the textfield:- \(txtfield_search.text)")
            if txtfield_search.text == "" {
                
                
                getDataMusicList(genreId: 0)
                
            }
            filtered = array.filter({$0.contains(txtfield_search.text!)})
            print("This is filtered array:- \(filtered)")
            
            
            /// filteredArr will contain now ["ab","abc"]
        }
        if(filtered.count == 0){
            searchActive = false;
            
            
            
        } else {
            searchActive = true;
            
            
        }
        self.tableView.reloadData()
        
        return true
    }*/
    
   
   
    
    
    
    @objc func textFieldDidChange(textField: UITextField) {
        if textField ==  txtfield_search {
            print("First character :- \(txtfield_search.text)")
            
//            let text = textField.text as NSString?
//            
//
//            let predicate=NSPredicate(format: "SELF.name CONTAINS[cd] %@", txtAfterUpdate)
//            let arr=arrayData.filtered(using: predicate)
            
             filtered.removeAll()
            
            
            if (txtfield_search.text?.isEmpty)! {
                searchActive = false;
                filtered.removeAll()
                array.removeAll()
                print("your data is :\(filtered)")
                
                getDataMusicList(genreId: 0)
                
                
            }else{
                searchActive = true;
                print("your data : \(filtered)")
                filtered.removeAll()
              print(array.count)
              print(array)

//                filtered = array.filter({ (text) -> Bool in
//
//                    let tmp: NSString = text as NSString
//                    let range = tmp.range(of: txtfield_search.text!, options: NSString.CompareOptions.caseInsensitive)
//                    //rangeOfCharacter(from: searchText, options: NSString.CompareOptions.caseInsensitive)
//                    //    let range = tmp.rangeOfString(searchText, options: NSString.CompareOptions.CaseInsensitiveSearch)
//
//
//                                    if range.location == NSNotFound {
//
//                                        musicArray1 = musicArray
//                                        musicArray.removeAll()
//                                        filtered.removeAll()
//                                        lbl_no_data_found.isHidden = false
//                                        tableView.reloadData()
//                                        print(range.location)
//
//
//
//                                         return false
//
//                                    }
//                     else if range.location != NSNotFound {
//
//
//                                        //searchActive = true;
//
//                    // musicArray = musicArray1
//                                        lbl_no_data_found.isHidden = true
//
//                        tableView.reloadData()
//                       return range.location != NSNotFound
//                     }
//
//                    return range.location != NSNotFound
//                })
              
              filtered.removeAll()
              print(array.count)
              print(txtfield_search.text!)


            filtered = array.filter { $0.lowercased().contains((txtfield_search.text!).lowercased()) }
             //  filtered = array.filter { $0.contains(txtfield_search.text!) }

              print(filtered)

            }
//            filtered.removeAll()
//            filtered = array.filter({$0.contains(txtfield_search.text!)})
//            print("This is filtered array:- \(filtered)")
//            /// filteredArr will contain now ["ab","abc"]
//        }
            
            if(filtered.count == 0){
lbl_no_data_found.isHidden = false
            }
          else {
              lbl_no_data_found.isHidden = true

        }
            self.tableView.reloadData()
            
        }
        
    }

}
//MARK:- TABLE VIEW DELEGATE
extension MusicListVC : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    self.tableView.deselectRow(at: indexPath, animated: true)
       
    }
    
    
}


extension MusicListVC : UITableViewDataSource {
   
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          if(searchActive) {
            print("here is :\(filtered.count)")
            print("here is array filtered : \(filtered)")
            return filtered.count
        }
            return musicArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MusicListTableViewCell
        cell.tag = indexPath.row
        cell.delegate = self
        if(searchActive){
 
            if musicArray.count > 0 {
                     print("total array: \(array)")
                     if filtered.count > 0 {
            
            if array.contains(filtered[indexPath.row]) {
                 print("your filtered data : \(filtered[indexPath.row])")
                 let index =   array.index(of: filtered[indexPath.row])
                 print("here your no. is : \(index)")
                 let count = index as! Int
                 let myCount = IndexPath(row: count, section: 0)
                 cell.cellData(arrayMusic: musicArray, indexPath: myCount)
            
                  }
                }
            }
        }
        else {
            
            cell.cellData(arrayMusic: musicArray, indexPath : indexPath)
        }
        
          cell.selectionStyle = .none
          cell.backgroundColor = UIColor.clear
          cell.separatorInset = UIEdgeInsetsMake(0, 1000, 0, 0);
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104.0
    }
 
}
//MARK:- PICKER DELEGATE METHODS
extension MusicListVC : UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK: - Picker Datasource and delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrPicker.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        let dic = arrPicker[row]
       let title = (dic as! GenreData).name
 
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        print("you clicked here \(row)")
        
      let dic = arrPicker[row]
        genreid = (dic as! GenreData).id
        
        
        
    }
    
}




