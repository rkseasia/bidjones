//
//  TalkVC.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/3/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseDatabase

class TalkVC:  UIViewController , UIScrollViewDelegate {
    
    
    
    //MARK: - Outlets
    
    //Constarints
    @IBOutlet var viewSlideLeading: NSLayoutConstraint!
    @IBOutlet weak var tblBuyerList: UITableView!
    @IBOutlet weak var tblSellerList: UITableView!
    
    @IBOutlet var lblNoItem: UILabel!
        //MARK: - Variables
    var countSent = 0
    var countRecieve = 0
    var selectedIndex = 0
    
    
    //MARK: - Bool Variable
    var isLoading : Bool?
    var isScrolling:Bool?
    var dataLoaded:Bool?
    var isReceiveSelected:Bool?
    //Array
    private lazy var arrRecived = [TalkData]()
    private lazy var arrSent = [TalkData]()
    
    var db : DocumentReference!
    var a = true
    var collection = Firestore.firestore()
    override func viewDidLoad() {
        super.viewDidLoad()
        

       // if a == true {
           let  db1 = Firestore.firestore()
            let settings = db1.settings
            settings.areTimestampsInSnapshotsEnabled = true
            db1.settings = settings
            let date2 = FieldValue.serverTimestamp()
          //  var docRef: DocumentReference!

        db1.collection("TestChat").document().setData(["created_at" : date2] , merge: true)
        
//        var  docRef = db1.collection("TestChat").document()
//
//        docRef.setData(["created_at" : date2])
          //  DocumentReference.addSnapshotListener(docRef)
//            docRef.collection("TestChat").addDocument(data: ["created_at" : date2], completion: {
//                (error) in
//
//                if ((error) != nil) {
//
//                    print("your error is :")
//                }
//                else {
//
//                    print("you in sccuess message")
//
//                }
//            })
          //  docRef
//                .addSnapshotListener { querySnapshot, err in
//
//
//                            if let err = err {
//                                print("Error getting documents: \(err)")
//                            } else {
//
//                                print("Here is entery ")
//
//                                print("whole data is : \(querySnapshot)")
//
////                                for document in querySnapshot!.documents {
////                                    let docId = document.documentID
////                                  let com =  document.data()
////                                    print("com : \(com)")
////                                   // print("my data : \(document)")
////                                }
//                }
//
//            }
        //    docRef.setData(["created_at" : date2])
//            docRef.setData(["created_at" : date2], merge: true) { (error) in
//
//                print(2)
//
//                if error != nil {
//                    // self.activityIndicator.stopAnimating()
//                    print("Error adding document: \(error.debugDescription)")
//                } else {
//                    // self.activityIndicator.stopAnimating()
//                    print("Document added")
//                }
//
//                print(3)
//            }
            
//            db.collection("TestChat").document()
//             let date2 = FieldValue.serverTimestamp()
//            let data = ["created_at" : date2] as! [String : Any]
//
//            db.setData(data , merge: true)


       // }
        
        self.automaticallyAdjustsScrollViewInsets = false

//         let date2 = FieldValue.serverTimestamp()
//
//        db = Firestore.firestore().collection("TestChat").document()
            //.document("sampleData")

        
//        let settings = db.settings
//        settings.areTimestampsInSnapshotsEnabled = true
//        db.settings = settings
        
//        let data = ["created_at" : date2] as! [String : Any]
//
//        db.setData(data , merge: true)

        
        
//        // old:
//        let date: Date = documentSnapshot.get("created_at") as! Date
//        // new:
//        let timestamp: Timestamp = documentSnapshot.get("created_at") as! Timestamp
      
        
//        
//        db.collection("chats").addSnapshotListener { querySnapshot, err in
//            
//            
//            if let err = err {
//                print("Error getting documents: \(err)")
//            } else {
//                
//                print("Here is entery ")
//                
//                print("whole data is : \(querySnapshot?.documents)")
//                
//                for document in querySnapshot!.documents {
//                    let docId = document.documentID
//                  let com =  document.data()
//                    print("com : \(com)")
//                   // print("my data : \(document)")
//                }
//                
//            }
//        }
//        
//        db.collection("test").document("testDocument").setData(["created_at" : date2]) { (error) in
//
//            print(2)
//
//            if error != nil {
//               // self.activityIndicator.stopAnimating()
//                print("Error adding document: \(error.debugDescription)")
//            } else {
//               // self.activityIndicator.stopAnimating()
//                print("Document added")
//            }
//
//            print(3)
//        }
        
        
        
        isLoading = false
        dataLoaded = false
        isScrolling = false
        isReceiveSelected = false
      GetBuyerList()
        self.tblBuyerList.tableFooterView = UIView()
        self.tblSellerList.tableFooterView = UIView()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBActions
    @IBAction func BackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func Buyer_Action(_ sender: Any) {
        
        self.lblNoItem.isHidden = true
        //self.tblRecieveList.bringSubview(toFront: tblSentList)
        tblSellerList.isHidden = true
        tblBuyerList.isHidden = false
        
        isReceiveSelected = false
        self.viewSlideLeading.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if arrSent.count == 0
        {
            GetBuyerList()
        }
        
    }
    
    @IBAction func Seller_Action(_ sender: Any) {
        
        
        self.lblNoItem.isHidden = true
        
        //self.tblSentList.bringSubview(toFront: tblRecieveList)
        tblSellerList.isHidden = false
        tblBuyerList.isHidden = true
        
        isReceiveSelected = true
        self.viewSlideLeading.constant = self.view.frame.size.width/2
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if arrRecived.count == 0
        {
            GetSellerList()
        }
        
        
        
    }
    
   
    
    //MARK: - UISCrollview delegate
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func  scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            if isReceiveSelected == true{
                if (arrRecived.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
                {
                    print("222222222222")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    self.countRecieve = self.countRecieve+KPaginationcount
                    GetSellerList()
                }
            }
            else
            {
                if (arrSent.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
                {
                    print("222222222222")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    self.countSent = self.countSent+KPaginationcount
                    GetBuyerList()
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        print("11111111111")
        isScrolling = false
    }
    
    
//    //MARK: - Other Actions
//    func DeleteItem()
//    {
//        print(selectedIndex)
//        arrSent.remove(at: selectedIndex)
//        tblSentList.reloadData()
//
//    }
//    func PaymentRequestAction(_ sender: BidRecieveCell) {
//        KCommonFunctions.PushToContrller(from: self, ToController: .PaymentRequestSeller, Data: arrRecived[sender.tag])
//    }
//    func CheckOutAction(_ sender: BidSentCell) {
//        print(sender.tag)
//        selectedIndex = sender.tag
//        let obj =  arrSent[sender.tag]
//        print(obj)
//        if obj.categoryID==4
//        {
//            KCommonFunctions.PushToContrller(from: self, ToController: .PaymentRequestBuyer, Data: arrSent[sender.tag])
//        }
//        else
//        {
//            KCommonFunctions.PushToContrller(from: self, ToController: .CheckOut, Data: obj)
//        }
//    }
    func GetSellerList()  {
//        if let userID = UserDefaults.standard.getUserID()
//        {
            let urlStr = "/chatMembers"
            print(urlStr)
            dataLoaded = false
            //parm[Kcategory_id] = KServicesType
        
        var parm = ["chatgroup" : "seller", "limit" : 10 , "offset" : 0] as [String : Any]
        
        
        Talk.sharedManager.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
                
                print(Response.count)
                if Response.count>0
                {
                    if(self.countRecieve == 0)
                    {
                        // self.arrServiceSell = Response
                        for item in Response
                        {
                            let oldIds = self.arrRecived.map { $0.index! } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.index!))!)
                            {
                                self.arrRecived.insert(item, at: self.arrRecived.count)
                            }
                        }
                        self.tblSellerList.reloadData()
                    }
                    else
                    {
                        for item in Response
                        {
                            let oldIds = self.arrRecived.map { $0.index! } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.index!))!)
                            {
                                self.arrRecived.insert(item, at: self.arrRecived.count)
                            }
                        }
                        self.tblSellerList.reloadData()
                        print(self.arrRecived)
                    }
                    print(self.countRecieve)
                    self.lblNoItem.isHidden = true
                    self.tblSellerList.isHidden = false
                    
                }
                else
                {
                    if(self.countRecieve != 0)
                    {
                        print("abcdefgh")
                        self.dataLoaded = true
                    }
                    if (self.countRecieve == 0 || self.arrRecived.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                        self.tblSellerList.isHidden = true
                    }
                }
                self.isLoading = false
                //            if self.isDeleted!
                //            {
                //                self.isDeleted = false
                //                self.showAlertMessage(titleStr: KMessage, messageStr: "Item successfully deleted")
                //            }
            }
                , completionnilResponse: { (Response) in
                    //print(Response)
                    let statusCode = Response[Kstatus] as! Int
                    if statusCode == 500
                    {
                        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                            //print(resonse)
                            self.GetSellerList()
                        })
                    }
                    else if statusCode == 201
                    {
                        self.dataLoaded = true
                        if (self.countRecieve == 0 || self.arrRecived.count == 0)
                        {
                            self.lblNoItem.isHidden = false
                            self.tblSellerList.isHidden = true
                            
                        }
                    }
                    else
                    {
                        if (self.countRecieve == 0 || self.arrRecived.count == 0)
                        {
                            self.arrRecived.removeAll()
                            self.tblSellerList.reloadData()
                            self.lblNoItem.isHidden = false
                            self.tblSellerList.isHidden = true
                            
                        }
                        
                    }
                    self.isLoading = false
                    
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                self.isLoading = false
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                self.isLoading = false
            })
        //}
    }
    
    func GetBuyerList()  {
        if let userID = UserDefaults.standard.getUserID()
        {
            let urlStr = "/chatMembers"
            print(urlStr)
            dataLoaded = false
            let parm = ["chatgroup" : "buyer", "limit" : 10 , "offset" : 0] as [String : Any]
            
            
            Talk.sharedManager.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
                
                print(Response.count)
                if Response.count>0
                {
                    print("you get buyer response : \(Response)")
                    if(self.countSent == 0)
                    {
                        // self.arrServiceSell = Response
                        for item in Response
                        {
                            let oldIds = self.arrSent.map { $0.index! } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.index!))!)
                            {
                                self.arrSent.insert(item, at: self.arrSent.count)
                            }
                        }
                        self.tblBuyerList.reloadData()
                    }
                    else
                    {
                        for item in Response
                        {
                            let oldIds = self.arrSent.map { $0.index! } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.index!))!)
                            {
                                self.arrSent.insert(item, at: self.arrSent.count)
                            }
                        }
                        self.tblBuyerList.reloadData()
                        print(self.arrSent)
                    }
                    print(self.countSent)
                    self.lblNoItem.isHidden = true
                    self.tblBuyerList.isHidden =  false
                }
                else
                {
                    if(self.countSent != 0)
                    {
                        print("abcdefgh")
                        self.dataLoaded = true
                    }
                    if (self.countSent == 0 || self.arrSent.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                        self.tblBuyerList.isHidden = true
                        
                    }
                }
                self.isLoading = false
            }
                , completionnilResponse: { (Response) in
                    //print(Response)
                    let statusCode = Response[Kstatus] as! Int
                    if statusCode == 500
                    {
                        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                            //print(resonse)
                            self.GetBuyerList()
                        })
                    }
                    else if statusCode == 201
                    {
                        self.dataLoaded = true
                        if (self.countSent == 0 || self.arrSent.count == 0)
                        {
                            self.lblNoItem.isHidden = false
                            self.tblBuyerList.isHidden = true
                            
                        }
                    }
                    else
                    {
                        if (self.countSent == 0 || self.arrSent.count == 0)
                        {
                            self.arrSent.removeAll()
                            self.tblBuyerList.reloadData()
                            self.lblNoItem.isHidden = false
                            self.tblBuyerList.isHidden = true
                        }
                        
                    }
                    self.isLoading = false
                    
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                self.isLoading = false
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                self.isLoading = false
            })
        }
    }
}
extension TalkVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrBooks.count)
        if tableView == tblSellerList
        {
            return arrRecived.count
        }
        else
        {
            return arrSent.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblSellerList
        {
            return 100
        }
        else
        {
            return 100
        }
        // return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblSellerList
        {
            let cell : TalkSellerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TalkSellerTableViewCell") as! TalkSellerTableViewCell
            cell.LoadData(dic:arrRecived[indexPath.row])
           // cell.tag = indexPath.row
           // cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell : TalkBuyerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TalkBuyerTableViewCell") as! TalkBuyerTableViewCell
            cell.LoadData(dic:arrSent[indexPath.row])
//            cell.tag = indexPath.row
//            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }
    }
}




extension TalkVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        UserDefaults.standard.setShowEditButton(value: false)
        UserDefaults.standard.setShowSellerInfo(value: true)
        
        self.tblSellerList.deselectRow(at: indexPath, animated: true)
        self.tblBuyerList.deselectRow(at: indexPath, animated: true)
            
           // self.tableView.deselectRow(at: indexPath, animated: true)
            
            
       
        if tableView == tblSellerList
        {
           // if let data1 = arrRecived[indexPath.row].userImage {
             var data = arrRecived[indexPath.row]
            print("\(data.itemID!)")
            
            let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ChatRoomVC") as! ChatRoomVC
            
            let id = UserDefaults.standard
            
            let loginUserID = id.getUserID()
            
            vc.login_userID = "\(loginUserID!)"
            vc.anotherUserID = "\(data.userId!)"
            vc.itemID = "\(data.itemID!)"
            vc.talkData = data
            navigationController?.pushViewController(vc, animated: true)
            
            
          //  }
            
            
          //  CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
        }
        else
        {
          //  if  let data1 = arrSent[indexPath.row].userImage {
                var data = arrSent[indexPath.row]
           // CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
            
            let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ChatRoomVC") as! ChatRoomVC
            let id = UserDefaults.standard
            
            let loginUserID = id.getUserID()
            
            vc.login_userID = "\(loginUserID!)"
            vc.anotherUserID = "\(data.userId!)"
             vc.itemID = "\(data.itemID!)"
             vc.talkData = data
            navigationController?.pushViewController(vc, animated: true)
           
           // }
        }
    }
}


