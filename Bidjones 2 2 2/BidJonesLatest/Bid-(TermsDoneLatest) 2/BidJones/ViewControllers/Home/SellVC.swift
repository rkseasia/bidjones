//
//  SellVC.swift
//  BidJones
//
//  Created by Harpreet Singh on 20/03/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class SellVC: UIViewController {
    
    @IBOutlet var tbl_Sell_Items: UITableView!
    
    /**
     *  Array containing menu options
     */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tbl_Sell_Items.tableFooterView = UIView.init()
        tbl_Sell_Items.separatorColor = .clear
        
       
//       self.navigationController?.navigationBar.backIndicatorImage = UIImage.init(named: Kback_arrow)
//        self.navigationController?.navigationBar.topItem?.title = "";
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        updateArrayMenuOptions()
    }
    
    func updateArrayMenuOptions(){
        arrayMenuOptions.removeAll()
        arrayMenuOptions.append([Ktitle:KSellServices, Kicon:Ksell_ic])
        arrayMenuOptions.append([Ktitle:KMyStuff, Kicon:Kstuff_ic])
        arrayMenuOptions.append([Ktitle:KMyMusic, Kicon:Kmusic_ic])
        arrayMenuOptions.append([Ktitle:KMyBooks, Kicon:Kbook_ic])
        arrayMenuOptions.append([Ktitle:KMyVideos, Kicon:Kvideo_ic])
        arrayMenuOptions.append([Ktitle:KMyGraphics, Kicon:Kgraphics_ic])
        arrayMenuOptions.append([Ktitle:KTradeItems, Kicon : "product"])
        arrayMenuOptions.append([Ktitle:KFreeItems, Kicon: "product"])
        tbl_Sell_Items.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SellVC:UITableViewDelegate,UITableViewDataSource{

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : SellTableViewCell = tableView.dequeueReusableCell(withIdentifier: Kcell) as! SellTableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        
        switch indexPath.row {
        case 0:
        cell.btnImg_Menu.imageEdgeInsets = UIEdgeInsets(top: 2, left:5, bottom: 2, right: 5)
            break
        case 1:
        cell.btnImg_Menu.imageEdgeInsets = UIEdgeInsets(top: 2, left:2, bottom: 2, right: 2)
            break
        case 2:
        cell.btnImg_Menu.imageEdgeInsets = UIEdgeInsets(top: 5, left:5, bottom: 5, right: 5)
            break
        case 3:
        cell.btnImg_Menu.imageEdgeInsets = UIEdgeInsets(top: 4, left:7, bottom: 4, right: 7)
            break
        case 4:
        cell.btnImg_Menu.imageEdgeInsets = UIEdgeInsets(top: 8, left:0, bottom: 8, right: 0)
            break
        case 5:
        cell.btnImg_Menu.imageEdgeInsets = UIEdgeInsets(top: 3, left:3, bottom: 3, right: 3)
            break
       default:
          break
        }
        cell.btnImg_Menu.setImage(UIImage(named: arrayMenuOptions[indexPath.row][Kicon]!), for: .normal)

        cell.lbl_Menu.text = arrayMenuOptions[indexPath.row][Ktitle]!
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        UserDefaults.standard.setShowEditButton(value: true)
        DispatchQueue.main.async {
        switch indexPath.row {
        case 0:
            KCommonFunctions.PushToContrller(from: self, ToController: .MYServiceSellList, Data: nil)
        case 1:
            KCommonFunctions.PushToContrller(from: self, ToController: .MyStuffList, Data: nil)
        case 2:
            KCommonFunctions.PushToContrller(from: self, ToController: .MySongList, Data: nil)
        case 3:
            KCommonFunctions.PushToContrller(from: self, ToController: .MYBookSList, Data: nil)
        case 4:
            KCommonFunctions.PushToContrller(from: self, ToController: .MyVideoList, Data: nil)
        case 5:
            KCommonFunctions.PushToContrller(from: self, ToController: .MyGraphicsList, Data: nil)
        case 6:
            KCommonFunctions.PushToContrller(from: self, ToController: .MyTradeItemsVC, Data: nil)
        case 7:
             KCommonFunctions.PushToContrller(from: self, ToController: .MyFreeItemsVC, Data: nil)
        default:
                self.showAlertMessage(titleStr: KMessage, messageStr: KComingSoon)
}
}
}
}
