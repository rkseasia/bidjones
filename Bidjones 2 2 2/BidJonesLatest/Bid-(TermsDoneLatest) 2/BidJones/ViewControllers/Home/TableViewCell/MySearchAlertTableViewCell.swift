//
//  MySearchAlertTableViewCell.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/2/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

protocol MySearchAlertTableViewCellDelegate: class{
    func view_Action(sender: MySearchAlertTableViewCell)
}

class MySearchAlertTableViewCell: UITableViewCell {

    weak var delegate: MySearchAlertTableViewCellDelegate?
    
    @IBOutlet weak var lbl_description: UILabel!
    
    @IBOutlet weak var img_Service: UIImageView!
    
    @IBOutlet weak var lbl_ServiceName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func cellData(array : [ItemListData], indexPath : IndexPath) {
//
//        lbl_ServiceName.text = array[indexPath.row].shortTitle!
//            //array[indexPath.row].titleName!
//
//        let imageString = array[indexPath.row].imgName!
//
//        let url = URL(string:imageString)
//
//        img_Service.sd_setImage(with: url!)
//
//        lbl_description.text = array[indexPath.row].minBid!
//
//        //   btn_play.addTarget(self, action: #selector(songPlay(songName :)), for: .touchUpInside)
//
//    }
  
  func LoadData(dic:Any,type:CellType)
  {
    if let data = dic as? ItemListData
    {
      img_Service.contentMode = .scaleAspectFill
      img_Service.clipsToBounds = true
      print(data)
      lbl_ServiceName.text = data.title
      lbl_description.text = data.descrption

      img_Service.image = UIImage.init(named:"sound_placeholder")
      switch type
      {
      case .Music:
        img_Service.image = UIImage.init(named:"sound_placeholder")
      case .Book:
        img_Service.image =  UIImage.init(named:"book_placeholder")
      case .Stuff:
        img_Service.image = UIImage.init(named:"stuff_placeholder")
      case .SellService:
        img_Service.image = UIImage.init(named:"services_placeholder")
      case .video:
        img_Service.image = UIImage.init(named:"video_2_")
      case .graphic:
        img_Service.image = UIImage.init(named:"graphic_2_")
      }
      
      if let categoryID = data.categoryID
      {
        switch categoryID
        {
        case 1:
          img_Service.image =  UIImage.init(named:"book_placeholder")
        case 2:
          img_Service.image = UIImage.init(named:"graphic_2_")
        case 3:
          img_Service.image = UIImage.init(named:"sound_placeholder")
        case 4:
          img_Service.image = UIImage.init(named:"services_placeholder")
        case 5:
          img_Service.image = UIImage.init(named:"stuff_placeholder")
        case 6:
          img_Service.image = UIImage.init(named:"video_2_")
        default:
          img_Service.image = UIImage.init(named:"logo")
        }
      }
      if let imgUrlStr = data.ImageUrl
      {
        LoadImage(urlStr:imgUrlStr)
      }
    }
    print(dic)
  }
  
  func LoadImage(urlStr:String)
  {
    print(urlStr)
    var imageCache = SDImageCache.shared().imageFromCache(forKey: urlStr)
    
    if let Image = imageCache {
      print(Image)
      print(img_Service)
      img_Service.image = Image
    }
    else if let Image = SDImageCache.shared().imageFromDiskCache(forKey: urlStr)
    {
      img_Service.image = Image
    }
    else{
      imageCache = SDImageCache.shared().imageFromDiskCache(forKey: urlStr)
      if imageCache != nil {
        img_Service.image = imageCache
      }
      else
      {
        SDWebImageManager.shared().loadImage(with: URL(string:urlStr), options: .highPriority , progress: { (receivedSize :Int, ExpectedSize :Int, url : URL?) in
          
          
        }, completed: { (image : UIImage?, data : Data?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
          
          DispatchQueue.main.async {
            self.img_Service.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached , completed: nil)
          }
        })
      }
    }
  }
  
    @IBAction func view_Action(_ sender: Any)
    {
        delegate?.view_Action(sender: self)
    }

}
