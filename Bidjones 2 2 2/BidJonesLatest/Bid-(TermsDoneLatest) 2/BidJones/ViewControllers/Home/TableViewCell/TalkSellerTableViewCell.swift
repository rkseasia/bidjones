//
//  TalkSellerTableViewCell.swift
//  BidJones
//
//  Created by Kuldeep Singh on 10/3/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import SDWebImage

class TalkSellerTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet var imgView: UIImageView!
    
    @IBOutlet weak var lbl_UserName: UILabel!
    
    @IBOutlet weak var lbl_itemName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func LoadData(dic: TalkData )
    {
        
        lbl_UserName.text = dic.username!
        lbl_itemName.text = dic.itemName!
        
        let imageString = dic.userImage!
        
        let url = URL(string:imageString)
        
     if let url1 = url as? URL {
    
       imgView.sd_setImage(with: url1)
        
        }
        
    }
    
    
}
