//
//  MenuTableViewCell.swift
//  BidJones
//
//  Created by Gurleen Osahan on 28/09/20.
//  Copyright © 2020 Seasia. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {
    
  @IBOutlet var imgView : UIImageView!
  @IBOutlet weak var btn_ImgIcon: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
