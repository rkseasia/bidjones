//
//  AlertsVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 7/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class AlertsVC:  BaseViewController {
    var titleArr = [String]()
    var imageArr = [String]()
    
    static var isAlertVC = false
    

    @IBOutlet var tblAlerts: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
     titleArr = ["Services","Merch Pickup","Talks","My Search Alerts","My Sales"]
      imageArr = ["service","merch_pickup","talks","alert","purchases"]
        tblAlerts.tableFooterView = UIView()

        self.automaticallyAdjustsScrollViewInsets = false
        
        
        // Do any additional setup after loading the view.
    }
    @IBAction func BackAction(_ sender: Any)
    {
        AlertsVC.isAlertVC = true
      
        //self.onSlideMenuButtonPressed(btnShowMenu)
        
     //   KCommonFunctions.PushToContrller(from: self, ToController: .Alerts, Data: nil)
        
       // addSlideMenuButton()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
extension AlertsVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if ( indexPath.row == 0) {
            
            KCommonFunctions.PushToContrller(from: self, ToController: .ServiceVC, Data: nil)
        }
        
        if (indexPath.row == 1)
        {
            KCommonFunctions.PushToContrller(from: self, ToController: .MerchPickUp, Data: nil)
        }
        
        if (indexPath.row == 2) {
            
            KCommonFunctions.PushToContrller(from: self, ToController: .Talk, Data: nil)
            
        }
        
        
        
        if (indexPath.row == 3){
            
            
            KCommonFunctions.PushToContrller(from: self, ToController: .MySearchAlert, Data: nil)
            
            
        }
        
        if (indexPath.row == 4) {
            
            
            KCommonFunctions.PushToContrller(from: self, ToController: .MySale, Data: nil)
            
        }
        //        else
//        {
//        DispatchQueue.main.async {
//            self.showAlertMessage(titleStr: KMessage, messageStr: KComingSoon)
//        }
        //}
    }
}

extension AlertsVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrServiceSell.count)
        return imageArr.count
    }
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return 100
    //    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let cell : AlertTableCell = tableView.dequeueReusableCell(withIdentifier: "alertTableCell") as! AlertTableCell
         cell.LoadData(Title: titleArr[indexPath.row], ImageStr: imageArr[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
}
