//
//  HistoryVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 6/15/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class HistoryVC: UIViewController,UIScrollViewDelegate,BidSentCellDelegate {
    
    //MARK: - Outlets
    //Constarints
    
    
    
    @IBOutlet var viewSlideLeading: NSLayoutConstraint!
    //Table
    @IBOutlet var tblSellList: UITableView!
    @IBOutlet var tblPurchaseList: UITableView!
    
    //Label
    @IBOutlet var lblNoItem: UILabel!
    
    //MARK: - Variables
    var countPurchase = 0
    var countSell = 0
    
    //MARK: - Bool Variable
    var isLoading : Bool?
    var isScrolling:Bool?
    var dataLoaded:Bool?
    var isSellSelected:Bool?
    //Array
    private lazy var arrSell = [ItemListData]()
    private lazy var arrPurchase = [ItemListData]()
    static var isMyPurchase = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isSellSelected = false
        GetPurchaseList()
        isLoading = false
        dataLoaded = false
        isScrolling = false
        self.tblSellList.tableFooterView = UIView()
        self.tblPurchaseList.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: IBActions
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func MyPurchasesAction(_ sender: Any)
    {
        self.lblNoItem.isHidden = true

        //self.tblSellList.bringSubview(toFront: tblPurchaseList)
        tblSellList.isHidden = true
        tblPurchaseList.isHidden = false
        
        isSellSelected = false
        self.viewSlideLeading.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if arrPurchase.count == 0
        {
            GetPurchaseList()
        }
    }
    @IBAction func MySellsAction(_ sender: Any)
    {
        self.lblNoItem.isHidden = true

        //self.tblPurchaseList.bringSubview(toFront: tblSellList)
        tblSellList.isHidden = false
        tblPurchaseList.isHidden = true
        
        isSellSelected = true
        self.viewSlideLeading.constant = self.view.frame.size.width/2
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if arrSell.count == 0
        {
            GetSellList()
        }
    }
    //MARK: - UISCrollview delegate
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
    }
    
    func  scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
    {
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
            if isSellSelected == true{
                if (arrSell.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
                {
                    print("222222222222")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    self.countSell = self.countSell+KPaginationcount
                    GetSellList()
                }
            }
            else
            {
                if (arrPurchase.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
                {
                    print("222222222222")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    self.countPurchase = self.countPurchase+KPaginationcount
                    GetPurchaseList()
                }
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        print("11111111111")
        isScrolling = false
    }
    
    
    //MARK: - Other Actions
    
    func CheckOutAction(_ sender: BidSentCell) {
        print(sender.tag)
        let obj =  arrPurchase[sender.tag]
        KCommonFunctions.PushToContrller(from: self, ToController: .CheckOut, Data: obj)
    }
    func GetSellList()  {
        if let userID = UserDefaults.standard.getUserID()
        {
            let urlStr = KitemHistory
            var parm = [String : Any]()
            parm["offset"] = countSell
            parm["limit"]  = KPaginationcount
            parm["user_type"] = "seller"
            parm["user_id"] = userID
            print(parm)
            print(urlStr)
            dataLoaded = false
            History.sharedManager.PostSellApi(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
                
                print(Response.count)
                if Response.count>0
                {
                    if(self.countSell == 0)
                    {
                        // self.arrServiceSell = Response
                        for item in Response
                        {
                            let oldIds = self.arrSell.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrSell.insert(item, at: self.arrSell.count)
                            }
                        }
                        self.tblSellList.reloadData()
                    }
                    else
                    {
                        for item in Response
                        {
                            let oldIds = self.arrSell.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrSell.insert(item, at: self.arrSell.count)
                            }
                        }
                        self.tblSellList.reloadData()
                        print(self.arrSell)
                    }
                    print(self.countSell)
                    self.lblNoItem.isHidden = true
                    self.tblSellList.isHidden = false
                    
                }
                else
                {
                    if(self.countSell != 0)
                    {
                        print("abcdefgh")
                        self.dataLoaded = true
                    }
                    if (self.countSell == 0 || self.arrSell.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                        self.tblSellList.isHidden = true
                    }
                }
                self.isLoading = false
                //            if self.isDeleted!
                //            {
                //                self.isDeleted = false
                //                self.showAlertMessage(titleStr: KMessage, messageStr: "Item successfully deleted")
                //            }
            }
                , completionnilResponse: { (Response) in
                    //print(Response)
                    let statusCode = Response[Kstatus] as! Int
                    if statusCode == 500
                    {
                        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                            //print(resonse)
                            self.GetSellList()
                        })
                    }
                    else if statusCode == 201
                    {
                        self.dataLoaded = true
                        if (self.countSell == 0 || self.arrSell.count == 0)
                        {
                            self.lblNoItem.isHidden = false
                            self.tblSellList.isHidden = true
                            
                        }
                    }
                    else
                    {
                        if (self.countSell == 0 || self.arrSell.count == 0)
                        {
                            self.arrSell.removeAll()
                            self.tblSellList.reloadData()
                            self.lblNoItem.isHidden = false
                            self.tblSellList.isHidden = true
                            
                        }
                        
                    }
                    self.isLoading = false
                    
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                self.isLoading = false
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                self.isLoading = false
            })
        }
    }
    
    func GetPurchaseList()  {
        if (1 == 1)
        {
            
        }
        else
        {
            
        }
        
        if let userID = UserDefaults.standard.getUserID()
        {
            let urlStr = KitemHistory
            var parm = [String : Any]()
            parm["offset"] = countPurchase
            parm["limit"]  = KPaginationcount
            parm["user_type"] = "buyer"
            parm["user_id"] = userID
            print(urlStr)
            dataLoaded = false
            History.sharedManager.PostPurchaseApi(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
                
                print(Response.count)
                if Response.count>0
                {
                    if(self.countPurchase == 0)
                    {
                        for item in Response
                        {
                            let oldIds = self.arrPurchase.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrPurchase.insert(item, at: self.arrPurchase.count)
                            }
                        }
                        self.tblPurchaseList.reloadData()
                    }
                    else
                    {
                        for item in Response
                        {
                            let oldIds = self.arrPurchase.map { $0.id } as? [Int]
                            print(oldIds as Any)
                            if !((oldIds?.contains(item.id!))!)
                            {
                                self.arrPurchase.insert(item, at: self.arrPurchase.count)
                            }
                        }
                        self.tblPurchaseList.reloadData()
                        print(self.arrPurchase)
                    }
                    print(self.countPurchase)
                    self.lblNoItem.isHidden = true
                    self.tblPurchaseList.isHidden =  false
                }
                else
                {
                    if(self.countPurchase != 0)
                    {
                        print("abcdefgh")
                        self.dataLoaded = true
                    }
                    if (self.countPurchase == 0 || self.arrPurchase.count == 0)
                    {
                        self.lblNoItem.isHidden = false
                        self.tblPurchaseList.isHidden = true
                        
                    }
                }
                self.isLoading = false
            }
                , completionnilResponse: { (Response) in
                    //print(Response)
                    let statusCode = Response[Kstatus] as! Int
                    if statusCode == 500
                    {
                        KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                            //print(resonse)
                            self.GetPurchaseList()
                        })
                    }
                    else if statusCode == 201
                    {
                        self.dataLoaded = true
                        if (self.countPurchase == 0 || self.arrPurchase.count == 0)
                        {
                            self.lblNoItem.isHidden = false
                            self.tblPurchaseList.isHidden = true
                        }
                    }
                    else
                    {
                        if (self.countPurchase == 0 || self.arrPurchase.count == 0)
                        {
                            self.arrPurchase.removeAll()
                            self.tblPurchaseList.reloadData()
                            self.lblNoItem.isHidden = false
                            self.tblPurchaseList.isHidden = true
                        }
                    }
                    self.isLoading = false
                    
            }, completionError: { (error) in
                self.showAlertMessage(titleStr: KMessage, messageStr:KError)
                self.isLoading = false
            },networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
                self.isLoading = false
            })
        }
    }
}
extension HistoryVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UserDefaults.standard.setShowEditButton(value: false)
        UserDefaults.standard.setShowSellerInfo(value: true)

        if tableView == tblSellList
        {
            
             HistoryVC.isMyPurchase = false
            
            let data = arrSell[indexPath.row]
            CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
        }
        else
        {
            HistoryVC.isMyPurchase = true
            let data = arrPurchase[indexPath.row]
            print("your data purchase array : \(data)")
            CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
            
        }
       
//        DispatchQueue.main.async {
//            self.showAlertMessage(titleStr: KMessage, messageStr: KComingSoon)
//        }
        //tableView.deselectRow(at: indexPath, animated: true)
      //  let data = arrGraphics[indexPath.row]
        // let itemId = data.id
      //  CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
    }
}
extension HistoryVC : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(arrBooks.count)
        if tableView == tblSellList
        {
            return arrSell.count
        }
        else
        {
            return arrPurchase.count
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if tableView == tblSellList
        //        {
        //            return UITableViewAutomaticDimension
        //        }
        //        else
        //        {
        //            return UITableViewAutomaticDimension
        //        }
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblSellList
        {
            let cell : HistoryCell = tableView.dequeueReusableCell(withIdentifier: "historyCell") as! HistoryCell
            cell.LoadData(dic:arrSell[indexPath.row], type: .Book)
            cell.tag = indexPath.row
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell : HistoryCell = tableView.dequeueReusableCell(withIdentifier: "historyCell") as! HistoryCell
            cell.LoadData(dic:arrPurchase[indexPath.row], type: .Book)
            cell.tag = indexPath.row
            cell.selectionStyle = .none
            return cell

        }
    }
}

