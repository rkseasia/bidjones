//
//  MenuViewController.swift
//  BidJones
//
//  Created by Harpreet Singh on 20/03/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    /**
     *  Array to display menu options
     */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
     *  Transparent button to hide menu
     */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
     *  Array containing menu options
     */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
     *  Menu button which was tapped to display the menu
     */
    var btnMenu : UIButton!
    
    /**
     *  Delegate of the MenuVC
     */
    var delegate : SlideMenuDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenuOptions.tableFooterView = UIView()
        tblMenuOptions.layoutMargins = UIEdgeInsets.zero
        tblMenuOptions.separatorInset = UIEdgeInsets.zero
        arrayMenuOptions.removeAll()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        arrayMenuOptions.removeAll()
        updateArrayMenuOptions()
    }
    
    func updateArrayMenuOptions(){
        //gurleen
        arrayMenuOptions.append([Ktitle:KMyProfile, Kicon:Kmy_profile_ic])
        arrayMenuOptions.append([Ktitle:KStuffSales, Kicon:Kstuff_ic])
        arrayMenuOptions.append([Ktitle:KServicesSales, Kicon:Kservices_ic])
        arrayMenuOptions.append([Ktitle:KMusicSales, Kicon:Kmusic_ic])
        arrayMenuOptions.append([Ktitle:KBookSales, Kicon:Kbook_ic])
        arrayMenuOptions.append([Ktitle:KVideoSales, Kicon:Kvideo_ic])
        
       // arrayMenuOptions.append([Ktitle:KSettings, Kicon:Ksetting_menu])
        arrayMenuOptions.append([Ktitle:KAlerts, Kicon:Kalert_menu])
        arrayMenuOptions.append([Ktitle:"Bids", Kicon:"bid_hover"])
        arrayMenuOptions.append([Ktitle:"History", Kicon:"history_hover"])
        arrayMenuOptions.append([Ktitle:KHelp, Kicon:Khelp_menu])
        arrayMenuOptions.append([Ktitle:KAboutUs, Kicon:Kabt_us_menu])
        arrayMenuOptions.append([Ktitle:"Change Password", Kicon:"passwod_ic"])
        arrayMenuOptions.append([Ktitle:KLogout, Kicon:Klogout])
        tblMenuOptions.reloadData()
    }
    
    func logout() {
        
        let id = UserDefaults.standard
        let loginUserID = id.getUserID()
        let isDevicetype = 1 as! NSNumber
        //NSNumber(1)
        
        let url = "/user/logout"
        let parm = ["is_mobile" : isDevicetype] as! [String : Any]
        
        Logout.sharedManager.PostApi(url: url, parameter: parm, Target: self , completionResponse: {
            (response) in
            
            print("my Logout result : \(response)")
            
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    //   self.GetRecieveBidList()
                    // self.getAlertData()
                    self.logout()
                })
            }
            else if statusCode == 201
            {
                
            }
            
            
            
            
            
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            
        }, networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
            
        })
        
        
        
    }
    
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
        }, completion: { (finished) -> Void in
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: KcellMenu)! as! MenuTableViewCell
        
        //cell.imgView.image = UIImage(named: arrayMenuOptions[indexPath.row][Kicon]!)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
         cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.backgroundColor = UIColor.clear
        cell.btn_ImgIcon.setImage(UIImage(named: arrayMenuOptions[indexPath.row][Kicon]!), for: .normal)
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        //let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
     
//
//        if indexPath.row  == 5{
//    //        print(imgIcon.frame.size)
//            cell.imgView.frame.size.width = 10.0 //CGRect(x:0.0,y:0.0,width:0.0,height:0.0) //Or whatever frame you think is necessary
//
//        }
        cell.selectionStyle = UITableViewCellSelectionStyle.none
         cell.layoutMargins = UIEdgeInsets.zero
         cell.preservesSuperviewLayoutMargins = false
         cell.backgroundColor = UIColor.clear
        if indexPath.row  == 5{
           cell.btn_ImgIcon.imageEdgeInsets = UIEdgeInsets(top: 9, left:3, bottom: 9, right: 2)
        }
     
        lblTitle.text = arrayMenuOptions[indexPath.row][Ktitle]!
     
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == arrayMenuOptions.count - 1 {
          
            DispatchQueue.main.async {
            let alertController = UIAlertController(title: KMessage, message: KDoyoureallywanttologout, preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: KLogout, style: UIAlertActionStyle.destructive) {
                UIAlertAction in
                NSLog("OK Pressed")
                btnShowMenu.tag = 0
                self.logout()
               //gagan
                 UserDefaults.standard.setStripeExist(value: false)
                
                UserDefaults.standard.setLoggedIn(value: false)
                UserDefaults.standard.setRefreshTokken(value: nil)
                UserDefaults.standard.setAccessTokken(value: nil)
              
                //userd// Bool
                KCommonFunctions.SetRootViewController(rootVC:.LoginNavigation)
            }
            let cancelAction = UIAlertAction(title: KCancel, style: UIAlertActionStyle.cancel) {
                UIAlertAction in
                NSLog("Cancel Pressed")
            }
            
            // Add the actions
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
                
            }
        }
        else{
        let btn = UIButton(type: UIButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
}

