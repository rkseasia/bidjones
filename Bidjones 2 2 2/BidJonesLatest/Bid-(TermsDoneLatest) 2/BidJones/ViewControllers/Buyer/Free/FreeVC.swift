
//
//  TradeVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 1/7/19.
//  Copyright © 2019 Seasia. All rights reserved.
//

import UIKit

protocol RemoveFreeItemDelegate: class{
    func RemoveFreeItemAction()
}

class FreeVC: BaseViewController, RemoveFreeItemDelegate, CustomStripeDelegate {
   
    
    
    @IBOutlet var viewFilter: UIView!
    
    @IBOutlet var lblUnit: CustomUILabel!
    
    @IBOutlet var lblRadius: CustomUILabel!
    
    @IBOutlet var searchBar: UISearchBar!
    private var pickerView = UIPickerView()
    @IBOutlet var txtFld_forOpeningPickers: UITextField!
    @IBOutlet var lblRadius_Width: NSLayoutConstraint!
    @IBOutlet var lblNoItem: UILabel!
    @IBOutlet var viewNotify: UIView!
    
    var account_already:Int?
    let url = "/user/detail"
    var arrRadius   = [String]()
    var arrUnit     = [String]()
    var arrGenre =   [GenreData]()
    var arrPicker = [Any]()
    var count = 0
    var dataLoaded:Bool?
    var isLoading:Bool?
    var isFilter:Bool?
    var userType : String?
    var user_Id:Int?
    var Index = 0
    var isDeleted:Bool?


    
    var isScrolling:Bool?
    
    
    @IBOutlet var tblList: UITableView!
    
    private lazy var arrList = [ItemListData]()
    
    
    enum PickerType: Int {
        case RadiusPicker = 1
        case UnitPicker = 2
        init() {
            self = .RadiusPicker
        }
    }
    private var pickerType: PickerType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblNoItem.isHidden = true
         dataLoaded = false
         isLoading = false
        isDeleted = false

      //  isScrolling = false
        // searchBar.placeholder = "Enter title"
        //isFilter = false
        lblNoItem.isHidden = true
        viewFilter.isHidden = true
        self.title = "SEARCH"
        drawerType = DrawerType.Free
        pickerView.dataSource = self
        searchBar.delegate = self
        pickerView.delegate = self
        lblRadius.text = "World Wide"
        lblRadius_Width.constant = 100
        lblUnit.text = "Miles"
        txtFld_forOpeningPickers.inputView = pickerView
        
        arrRadius.append("World Wide")
        
        for i in 1...20 {
            arrRadius.append(String(i*10))
        }
        
        for i in 3...15 {
            arrRadius.append(String(i*100))
        }
        arrRadius.append(String(2000
            
        ))
        
        
        print(arrRadius)
        
        arrUnit.append("Miles")
        arrUnit.append("Kms")
        print(arrUnit)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 3/255, green: 95/255, blue: 253/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: KDone, style: UIBarButtonItemStyle.plain, target: self, action: #selector(DonePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: KCancel, style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFld_forOpeningPickers.inputAccessoryView = toolBar
        self.hideKeyboardWhenTappedAround()
        
        addSlideMenuButton()
        let notificationButton = SSBadgeButton()
        let notifiImg:UIImage = UIImage(named: "add_product")!
        
        notificationButton.frame = CGRect(x: 0, y: 0, width: notifiImg.size.width, height: notifiImg.size.height)
        notificationButton.setImage(notifiImg.withRenderingMode(.alwaysTemplate), for: .normal)
        notificationButton.addTarget(self, action:#selector(self.AddProduct), for: .touchUpInside)
        notificationButton.tintColor = .black
        
        let radioiImg:UIImage = #imageLiteral(resourceName: "Filter")
        
        let radioView:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: radioiImg.size.width, height: radioiImg.size.height))
        radioView.addTarget(self, action:#selector(self.didTapFilterButton), for: .touchUpInside)
        radioView.setImage(radioiImg, for: .normal)
        let radioButton:UIBarButtonItem = UIBarButtonItem(customView: radioView)
        let NotifyButton = UIBarButtonItem(customView: notificationButton)
        
        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
        fixedSpace.width = 20.0
        
        // navigationItem.rightBarButtonItems = [NotifyButton,fixedSpace,radioButton]
       // navigationItem.rightBarButtonItem = NotifyButton
        // navigationItem.leftBarButtonItems = [NotifyButton,fixedSpace,radioButton]
        
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        //self.SearchMethod()
        viewNotify.isHidden = true
        
        searchBar.placeholder = "Enter keyword"
        isFilter = true
        viewFilter.isHidden = false
        tblList.isHidden = true
        
        
    }
    
    //MARK: - Navigations Actions
    
    @objc func didTapFilterButton(sender: AnyObject){
        searchBar.placeholder = "Enter keyword"
        isFilter = true
        viewFilter.isHidden = false
        tblList.isHidden = true
    }
    
    func AddBackButton()
    {
        var btnShowMenu = UIButton(type: UIButtonType.system)
        
        btnShowMenu = UIButton(type: UIButtonType.system)
        btnShowMenu.setImage(UIImage.init(named: "back_arrow"), for: UIControlState())
        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnShowMenu.tintColor = .black
        btnShowMenu.addTarget(self, action: #selector(BackAction), for: UIControlEvents.touchUpInside)
        
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.leftBarButtonItem = customBarItem;
        
    }
    
    @objc func BackAction()
    {
        if viewNotify.isHidden == true
        {
            self.title = "SEARCH"
            addSlideMenuButton()
            searchBar.placeholder = "Enter keyword"
            isFilter = true
            viewFilter.isHidden = false
            tblList.isHidden = true
            lblNoItem.isHidden = true
        }
        
    }
    
    @objc func AddProduct()
    {
        if viewNotify.isHidden == true
        {
            //    if let userType = UserDefaults.standard.getUserType(), userType == true {
            //
            //      KCommonFunctions.PushToContrller(from: self, ToController: .AddTradeProduct, Data: nil)
            //    }
            //
            //    else{
            //
            //      if  let type  = UserDefaults.standard.getMessageType() , type == "1" || type == "0" {
            //        print("here yor type : \(type)")
            //        print("Here your userType is \(self.userType)")
            //        self.userType = type
            //
            //        if  userType! == "1"  || userType! == "0"  {
            //
            //          if userType! == "1" {
            //
            //            userType = ""
            //            UserDefaults.standard.setShowMessageType(value: "")
            //
            //            showAlertMessage(titleStr: KMessage, messageStr:  "Upgraded as seller with Bidjones and Stripe is successfully. Please check your inbox and claim your stripe account to make sure that your payments arrive in your bank account without delay.")
            //
            //
            //
            //          }
            //          else {
            //
            //            UserDefaults.standard.setUserType(value: true)
            //            userType = ""
            //            KCommonFunctions.PushToContrller(from: self, ToController: .AddTradeProduct, Data: nil)
            //
            //
            //          }
            //
            //
            //
            //
            //        }
            //      }
            //
            //      else {
            //
            //        MakeSellerShowAlert()
            //
            //      }
            //
            //    }
            
               KCommonFunctions.PushToContrller(from: self, ToController: .AddFreeProduct, Data: nil)
            
//            if let userType = UserDefaults.standard.getUserType(), userType == true {
//
//                KCommonFunctions.PushToContrller(from: self, ToController: .AddFreeProduct, Data: nil)
//            }
//                //First save stripe id at login and check here
//                //if it is null then open webpage and
//                //when it comes with id response
//
//            else {
//                let checkvalue = self.isKeyPresentInUserDefaults(key:"ISStripeAccountExist")
//                if(checkvalue){
//                    let check = UserDefaults.standard.getStripeExist()
//                    if(check!){
//                        //code RUN
//                        KCommonFunctions.PushToContrller(from: self, ToController: .AddFreeProduct, Data: nil)
//                    }else{
//
//                        self.apiCall(url: url, vc: self)
//                    }
//                    // self.checkPermissonToSell()
//                    //if exist then check value
//                    //if no again hit api and check
//                    //again no open web
//                    //if yes code run
//
//
//                }else{
//
//                    //profile api hit
//
//                    self.apiCall(url: url, vc: self)
//                    //store to userdefalut
//                    //check if no then open web
//                    //if yes code run
//
//                }
//
//            }
//
//
//
//
//
        }
    }
    //MARK: CustomStripeDelegate
    
    func StripeAccountCreated() {
        
        UserDefaults.standard.setUserType(value: true)
    }
    
    func StripeAccountNotCreated() {
        
    }
    
    //MARK: - Other function
    func MakeSellerShowAlert()
    {
        let alertController = UIAlertController(title: KMessage, message: "You must have a stripe account linked with bidjones.Tap Continue to Create.", preferredStyle: .alert)
        
        // Create the actions
        let NoAction = UIAlertAction(title: "Continue", style: UIAlertActionStyle.default) {
            UIAlertAction in
            
//            var urlSting = "https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_EhG8I0NY8m90bn6cdKPnOJGR7L5VTvQ1&scope=read_write&state=\(self.user_Id!)"
//            if let url = URL(string: urlSting), UIApplication.shared.canOpenURL(url) {
//                UIApplication.shared.openURL(url)
//            }
              KCommonFunctions.PushToContrller(from: self, ToController: .WebView, Data: self.user_Id!)
            
            //self.MakeMeSeller()
            NSLog("OK Pressed")
        }
        let YesAction = UIAlertAction(title: "Abort", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        self.dismiss(animated: true, completion: nil)
        alertController.addAction(NoAction)
        alertController.addAction(YesAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    func MakeMeSeller()  {
        guard let id = UserDefaults.standard.getUserID()else {
            return
        }
        let urlStr = KmakeMeSeller + String(id)
        Preview.sharedManager.GetApiMakeSeller(url: urlStr, Target: self, completionResponse: { (Response) in
            //print(Response)
            if let msg =  Response[Kmessage]
            {
                //                self.arrBooks.remove(at: self.indexDelete!)
                //                self.tblBooksList.reloadData()
                //                self.isDeleted = true
                //                //  self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                //                UserDefaults.standard.setUserType(value: true)
                //                //self.showToast(message: msg as! String)
                //                self.AddMoreAction(AnyObject.self)
                if let showMessage = Response[KshowMessage] as? Int , showMessage == 1{
                    UserDefaults.standard.setUserType(value: true)
                    
                    UserDefaults.standard.setShowMessageType(value: "1")
                    
                    self.userType = UserDefaults.standard.getMessageType()
                    
                    print("Here your userType is \(self.userType)")
                    //self.showToast(message: msg as! String)
                    self.AddProduct()
                    // self.goToAddScreen()
                    
                }
                else {
                    //  print("Here your userType is \(self.userType)")
                    
                    UserDefaults.standard.setShowMessageType(value: "0")
                    
                    self.userType = UserDefaults.standard.getMessageType()
                    
                    print("Here your userType is \(self.userType)")
                    //  self.goToAddScreen()
                    
                    self.AddProduct()
                }
                
            }
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.MakeMeSeller()
                })
            }
            //      else
            //      {
            //        if (self.count == 0 || self.arrList.count == 0)
            //        {
            //          self.arrList.removeAll()
            //          self.tblList.reloadData()
            //          self.lblNoItem.isHidden = false
            //          self.viewNotify.isHidden = false
            //
            //        }
            //
            //      }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    
    //MARK: - Drawer Actions
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if(isFilter == true)
        {
            if let swipeGesture = gesture as? UISwipeGestureRecognizer {
                switch swipeGesture.direction {
                case UISwipeGestureRecognizerDirection.right:
                    print("Swiped right")
                    self.onSlideMenuButtonPressed(btnShowMenu)
                case UISwipeGestureRecognizerDirection.down:
                    print("Swiped down"   )
                case UISwipeGestureRecognizerDirection.left:
                    print("Swiped left")
                case UISwipeGestureRecognizerDirection.up:
                    print("Swiped up")
                default:
                    break
                }
            }
        }
    }
    
    //MARK: - Picker Actions
    
    @objc func DonePicker()
    {
        let row = pickerView.selectedRow(inComponent: 0)
        switch pickerType
        {
        case .RadiusPicker?:
            let radius = arrPicker[row] as! String
            lblRadius.text = radius
            if radius == "World Wide"
            {
                lblRadius_Width.constant = 100
            }
            else
            {
                lblRadius_Width.constant = 45
            }
            
        case .UnitPicker?:
            let unit = arrPicker[row] as! String
            lblUnit.text = unit
            
        case .none:
            print("none")
        }
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    
    @objc func CancelPicker()
    {
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    
    //MARK: - Other Actions
    
    func RemoveFreeItemAction()
    {
        //gurleen
//        print(Index)
//        self.arrList.remove(at: self.Index)
//        self.tblList.reloadData()
//        self.isDeleted = true
        if arrList.count == 0
        {
            tblList.isHidden = true
            lblNoItem.isHidden = false
        }
        
    }

    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    func apiCall(url:String,vc:UIViewController){
        
        
        guard let id = UserDefaults.standard.getUserID()else {
            return
        }
        var chckRtoS:Int?
        let urlStr = KmakeMeSeller + String(id)
        Preview.sharedManager.GetApiMakeSeller(url: urlStr, Target: self, completionResponse: { (Response) in
            print("Make me seller",Response)
            let status = Response["status"] as! Int
            print("status",status)
            //            let message = Response["message"] as? String
            //            print("message",message)
            if let accountalready = Response["account_already"] as? Int{
                self.account_already = accountalready
            }
            print("account_already",self.account_already)
            let data  = Response["result"] as! [String:Any]
            self.user_Id =  data["id"] as? Int
            //let state_Id =  data["state_id"] as? Int
            //            print("data",data)
            if let reToStripe = data["redirect_to_stripe"] as? Int{
                print("reToStripe",reToStripe)
                chckRtoS  =  reToStripe
            }
            let stripe_account_id = data["stripe_account_id"] as? Any
            print("stripe_account_id",stripe_account_id)
            let stpID = "\(stripe_account_id!)"
            print(stpID)
            
            if(status == 200){
                // id not null and account already // 3rd check+
                if(!(stpID.contains("<null>"))){
                    UserDefaults.standard.setUserType(value: true)
                    self.account_already = 1
                    UserDefaults.standard.setStripeExist(value: true)
                    KCommonFunctions.PushToContrller(from: self, ToController: .AddFreeProduct, Data: nil)
                }
                    
                    //account already = 0
                else if(chckRtoS == 1 || self.account_already == 0){
                    print("Web Open")
                    self.MakeSellerShowAlert()
                    
                    //web Open
                    // id null but account already 1
                    //send message of popup link
                }else if(self.account_already == 1){
                    print("Message")
                    self.showAlertMessage(titleStr: KMessage, messageStr:  "We sent a link to your registered email to connect your stripe account with BidJones to receive payments.")
                    
                    
                }
            }
            
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.MakeMeSeller()
                })
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
        
        
        
    }
    func SearchMethod()
    {
        self.isFilter = false
        self.title = "PRODUCTS"
        self.searchBar.placeholder = "Enter title"
        
        viewFilter.isHidden = true
        let urlStr = Kfreesearchitems
        var parm = [String : Any]()
        if let userID = UserDefaults.standard.getUserID()
        {
            parm["seller_id"] = userID
        }
        if let lattitude = Location.sharedInstance.lat
        {
            parm["current_lat"] = lattitude
        }
        if let longitude = Location.sharedInstance.lng
        {
            parm["current_lng"] = longitude
        }
        
        parm["category"] = "8"
        
        
        if(isFilter == true)
        {
            parm["keywords"] = searchBar.text
        }
        else
        {
             parm["keywords"] = searchBar.text
        }
        
        
        print(lblRadius.text ?? "ABC")
        if(lblRadius.text == "World Wide")
        {
            parm["distance"] = ""
        }
        else
        {
            parm["distance"] = lblRadius.text
            
        }
        if lblUnit.text == "Miles"
        {
            parm["distance_type"] = "mi"
        }
        else
        {
            parm["distance_type"] = "km"
        }
        
        parm["offset"] = count
        parm["limit"]  = KPaginationcount
        
        print(parm)
        Search.sharedManager.PostApi(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
            print(Response)
            //      if(self.isFilter == true)
            //      {
            //      self.searchBar.text = ""
            //      }
            self.tblList.isHidden = false
            
            print(self.arrList)
            
            if Response.count>0
            {
                if(self.count == 0)
                {
                    //  self.arrStuff = Response
                    for item in Response
                    {
                        let oldIds = self.arrList.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrList.insert(item, at: self.arrList.count)
                        }
                    }
                    self.tblList.reloadData()
                }
                else
                {
                    for item in Response
                    {
                        let oldIds = self.arrList.map { $0.id } as? [Int]
                        print(oldIds as Any)
                        if !((oldIds?.contains(item.id!))!)
                        {
                            self.arrList.insert(item, at: self.arrList.count)
                        }
                    }
                    self.tblList.reloadData()
                    print(self.arrList)
                }
                print(self.count)
                self.lblNoItem.isHidden = true
            }
            else
            {
                if(self.count != 0)
                {
                    print("abcdefgh")
                    //self.dataLoaded = true
                }
                if (self.count == 0 || self.arrList.count == 0)
                {
                    self.lblNoItem.isHidden = false
                    self.viewNotify.isHidden = false
                }
            }
            self.isLoading = false
        }, completionnilResponse: { (Response) in
            //print(Response)
            //      if(self.isFilter == true)
            //      {
            //        self.searchBar.text = ""
            //      }
            //  self.searchBar.placeholder = "Enter title"
            
            let statusCode = Response[Kstatus] as! Int
            self.arrList.removeAll()
            self.tblList.isHidden = true
            self.lblNoItem.isHidden = false
            
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.SearchMethod()
                })
            }
                //          else if statusCode == 201
                //            {
                //                self.dataLoaded = true
                //            }
            else
            {
                if (self.count == 0 || self.arrList.count == 0)
                {
                    self.arrList.removeAll()
                    self.tblList.reloadData()
                    self.lblNoItem.isHidden = false
                    self.viewNotify.isHidden = false
                    
                }
                
            }
            self.isLoading = false
            
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
            self.isLoading = false
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
            self.isLoading = false
        })
        
    }
    
    //MARK: - IB Actions
    @IBAction func NoAction(_ sender: CustomButton) {
        self.viewNotify.isHidden = true
    }
    
    @IBAction func YesAction(_ sender: Any) {
        self.viewNotify.isHidden = true
        let urlStr = Kfreebuyersavesearchpreference
        var parm = [String : Any]()
        if let userID = UserDefaults.standard.getUserID()
        {
            parm["seller_id"] = userID
        }
        if let Keywords = searchBar.text
        {
            parm["keywords"] = Keywords
        }
        if(lblRadius.text == "World Wide")
        {
            parm["radius"] = "0"
        }
        else
        {
            parm["radius"] = lblRadius.text
            
        }
        if let Unit = lblUnit.text
        {
            if Unit == "Miles"
            {
                parm["distance_type"] = "mi"
            }
            else
            {
                parm["distance_type"] = "km"
            }
        }
        
        
        print(parm)
        Search.sharedManager.PostApiForSearchSave(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
            print(Response)
            print(self.arrList)
            if let msg =  Response[Kmessage] as? String
            {
                // self.showAlertMessage(titleStr: KMessage, messageStr: "Saved succesfully!")
            }
            
        }, completionnilResponse: { (Response) in
            //print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.YesAction(AnyObject.self)
                })
            }
            
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    @IBAction func CancelAction(_ sender: CustomButton) {
        searchBar.placeholder = "Enter title"
        isFilter = false
        viewFilter.isHidden = true
        tblList.isHidden = false
        
    }
    @IBAction func SearchAction(_ sender: CustomButton? = nil)
    {
        AddBackButton()
        arrList.removeAll()
        self.SearchMethod()
    }
    
    @IBAction func UnitAction(_ sender: Any) {
        if(arrUnit.count>0)
        {
            pickerType = PickerType(rawValue: 2)
            self.arrPicker = self.arrUnit
            print(arrUnit)
            print(arrPicker)
            
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(lblUnit.text != "")
            {
                print(arrUnit)
                print(lblUnit.text ?? "")
                let index = arrUnit.index(of: lblUnit.text!)
                print(index ?? "Blank")
                pickerView.selectRow(index!, inComponent: 0, animated: false)
            }
        }
    }
    
    @IBAction func NumAction(_ sender: UIButton) {
        if(
            arrRadius.count>0)
        {
            pickerType = PickerType(rawValue: 1)
            self.arrPicker = self.arrRadius
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(lblRadius.text != "")
            {
                print(arrRadius)
                print(lblRadius.text ?? "")
                let index = arrRadius.index(of: lblRadius.text!)
                print(index ?? "Blank")
                pickerView.selectRow(index!, inComponent: 0, animated: false)
            }
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        //MARK: - UISCrollview delegate
        func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
    
        }
    
        func  scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
        {
        }
        func scrollViewDidScroll(_ scrollView: UIScrollView)
        {
            if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
                //reach bottom
                print(arrList.count)
                print(arrList.count)
                 print(isScrolling)
                  print(isLoading)
                print(dataLoaded)
    
    
                if (arrList.count>0 && arrList.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
                {
                    print("222222222222")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    self.count = self.count+KPaginationcount
                    SearchMethod()
                }
                else if isDeleted!
                {
                    isDeleted = false
                    print("222222222222")
                    isScrolling = true
                    print("reach bottom")
                    isLoading = true
                    SearchMethod()
                }
            }
        }
    
        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
        {
            print("11111111111")
            isScrolling = false
        }
    
}

extension FreeVC : UITableViewDataSource,ProductTableCellDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //print(arrBooks.count)
        return arrList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : ProductTableCell = tableView.dequeueReusableCell(withIdentifier: KproductTableCell) as! ProductTableCell
        cell.delegate = self
        cell.LoadData(dic:arrList[indexPath.row], type: .Book)
        cell.tag = indexPath.row
        cell.selectionStyle = .none
        
        return cell
    }
}
extension FreeVC : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        Index = indexPath.row
        var data = arrList[indexPath.row]
       
        if let id = UserDefaults.standard.getUserID()
        {
            if(id == data.sellerID)
            {
                UserDefaults.standard.setShowEditButton(value: true)
                UserDefaults.standard.setShowSellerInfo(value: false)
                data.myFreeItem = 1
            }
            else
            {
                UserDefaults.standard.setShowEditButton(value: false)
                UserDefaults.standard.setShowSellerInfo(value: true)
                data.FreeSearch = 1
            }
        }
        
        CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
    }
}
extension FreeVC: UISearchBarDelegate
{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        //    print("searchText \(searchText)")
        //    print(isFilter ?? "ABC")
        //     arrList.removeAll()
        //    if(isFilter == false)
        //    {
        //      self.SearchMethod()
        //
        //    }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        searchBar.resignFirstResponder()
        arrList.removeAll()
        print("searchText \(String(describing: searchBar.text))")
        self.SearchMethod()
        
    }
}
extension FreeVC: UIPickerViewDelegate,UIPickerViewDataSource
{
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        print(arrPicker.count)
        return arrPicker.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dic = arrPicker[row]
        print(dic)
        var title = ""
        switch pickerType
        {
        case .RadiusPicker?:
            title = dic as! String
        case .UnitPicker?:
            title = dic as! String
        case .none:
            print("none")
        }
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
    }
}



////
////  FreeVC.swift
////  BidJones
////
////  Created by Kuldeep Singh on 5/14/19.
////  Copyright © 2019 Seasia. All rights reserved.
////
//
//import UIKit
//
//class FreeVC: BaseViewController {
//
//    @IBOutlet var viewFilter: UIView!
//    @IBOutlet var lblUnit: CustomUILabel!
//    @IBOutlet var lblRadius: CustomUILabel!
//    @IBOutlet var searchBar: UISearchBar!
//    private var pickerView = UIPickerView()
//    @IBOutlet var txtFld_forOpeningPickers: UITextField!
//    @IBOutlet var lblRadius_Width: NSLayoutConstraint!
//    @IBOutlet var lblNoItem: UILabel!
//    @IBOutlet var viewNotify: UIView!
//
//    var account_already:Int?
//    let url = "/user/detail"
//    var arrRadius   = [String]()
//    var arrUnit     = [String]()
//    var arrGenre =   [GenreData]()
//    var arrPicker = [Any]()
//    var count = 0
//    var dataLoaded:Bool?
//    var isLoading:Bool?
//    var isFilter:Bool?
//    var userType : String?
//    var user_Id:Int?
//
//    @IBOutlet var tblList: UITableView!
//
//    private lazy var arrList = [ItemListData]()
//
//
//    enum PickerType: Int {
//        case RadiusPicker = 1
//        case UnitPicker = 2
//        init() {
//            self = .RadiusPicker
//        }
//    }
//    private var pickerType: PickerType?
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        lblNoItem.isHidden = true
//
//        // searchBar.placeholder = "Enter title"
//        //isFilter = false
//        lblNoItem.isHidden = true
//        viewFilter.isHidden = true
//        self.title = "SEARCH"
//        drawerType = DrawerType.Free
//        pickerView.dataSource = self
//        searchBar.delegate = self
//        pickerView.delegate = self
//        lblRadius.text = "World Wide"
//        lblRadius_Width.constant = 100
//        lblUnit.text = "Miles"
//        txtFld_forOpeningPickers.inputView = pickerView
//
//        arrRadius.append("World Wide")
//
//        for i in 1...20 {
//            arrRadius.append(String(i*10))
//        }
//
//        for i in 3...15 {
//            arrRadius.append(String(i*100))
//        }
//        arrRadius.append(String(2000
//
//        ))
//
//
//        print(arrRadius)
//
//        arrUnit.append("Miles")
//        arrUnit.append("Kms")
//        print(arrUnit)
//
//        let toolBar = UIToolbar()
//        toolBar.barStyle = UIBarStyle.default
//        toolBar.isTranslucent = true
//        toolBar.tintColor = UIColor(red: 3/255, green: 95/255, blue: 253/255, alpha: 1)
//        toolBar.sizeToFit()
//        let doneButton = UIBarButtonItem(title: KDone, style: UIBarButtonItemStyle.plain, target: self, action: #selector(DonePicker))
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
//        let cancelButton = UIBarButtonItem(title: KCancel, style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPicker))
//        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
//        toolBar.isUserInteractionEnabled = true
//        txtFld_forOpeningPickers.inputAccessoryView = toolBar
//        self.hideKeyboardWhenTappedAround()
//
//        addSlideMenuButton()
//        let notificationButton = SSBadgeButton()
//        let notifiImg:UIImage = UIImage(named: "add_product")!
//
//        notificationButton.frame = CGRect(x: 0, y: 0, width: notifiImg.size.width, height: notifiImg.size.height)
//        notificationButton.setImage(notifiImg.withRenderingMode(.alwaysTemplate), for: .normal)
//        notificationButton.addTarget(self, action:#selector(self.AddProduct), for: .touchUpInside)
//        notificationButton.tintColor = .black
//
//        let radioiImg:UIImage = #imageLiteral(resourceName: "Filter")
//
//        let radioView:UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: radioiImg.size.width, height: radioiImg.size.height))
//        radioView.addTarget(self, action:#selector(self.didTapFilterButton), for: .touchUpInside)
//        radioView.setImage(radioiImg, for: .normal)
//        let radioButton:UIBarButtonItem = UIBarButtonItem(customView: radioView)
//        let NotifyButton = UIBarButtonItem(customView: notificationButton)
//
//        let fixedSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
//        fixedSpace.width = 20.0
//
//        // navigationItem.rightBarButtonItems = [NotifyButton,fixedSpace,radioButton]
//        navigationItem.rightBarButtonItem = NotifyButton
//        // navigationItem.leftBarButtonItems = [NotifyButton,fixedSpace,radioButton]
//
//
//        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
//        swipeRight.direction = UISwipeGestureRecognizerDirection.right
//        self.view.addGestureRecognizer(swipeRight)
//        //self.SearchMethod()
//        viewNotify.isHidden = true
//
//        searchBar.placeholder = "Enter keyword"
//        isFilter = true
//        viewFilter.isHidden = false
//        tblList.isHidden = true
//
//
//    }
//
//    //MARK: - Navigations Actions
//
//    @objc func didTapFilterButton(sender: AnyObject){
//        searchBar.placeholder = "Enter keyword"
//        isFilter = true
//        viewFilter.isHidden = false
//        tblList.isHidden = true
//    }
//
//    func AddBackButton()
//    {
//        var btnShowMenu = UIButton(type: UIButtonType.system)
//
//        btnShowMenu = UIButton(type: UIButtonType.system)
//        btnShowMenu.setImage(UIImage.init(named: "back_arrow"), for: UIControlState())
//        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
//        btnShowMenu.tintColor = .black
//        btnShowMenu.addTarget(self, action: #selector(BackAction), for: UIControlEvents.touchUpInside)
//
//        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
//        self.navigationItem.leftBarButtonItem = customBarItem;
//
//    }
//
//    @objc func BackAction()
//    {
//        if viewNotify.isHidden == true
//        {
//            self.title = "SEARCH"
//            addSlideMenuButton()
//            searchBar.placeholder = "Enter keyword"
//            isFilter = true
//            viewFilter.isHidden = false
//            tblList.isHidden = true
//            lblNoItem.isHidden = true
//        }
//
//    }
//
//    @objc func AddProduct()
//    {
//        if viewNotify.isHidden == true
//        {
//                KCommonFunctions.PushToContrller(from: self, ToController: .AddFreeProduct, Data: nil)
//        }
//    }
//
//    //MARK: - Other function
//    func MakeSellerShowAlert()
//    {
//        let alertController = UIAlertController(title: KMessage, message: "You must have a stripe account linked with bidjones.Tap Continue to Create.", preferredStyle: .alert)
//
//        // Create the actions
//        let NoAction = UIAlertAction(title: "Continue", style: UIAlertActionStyle.default) {
//            UIAlertAction in
//
//            var urlSting = "https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_EhG8I0NY8m90bn6cdKPnOJGR7L5VTvQ1&scope=read_write&state=\(self.user_Id!)"
//            if let url = URL(string: urlSting), UIApplication.shared.canOpenURL(url) {
//                UIApplication.shared.openURL(url)
//            }
//
//            //self.MakeMeSeller()
//            NSLog("OK Pressed")
//        }
//        let YesAction = UIAlertAction(title: "Abort", style: UIAlertActionStyle.default) {
//            UIAlertAction in
//            NSLog("OK Pressed")
//        }
//        self.dismiss(animated: true, completion: nil)
//        alertController.addAction(NoAction)
//        alertController.addAction(YesAction)
//
//        // Present the controller
//        self.present(alertController, animated: true, completion: nil)
//
//    }
//
//    func MakeMeSeller()  {
//        guard let id = UserDefaults.standard.getUserID()else {
//            return
//        }
//        let urlStr = KmakeMeSeller + String(id)
//        Preview.sharedManager.GetApiMakeSeller(url: urlStr, Target: self, completionResponse: { (Response) in
//            //print(Response)
//            if let msg =  Response[Kmessage]
//            {
//                //                self.arrBooks.remove(at: self.indexDelete!)
//                //                self.tblBooksList.reloadData()
//                //                self.isDeleted = true
//                //                //  self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
//                //                UserDefaults.standard.setUserType(value: true)
//                //                //self.showToast(message: msg as! String)
//                //                self.AddMoreAction(AnyObject.self)
//                if let showMessage = Response[KshowMessage] as? Int , showMessage == 1{
//                    UserDefaults.standard.setUserType(value: true)
//
//                    UserDefaults.standard.setShowMessageType(value: "1")
//
//                    self.userType = UserDefaults.standard.getMessageType()
//
//                    print("Here your userType is \(self.userType)")
//                    //self.showToast(message: msg as! String)
//                    self.AddProduct()
//                    // self.goToAddScreen()
//
//                }
//                else {
//                    //  print("Here your userType is \(self.userType)")
//
//                    UserDefaults.standard.setShowMessageType(value: "0")
//
//                    self.userType = UserDefaults.standard.getMessageType()
//
//                    print("Here your userType is \(self.userType)")
//                    //  self.goToAddScreen()
//
//                    self.AddProduct()
//                }
//
//            }
//
//        }, completionnilResponse: { (Response) in
//            //print(Response)
//            let statusCode = Response[Kstatus] as! Int
//            if statusCode == 500
//            {
//                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
//                    //print(resonse)
//                //    self.MakeMeSeller()
//                })
//            }
//            //      else
//            //      {
//            //        if (self.count == 0 || self.arrList.count == 0)
//            //        {
//            //          self.arrList.removeAll()
//            //          self.tblList.reloadData()
//            //          self.lblNoItem.isHidden = false
//            //          self.viewNotify.isHidden = false
//            //
//            //        }
//            //
//            //      }
//        }, completionError: { (error) in
//            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
//        },networkError: {(error) in
//            self.showAlertMessage(titleStr: KMessage, messageStr: error)
//        })
//    }
//
//
//    //MARK: - Drawer Actions
//
//    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
//
//        if(isFilter == true)
//        {
//            if let swipeGesture = gesture as? UISwipeGestureRecognizer {
//                switch swipeGesture.direction {
//                case UISwipeGestureRecognizerDirection.right:
//                    print("Swiped right")
//                    self.onSlideMenuButtonPressed(btnShowMenu)
//                case UISwipeGestureRecognizerDirection.down:
//                    print("Swiped down"   )
//                case UISwipeGestureRecognizerDirection.left:
//                    print("Swiped left")
//                case UISwipeGestureRecognizerDirection.up:
//                    print("Swiped up")
//                default:
//                    break
//                }
//            }
//        }
//    }
//
//    //MARK: - Picker Actions
//
//    @objc func DonePicker()
//    {
//        let row = pickerView.selectedRow(inComponent: 0)
//        switch pickerType
//        {
//        case .RadiusPicker?:
//            let radius = arrPicker[row] as! String
//            lblRadius.text = radius
//            if radius == "World Wide"
//            {
//                lblRadius_Width.constant = 100
//            }
//            else
//            {
//                lblRadius_Width.constant = 45
//            }
//
//        case .UnitPicker?:
//            let unit = arrPicker[row] as! String
//            lblUnit.text = unit
//
//        case .none:
//            print("none")
//        }
//        txtFld_forOpeningPickers.resignFirstResponder()
//    }
//
//    @objc func CancelPicker()
//    {
//        txtFld_forOpeningPickers.resignFirstResponder()
//    }
//
//    //MARK: - Other Actions
//    func isKeyPresentInUserDefaults(key: String) -> Bool {
//        return UserDefaults.standard.object(forKey: key) != nil
//    }
//    func apiCall(url:String,vc:UIViewController){
//
//
//
//
//
//
//
//        guard let id = UserDefaults.standard.getUserID()else {
//            return
//        }
//        var chckRtoS:Int?
//        let urlStr = KmakeMeSeller + String(id)
//        Preview.sharedManager.GetApiMakeSeller(url: urlStr, Target: self, completionResponse: { (Response) in
//            print("Make me seller",Response)
//            let status = Response["status"] as! Int
//            print("status",status)
//            //            let message = Response["message"] as? String
//            //            print("message",message)
//            if let accountalready = Response["account_already"] as? Int{
//                self.account_already = accountalready
//            }
//            print("account_already",self.account_already)
//            let data  = Response["result"] as! [String:Any]
//            self.user_Id =  data["id"] as? Int
//            //let state_Id =  data["state_id"] as? Int
//            //            print("data",data)
//            if let reToStripe = data["redirect_to_stripe"] as? Int{
//                print("reToStripe",reToStripe)
//                chckRtoS  =  reToStripe
//            }
//            let stripe_account_id = data["stripe_account_id"] as? Any
//            print("stripe_account_id",stripe_account_id)
//            let stpID = "\(stripe_account_id!)"
//            print(stpID)
//
//            if(status == 200){
//                // id not null and account already // 3rd check+
//                if(!(stpID.contains("<null>"))){
//                    UserDefaults.standard.setUserType(value: true)
//                    self.account_already = 1
//                    UserDefaults.standard.setStripeExist(value: true)
//                    KCommonFunctions.PushToContrller(from: self, ToController: .AddTradeProduct, Data: nil)
//                }
//
//                    //account already = 0
//                else if(chckRtoS == 1 || self.account_already == 0){
//                    print("Web Open")
//                    self.MakeSellerShowAlert()
//
//
//
//                    //web Open
//                    // id null but account already 1
//                    //send message of popup link
//                }else if(self.account_already == 1){
//                    print("Message")
//                    self.showAlertMessage(titleStr: KMessage, messageStr:  "We sent a link to your registered email to connect your stripe account with BidJones to receive payments.")
//
//
//                }
//            }
//
//
//        }, completionnilResponse: { (Response) in
//            //print(Response)
//            let statusCode = Response[Kstatus] as! Int
//            if statusCode == 500
//            {
//                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
//                    //print(resonse)
//                  //  self.MakeMeSeller()
//                })
//            }
//        }, completionError: { (error) in
//            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
//        },networkError: {(error) in
//            self.showAlertMessage(titleStr: KMessage, messageStr: error)
//        })
//
//
//
//    }
//    func SearchMethod()
//    {
//        self.isFilter = false
//        self.title = "PRODUCTS"
//        self.searchBar.placeholder = "Enter title"
//
//        viewFilter.isHidden = true
//        let urlStr = Kfreesearchitems
//        var parm = [String : Any]()
//        if let userID = UserDefaults.standard.getUserID()
//        {
//            parm["seller_id"] = userID
//        }
//        if let lattitude = Location.sharedInstance.lat
//        {
//            parm["current_lat"] = lattitude
//        }
//        if let longitude = Location.sharedInstance.lng
//        {
//            parm["current_lng"] = longitude
//        }
//
//        parm["category"] = "8"
//
//
//        if(isFilter == true)
//        {
//            parm["keywords"] = searchBar.text
//        }
//        else
//        {
//            parm["search"] = searchBar.text
//        }
//
//
//        print(lblRadius.text ?? "ABC")
//        if(lblRadius.text == "World Wide")
//        {
//            parm["distance"] = ""
//        }
//        else
//        {
//            parm["distance"] = lblRadius.text
//
//        }
//        if lblUnit.text == "Miles"
//        {
//            parm["distance_type"] = "mi"
//        }
//        else
//        {
//            parm["distance_type"] = "km"
//        }
//
//        parm["offset"] = count
//        parm["limit"]  = KPaginationcount
//
//        print(parm)
//        Search.sharedManager.PostApi(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
//            print(Response)
//            //      if(self.isFilter == true)
//            //      {
//            //      self.searchBar.text = ""
//            //      }
//            self.tblList.isHidden = false
//
//            print(self.arrList)
//
//            if Response.count>0
//            {
//                if(self.count == 0)
//                {
//                    //  self.arrStuff = Response
//                    for item in Response
//                    {
//                        let oldIds = self.arrList.map { $0.id } as? [Int]
//                        print(oldIds as Any)
//                        if !((oldIds?.contains(item.id!))!)
//                        {
//                            self.arrList.insert(item, at: self.arrList.count)
//                        }
//                    }
//                    self.tblList.reloadData()
//                }
//                else
//                {
//                    for item in Response
//                    {
//                        let oldIds = self.arrList.map { $0.id } as? [Int]
//                        print(oldIds as Any)
//                        if !((oldIds?.contains(item.id!))!)
//                        {
//                            self.arrList.insert(item, at: self.arrList.count)
//                        }
//                    }
//                    self.tblList.reloadData()
//                    print(self.arrList)
//                }
//                print(self.count)
//                self.lblNoItem.isHidden = true
//            }
//            else
//            {
//                if(self.count != 0)
//                {
//                    print("abcdefgh")
//                    self.dataLoaded = true
//                }
//                if (self.count == 0 || self.arrList.count == 0)
//                {
//                    self.lblNoItem.isHidden = false
//                    self.viewNotify.isHidden = false
//                }
//            }
//            self.isLoading = false
//        }, completionnilResponse: { (Response) in
//            //print(Response)
//            //      if(self.isFilter == true)
//            //      {
//            //        self.searchBar.text = ""
//            //      }
//            //  self.searchBar.placeholder = "Enter title"
//
//            let statusCode = Response[Kstatus] as! Int
//            self.arrList.removeAll()
//            self.tblList.isHidden = true
//            self.lblNoItem.isHidden = false
//
//            if statusCode == 500
//            {
//                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
//                    //print(resonse)
//                    self.SearchMethod()
//                })
//            }
//                //          else if statusCode == 201
//                //            {
//                //                self.dataLoaded = true
//                //            }
//            else
//            {
//                if (self.count == 0 || self.arrList.count == 0)
//                {
//                    self.arrList.removeAll()
//                    self.tblList.reloadData()
//                    self.lblNoItem.isHidden = false
//                    self.viewNotify.isHidden = false
//
//                }
//
//            }
//            self.isLoading = false
//
//        }, completionError: { (error) in
//            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
//            self.isLoading = false
//        },networkError: {(error) in
//            self.showAlertMessage(titleStr: KMessage, messageStr: error)
//            self.isLoading = false
//        })
//
//    }
//
//    //MARK: - IB Actions
//    @IBAction func NoAction(_ sender: CustomButton) {
//        self.viewNotify.isHidden = true
//    }
//
//    @IBAction func YesAction(_ sender: Any) {
//        self.viewNotify.isHidden = true
//        let urlStr = Kbuyersavesearchpreference
//        var parm = [String : Any]()
//        if let userID = UserDefaults.standard.getUserID()
//        {
//            parm["seller_id"] = userID
//        }
//        if let Keywords = searchBar.text
//        {
//            parm["keywords"] = Keywords
//        }
//        if(lblRadius.text == "World Wide")
//        {
//            parm["radius"] = "0"
//        }
//        else
//        {
//            parm["radius"] = lblRadius.text
//
//        }
//        if let Unit = lblUnit.text
//        {
//            if Unit == "Miles"
//            {
//                parm["distance_type"] = "mi"
//            }
//            else
//            {
//                parm["distance_type"] = "km"
//            }
//        }
//
//
//        print(parm)
//        Search.sharedManager.PostApiForSearchSave(url: urlStr, parameter: parm,Target: self, completionResponse: { (Response) in
//            print(Response)
//            print(self.arrList)
//            if let msg =  Response[Kmessage] as? String
//            {
//                // self.showAlertMessage(titleStr: KMessage, messageStr: "Saved succesfully!")
//            }
//
//        }, completionnilResponse: { (Response) in
//            //print(Response)
//            let statusCode = Response[Kstatus] as! Int
//            if statusCode == 500
//            {
//                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
//                    //print(resonse)
//                    self.YesAction(AnyObject.self)
//                })
//            }
//
//        }, completionError: { (error) in
//            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
//        },networkError: {(error) in
//            self.showAlertMessage(titleStr: KMessage, messageStr: error)
//        })
//    }
//    @IBAction func CancelAction(_ sender: CustomButton) {
//        searchBar.placeholder = "Enter title"
//        isFilter = false
//        viewFilter.isHidden = true
//        tblList.isHidden = false
//
//    }
//    @IBAction func SearchAction(_ sender: CustomButton? = nil)
//    {
//        AddBackButton()
//        arrList.removeAll()
//        self.SearchMethod()
//    }
//
//    @IBAction func UnitAction(_ sender: Any) {
//        if(arrUnit.count>0)
//        {
//            pickerType = PickerType(rawValue: 2)
//            self.arrPicker = self.arrUnit
//            print(arrUnit)
//            print(arrPicker)
//
//            self.pickerView.reloadAllComponents()
//            pickerView.selectRow(0, inComponent: 0, animated: false)
//            txtFld_forOpeningPickers.becomeFirstResponder()
//            if(lblUnit.text != "")
//            {
//                print(arrUnit)
//                print(lblUnit.text ?? "")
//                let index = arrUnit.index(of: lblUnit.text!)
//                print(index ?? "Blank")
//                pickerView.selectRow(index!, inComponent: 0, animated: false)
//            }
//        }
//    }
//
//    @IBAction func NumAction(_ sender: UIButton) {
//        if(
//            arrRadius.count>0)
//        {
//            pickerType = PickerType(rawValue: 1)
//            self.arrPicker = self.arrRadius
//            self.pickerView.reloadAllComponents()
//            pickerView.selectRow(0, inComponent: 0, animated: false)
//            txtFld_forOpeningPickers.becomeFirstResponder()
//            if(lblRadius.text != "")
//            {
//                print(arrRadius)
//                print(lblRadius.text ?? "")
//                let index = arrRadius.index(of: lblRadius.text!)
//                print(index ?? "Blank")
//                pickerView.selectRow(index!, inComponent: 0, animated: false)
//            }
//        }
//    }
//    override func didReceiveMemoryWarning()
//    {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    //MARK: - UISCrollview delegate
//    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
//
//    }
//
//    func  scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView)
//    {
//    }
//    func scrollViewDidScroll(_ scrollView: UIScrollView)
//    {
//        if (scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)) {
//            //reach bottom
//            print(arrList.count)
//            print(arrList.count)
//            // print(isScrolling)
//            //  print(isLoading)
//            //print(dataLoaded)
//
//
//            if (arrList.count>0 && arrList.count % KPaginationcount == 0 && isScrolling == false && isLoading == false && dataLoaded == false)
//            {
//                print("222222222222")
//                isScrolling = true
//                print("reach bottom")
//                isLoading = true
//                self.count = self.count+KPaginationcount
//                GetSearchData()
//            }
//            else if isDeleted!
//            {
//                isDeleted = false
//                print("222222222222")
//                isScrolling = true
//                print("reach bottom")
//                isLoading = true
//                GetSearchData()
//            }
//        }
//    }
//
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
//    {
//        print("11111111111")
//        isScrolling = false
//    }
//}
//
//extension FreeVC : UITableViewDataSource,ProductTableCellDelegate
//{
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//
//        return 100
//    }
//    func numberOfSections(in tableView: UITableView) -> Int
//    {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
//    {
//        //print(arrBooks.count)
//        return arrList.count
//    }
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell : ProductTableCell = tableView.dequeueReusableCell(withIdentifier: KproductTableCell) as! ProductTableCell
//        cell.delegate = self
//        cell.LoadData(dic:arrList[indexPath.row], type: .Book)
//        cell.tag = indexPath.row
//        cell.selectionStyle = .none
//
//        return cell
//    }
//}
//extension FreeVC : UITableViewDelegate {
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        UserDefaults.standard.setShowEditButton(value: false)
//        UserDefaults.standard.setShowSellerInfo(value: true)
//        var data = arrList[indexPath.row]
//        // Index = indexPath.row
//        // let itemId = data.id
//        data.isTradeItem = 1
//        CommonFunctions.sharedInstance.PushToContrller(from: self, ToController: .Detail, Data: data)
//    }
//}
//extension FreeVC: UISearchBarDelegate
//{
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
//    {
//        //    print("searchText \(searchText)")
//        //    print(isFilter ?? "ABC")
//        //     arrList.removeAll()
//        //    if(isFilter == false)
//        //    {
//        //      self.SearchMethod()
//        //
//        //    }
//    }
//
//    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
//    {
//        searchBar.resignFirstResponder()
//        arrList.removeAll()
//        print("searchText \(String(describing: searchBar.text))")
//        self.SearchMethod()
//
//    }
//}
//extension FreeVC: UIPickerViewDelegate,UIPickerViewDataSource
//{
//
//
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        print(arrPicker.count)
//        return arrPicker.count;
//    }
//
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        let dic = arrPicker[row]
//        print(dic)
//        var title = ""
//        switch pickerType
//        {
//        case .RadiusPicker?:
//            title = dic as! String
//        case .UnitPicker?:
//            title = dic as! String
//        case .none:
//            print("none")
//        }
//        return title
//    }
//
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
//    {
//    }
//}

