//
//  InVoiceVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 6/22/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class InVoiceVC:  UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var constraint_btn_com: NSLayoutConstraint!
    @IBOutlet weak var constraintcom_below: NSLayoutConstraint!
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lbl_Total: UILabel!
    //@IBOutlet weak var lbl_SellerCode: UILabel!
    @IBOutlet weak var lbl_Item: UILabel!
    @IBOutlet weak var view_for_Detail: UIView!
    @IBOutlet var lblProductName: UILabel!
   // @IBOutlet var lblproductName1: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblPriceWidTax: UILabel!
    @IBOutlet var lblTotal: UILabel!
    
   // @IBOutlet var txtFldSellerCode: CustomTextField!
    //MARK: - Variable
    var productDetail:ItemListData?

    override func viewDidLoad() {
        self.hideKeyboardWhenTappedAround()
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
        if let title  = productDetail?.title
        {
            lblProductName.text = title
            //lblproductName1.text = title
        }
        if let bidAmount  = productDetail?.bidAmount
        {
            lblPrice.text = "$" + "\(String(format:"%.2f", bidAmount))"
            let taxAmount = (Double(bidAmount)!*3)/100
            let taxAmountRounded = taxAmount.rounded(toPlaces: 2)
            let totalAmount = taxAmountRounded + Double(bidAmount)!
            lblPriceWidTax.text = "$"+"\(String(format:"%.2f", totalAmount))"
            lblTotal.text = "$" + "\(String(format:"%.2f", totalAmount))"
        }
        self.hideKeyboardWhenTappedAround()

    }
    
    @IBAction func ProcessPaymentAction(_ sender: Any) {
        KCommonFunctions.PushToContrller(from: self, ToController: .Payment, Data: productDetail)
    }
    //MARK: - Other functions
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TextField Delegate
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        //txtFldSellerCode.resignFirstResponder()
         showAlertMessage(titleStr: KMessage, messageStr: KComingSoon)
        return false
    }
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        //txtFldSellerCode.resignFirstResponder()
       // self.view.endEditing(true)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
//        if (string == " ") && (textField.text?.count)! == 0
//        {
//            return false
//        }
//        if textField == txtFldSellerCode
//        {
//            return textField.RestrictMaxCharacter(maxCount: 6, range: range, string: string)
//        }
//       
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    
    
}

