//
//  CheckOutVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 6/21/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class CheckOutVC: UIViewController {
   
    //MARK: - Outlets
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet weak var view_for_Price: UIView!
    @IBOutlet weak var view_for_Chat: UIView!
    @IBOutlet weak var view_for_SeePurchase: UIView!
    var productDetail:ItemListData?
     override func viewDidLoad()
        {
        super.viewDidLoad()
            if let bidAmount = productDetail?.bidAmount
            {
            print(productDetail?.price as Any)
            lblPrice.text = "$" + "\(bidAmount)"
            }
            print("your product detail and own :\(productDetail)")
        }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - IBAction
    @IBAction func ChatAction(_ sender: Any) {
        //print(sender.tag)
       // print(arrList[(sender as AnyObject).tag])
        if let itemID = productDetail?.id
        {
            if let sellerID = productDetail?.sellerID
            {
                print(sellerID)
                print(itemID)
                
                
                guard let userID = UserDefaults.standard.getUserID() else
                {
                    return
                }
                
                let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ChatRoomVC") as! ChatRoomVC
                vc.anotherUserID = "\(sellerID)"
                vc.login_userID = "\(userID)"
                vc.itemID = "\(itemID)"
                var username = ""
                var userImage = ""
                if let name = productDetail?.username {
                    username = name
                }
                if let userimg = productDetail?.userImage {
                    userImage = userimg
                }
                let talkInformation = ["index" : 1 ,"userId" : userID, "username" : username, "userimage":  userImage ] as [String : Any]
                
                let data = TalkData.init(dict: talkInformation)
                
                vc.talkData = data
                
            
                navigationController?.pushViewController(vc, animated: true)
                
                
            }
        }
    }
    @IBAction func CheckOutAction(_ sender: Any)
    {
        KCommonFunctions.PushToContrller(from: self, ToController: .Invoice, Data: productDetail)
    }
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - Other functions
    func shadow_for_view(view: UIView) {
        
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowRadius = 8.0
        view.layer.shadowOpacity = 0.2
        view.layer.shadowOffset = CGSize.init(width: 0.5, height: 0.5)
        view.clipsToBounds = false
    
}
}
