//
//  RatingListTableCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 7/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import Cosmos

class RatingListTableCell: UITableViewCell {
    @IBOutlet var name: UILabel!
    @IBOutlet var lblComment: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var viewRating: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func LoadData(Data dic:RatingData)
    {
        print(dic)
       name.text = dic.buyerName
        if let comment = dic.comment
        {
            lblComment.text = comment
        }
        lblDate.text = dic.date
        viewRating.rating = Double(dic.rating!)
        print(dic.rating!)
        print(Double(dic.rating!))
        //viewRating.rating = 3
    }

}
