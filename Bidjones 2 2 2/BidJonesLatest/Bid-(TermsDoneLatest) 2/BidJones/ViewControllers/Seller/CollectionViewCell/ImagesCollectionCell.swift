//
//  ImagesCollectionCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/4/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import Kingfisher


protocol ImagesCollectionCellDelegate: class{
  func DeleteImage(_ sender: Any)
}
class ImagesCollectionCell: UICollectionViewCell {
    @IBOutlet var ImgView: UIImageView!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    weak var delegate: ImagesCollectionCellDelegate?

    func LoadData(Data : [ImagesData], index:IndexPath)
    {
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        btnDelete.tag = index.row
        if let image  = Data[index.row].image
        {
        btnDelete.isHidden = false
        ImgView.contentMode = .scaleAspectFill
        ImgView.clipsToBounds = true
        ImgView.image = image
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        }
        else if let ImgUrl = Data[index.row].imgUrl
        {
            btnDelete.isHidden = false
            ImgView.contentMode = .scaleToFill
            ImgView.clipsToBounds = true
            
            ImgView.kf.setImage(with: ImgUrl as Resource, placeholder:  UIImage.init(named:"image"), options: .none, progressBlock: { (receivedSize, totalSize) -> () in
                print("Download Progress: \(receivedSize)/\(totalSize)")
            }, completionHandler: { (image, error,type, imageURL) in
                print("Downloaded and set!")
                self.ImgView.contentMode = .scaleAspectFill
                self.ImgView.clipsToBounds = true
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
            })
        }
        else
        {
            btnDelete.isHidden = true
            ImgView.contentMode = .scaleToFill
            ImgView.image = UIImage.init(named:"image")
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
    }
    func LoadBlankData(index:IndexPath)
    {
        btnDelete.isHidden = true
        ImgView.contentMode = .scaleToFill
        ImgView.image = UIImage.init(named:"image")
        btnDelete.tag = index.row
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
    }
    @IBAction func DeleteAction(_ sender: Any)
    {
        delegate?.DeleteImage(sender)
    }
    
}
