//
//  MusicCollectionCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/9/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

 protocol MusicCollectionCellDelegate: class{
   // func ChangeMusicName(_ sender: Any)
    func DeleteMusic(_ sender: Any)
}

class MusicCollectionCell: UICollectionViewCell {
    @IBOutlet var ImgView: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnName: UIButton!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        let longPressGesture = UILongPressGestureRecognizer(target: self,
//                                                action: #selector(gestureAction))
//        
//        addGestureRecognizer(longPressGesture)
    }
    
   

    weak var delegate: MusicCollectionCellDelegate?

    
    func LoadData(Data : [AudioData], index:IndexPath)
    {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        btnDelete.isHidden = false
        btnName.tag = index.row
        btnDelete.tag = index.row
        print(index.row)
        lblTitle.text = Data[index.row].title
        ImgView.contentMode = .scaleAspectFill
        ImgView.clipsToBounds = true
        ImgView.image = #imageLiteral(resourceName: "audio")
    }
    func LoadBlankData(index:IndexPath)
    {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        btnDelete.isHidden = true
        ImgView.contentMode = .scaleToFill
        ImgView.clipsToBounds = true

        btnName.tag = index.row
        btnDelete.tag = index.row
        ImgView.image = UIImage.init(named:"upload")
        lblTitle.text = ""
    }
    
    @objc func gestureAction() {
        //print((sender as AnyObject).tag)
        //delegate?.ChangeMusicName(sender)
    }
    
    @IBAction func DeleteAction(_ sender: Any)
    {
        delegate?.DeleteMusic(sender)
    }
}
