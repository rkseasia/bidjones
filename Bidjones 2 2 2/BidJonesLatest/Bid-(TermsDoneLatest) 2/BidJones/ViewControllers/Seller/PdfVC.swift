//
//  PdfVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/23/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import QuickLook

class PdfVC: UIViewController , QLPreviewControllerDataSource , QLPreviewControllerDelegate {

    var pdfUrl : URL?
    let preview = QLPreviewController()
    let tempURL = FileManager.default.temporaryDirectory.appendingPathComponent("quicklook.pdf")
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        preview.delegate = self
        preview.dataSource = self
        preview.currentPreviewItemIndex = 0
        
        
        URLSession.shared.dataTask(with: pdfUrl!) { data, response, error in
            guard let data = data, error == nil else {
                //  in case of failure to download your data you need to present alert to the user and update the UI from the main thread
                DispatchQueue.main.async
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let alert = UIAlertController(title: "Alert", message: error?.localizedDescription ?? "Failed to download the pdf!!!", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default))
  
                        self.present(alert, animated: false)
                }
                return
            }
        
            do {
                try data.write(to: self.tempURL, options: .atomic)
              
                DispatchQueue.main.async
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        if self.tempURL.typeIdentifier == "com.adobe.pdf"
                        {
                            DispatchQueue.global(qos: .background).async {
                                
                                // ...
                                // heavy loop codes here
                                // ...
                                
                                DispatchQueue.main.async {
                                  //  self.activityIndicator.stopAnimating()
                                }
                            }
                            
                            
                            self.navigationController?.pushViewController(self.preview, animated: false)
                        }
                        else
                        {
                            print("the data downloaded it is not a valid pdf file")
                        }
                }
            } catch {
                print(error)
                return
            }
            
            }.resume()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return tempURL as QLPreviewItem
    }
    func previewControllerDidDismiss(_ controller: QLPreviewController) {
        popBack(2)
    }

//
//    private func createActivityIndicator() -> UIActivityIndicatorView {
//        let activityIndicator = UIActivityIndicatorView()
//        activityIndicator.hidesWhenStopped = true
//        activityIndicator.color = UIColor.black
//        return activityIndicator
//    }
//
//    private func showSpinning() {
//        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
//        self.view.addSubview(activityIndicator)
//        activityIndicator.center = self.view.center
//        activityIndicator.startAnimating()
//    }

    
    
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                print(viewControllers[viewControllers.count - nb]);
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
   
}

