//
//  AddSellService.swift
//  BidJones
//
//  Created by Rakesh Kumar on 4/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
import MobileCoreServices
import MediaPlayer
import GoogleMaps
import GooglePlaces
import GooglePlacePicker

class AddSellServiceVC: UIViewController,UINavigationControllerDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,ImagesCollectionCellDelegate,VideosCollectionCellDelegate,LocationProtocol {
   
    
    
    @IBOutlet var btnYes: UIButton!
    @IBOutlet var btnNo: UIButton!
    @IBOutlet var ViewDelete: UIView!
    @IBOutlet var btnProceed: CustomButton!
    @IBOutlet weak var btnOpenLink: UIButton!



    //MARK: - TextField Outlets
    @IBOutlet var txtFld_Location: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_KeySearchWord: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_Title: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_TCService: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_MinBidAmount: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_BidType: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_Descrption: CustomUITextFieldWithPadding!
    @IBOutlet var txtFld_forOpeningPickers: UITextField!
    @IBOutlet var txtFld_Genre: CustomUITextFieldWithPadding!

    //MARK: - UICollectionView Outlets
    @IBOutlet var CollectionView_Images: UICollectionView!
    @IBOutlet var CollectionView_Video: UICollectionView!
    //MARK: - UISCrollView Outlets
    @IBOutlet var scrollView_main: UIScrollView!
    //MARK: - UIPickerView Variable
    private var pickerView = UIPickerView()
    //MARK: - Int Variable
    private var count = 0
    private var totalCount = 0
    private var DeletedIndex = 0

    private var strBidTypeID:String?
    private var strGenreID:String?
    private var maxNumImages = 0
    //MARK: - Bool Variable
    var isGenrePicker = true
    var is_escrow = Bool()
    //MARK: - array Variable
    var arrImages = [ImagesData]()
    var arrVideos = [VideoData]()
    var arrBidType = [BidTypeData]()
    var arrGenre =   [GenreData]()
    var arrImagesUrl = [String]()
    var arrVideosUrl  = [String]()
    var arrRemoveMedia   = [String]()
    var arrPicker = [Any]()
    //MARK: - UIImagePickerController
    private var imagePicker =  UIImagePickerController()
    //MARK: - AudioCollection_heightConstraints
    @IBOutlet var ImagesCollection_heightConstarints: NSLayoutConstraint!
    //MARK: - Enum Variable
    
    enum PickerType: Int {
        case GenrePicker = 1
        case BidTypePicker = 2
        init() {
            self = .GenrePicker
        }
    }
    private var pickerType: PickerType?
    var productDetail:ProductData?
     var deleteItemType:AWSDataType?
    var Filename = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        GetGener()
        is_escrow = true
        ViewDelete.isHidden = true
      //  AddProuductData.sharedInstance?.Reinitilize()
        //  AudioCollection_heightConstraints.constant = 0
        //self.automaticallyAdjustsScrollViewInsets = false
        if #available(iOS 11.0, *) {
            scrollView_main.contentInsetAdjustmentBehavior = .automatic
        } else {
            // Fallback on earlier versions
        }
        pickerView.delegate = self
        pickerView.dataSource = self
        txtFld_forOpeningPickers.inputView = pickerView
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 3/255, green: 95/255, blue: 253/255, alpha: 1)
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(title: KDone, style: UIBarButtonItemStyle.plain, target: self, action: #selector(DonePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: KCancel, style: UIBarButtonItemStyle.plain, target: self, action: #selector(CancelPicker))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFld_forOpeningPickers.inputAccessoryView = toolBar
        txtFld_MinBidAmount.addDoneButtonToKeyboard(target:self,myAction:  #selector(self.doneButtonAction), Title: KNext)
        // Do any additional setup after loading the view.
        AddProuductData.sharedInstance?.Reinitilize()
       LoadDataForEdit()
        //Set Attributed text for btn open link
        KCommonFunctions.setAttributedStringInBtnOpenLink(button: btnOpenLink)

    }
    override func viewWillAppear(_ animated: Bool) {
    }
    override func viewDidAppear(_ animated: Bool) {
        
        //CollectionView_Video.reloadData()
    }
    //MARK: - Other Functions

    //Set data for web page view controller
    func setDataForWebPageViewController(){
        var dict = [String: Any]()
        dict["url"] = "https://app.bernstein.io/bidjones"
        dict["title"] = "Secure Trade"
        KCommonFunctions.PushToContrller(from: self, ToController: .TermsNew, Data: dict)
    }
    
    func ShowAlert()
    {
        ViewDelete.isHidden = false
    }
    func LoadDataForEdit()
    {
       

        if let Data = productDetail
        {
            self.title = "EDIT SELL SERVICE"
            btnProceed.setTitle("UPDATE", for: .normal)
            print(Data)
            if let ItemID = Data.itemId
            {
              AddProuductData.sharedInstance?.itemID = ItemID
            }
            if let title =  Data.title
            {
                txtFld_Title.text = title
               // AddProuductData.sharedInstance?.title = title
            }
            
            if let title =  Data.title
            {
                txtFld_Title.text = title
                // AddProuductData.sharedInstance?.title = title
            }
            if let description = Data.description
            {
                txtFld_Descrption.text = description
                //AddProuductData.sharedInstance?.description = description
            }
//            if let id = UserDefaults.standard.getUserID()
//            {
//                AddProuductData.sharedInstance?.id = id
//            }
            if let bidTYpe = Data.minBidType
            {
                txtFld_BidType.text = bidTYpe
                //AddProuductData.sharedInstance?.minBidType = bidTYpe
            }
            if let bidTypeID = Data.minBidTypeId
            {
                AddProuductData.sharedInstance?.minBidTypeId = Int(bidTypeID)
            }
            if let serviceType = Data.serviceType
            {
                txtFld_Genre.text = serviceType
                //AddProuductData.sharedInstance?.serviceType = serviceType
            }
            if let GenreID = Data.genreId
            {
                AddProuductData.sharedInstance?.genreId = Int(GenreID)
            }
            if let minBid = Data.minBidAmount
            {
                txtFld_MinBidAmount.text = "\(minBid)"
//                print(minBid)
//                print(Float(minBid) as Any)
//                print(Double(minBid) as Any)
//                AddProuductData.sharedInstance?.minBidAmount = Double(minBid)
//                print(Double(minBid))
//                print(AddProuductData.sharedInstance?.minBidAmount)
            }
            if let toc = Data.TC
            {
                txtFld_TCService.text = toc
                AddProuductData.sharedInstance?.TC = toc
            }
            if let ArrImages = Data.arrImages
            {
                arrImages = ArrImages
                CollectionView_Images.reloadData()
                AddProuductData.sharedInstance?.arrImages = ArrImages
            }
            if let ArrVideos = productDetail?.arrVideos
            {
                arrVideos = ArrVideos
                CollectionView_Video.reloadData()
                AddProuductData.sharedInstance?.arrVideos = ArrVideos
            }
            if let keywords = Data.keySearchWord
            {
                txtFld_KeySearchWord.text = keywords
                //AddProuductData.sharedInstance?.keySearchWord = keywords
            }
            
            if let location = Data.location
            {
                txtFld_Location.text = location
               // AddProuductData.sharedInstance?.location = location
            }
//            if let minBidType = txtFld_BidType.text
//            {
//                txtFld_BidType.text = minBidType
//                AddProuductData.sharedInstance?.minBidType = minBidType
//            }
            
            if let isEscrow = Data.is_escrow
            {
                //Gagan
            AddProuductData.sharedInstance?.is_escrow = isEscrow
               // AddProuductData.sharedInstance?.is_escrow = false
                if isEscrow == true
                {
                    is_escrow = true
                    if let image  = UIImage(named: Kselected)
                    {
                        btnYes.setImage(image, for: .normal)
                    }
                    if let image  = UIImage(named: Kunselected)
                    {
                        btnNo.setImage(image, for: .normal)
                    }
                }
                else
                {
                    is_escrow = false
                    if let image  = UIImage(named: Kselected)
                    {
                        btnNo.setImage(image, for: .normal)
                    }
                    if let image  = UIImage(named: Kunselected)
                    {
                        btnYes.setImage(image, for: .normal)
                    }
                }
                // AddProuductData.sharedInstance?.location = location
            }
//            if let itemType = Data.itemType
//            {
//                AddProuductData.sharedInstance?.itemType = itemType
//                // AddProuductData.sharedInstance?.location = location
//            }
//            AddProuductData.sharedInstance?.is_escrow = is_escrow
            AddProuductData.sharedInstance?.itemType = KServicesType
            if let lat = Data.lat
            {
               AddProuductData.sharedInstance?.lat = lat
            }
            if let lng = Data.lng
            {
                AddProuductData.sharedInstance?.lng = lng
            }
            AddProuductData.sharedInstance?.IsEdited = true
        }
        
    }
    @objc func doneButtonAction()
    {
        self.txtFld_KeySearchWord.becomeFirstResponder()
    }
    func Validations() throws
    {
        guard let title = txtFld_Title.text,  !title.isEmpty, !title.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
        {
            throw ValidationError.emptyTitle
        }
        guard let descrption  = txtFld_Descrption.text, !descrption.isEmpty, !descrption.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyDescription
        }
        guard let TCService  = txtFld_TCService.text, !TCService.isEmpty, !TCService.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyTCService
        }
        guard let genre  = txtFld_Genre.text, !genre.isEmpty, !genre.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyGenre
        }
        guard let bidType  = txtFld_BidType.text, !bidType.isEmpty, !bidType.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyBidType
        }
        guard let bidAmount  = txtFld_MinBidAmount.text, !bidAmount.isEmpty, !bidAmount.trimmingCharacters(in: .whitespaces).isEmpty, !bidAmount.replacingOccurrences(of: ".", with: "").isEmpty else
        {
            throw ValidationError.emptyBidAmount
        }
        guard let keysearchWord  = txtFld_KeySearchWord.text, !keysearchWord.isEmpty, !keysearchWord.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyKeySearchWord
        }
        guard let location  = txtFld_Location.text, !location.isEmpty, !location.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyLocation
        }
        if arrImages.count == 0
        {
            throw ValidationError.emptyImages
        }
    }
    func GetGener()  {
        let urlStr = KGenerApi + String(KServicesType)
        AddProduct.sharedManager.GetApi(url: urlStr,Target: self, completionResponse: { (Genre,BidType,maxImages) in
            //print(Genre)
            self.arrBidType = BidType
            self.arrGenre = Genre
            self.maxNumImages = maxImages
            //print(Genre)
            //print(BidType)
            //print(maxImages)
            self.CollectionView_Images.reloadData()
        }, completionnilResponse: { (Response) in
            print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    //print(resonse)
                    self.GetGener()
                })
            }
            else if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
    }
    
    @objc func DonePicker()
    {
        let row = pickerView.selectedRow(inComponent: 0)
        
        switch pickerType
        {
        case .GenrePicker?:
            let dic = arrPicker[row] as! GenreData
            let title = dic.name
            let id = dic.id
            let ID:String = String(id)
            strGenreID = ID
            txtFld_Genre.text = title
        case .BidTypePicker?:
            let dic = arrPicker[row] as! BidTypeData
            let title = dic.name
            let id = dic.id
            let ID:String = String(id)
            strBidTypeID = ID
            txtFld_BidType.text = title
        case .none:
            print("none")
        }
        txtFld_forOpeningPickers.resignFirstResponder()
    }
    @objc func CancelPicker()
    {
        txtFld_forOpeningPickers.resignFirstResponder()
    }
     func Submitstep1()
     {
        let imagescount = arrImages.count
        let videoCount = arrVideos.count
        totalCount = imagescount + videoCount
        //print(imagescount)
        //print(videoCount)
        //print(count)
            MBProgressHUD.hide(for: KappDelegate.window, animated: true)
        if (count < imagescount)
        {
             MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.count+1) + "/" + String(self.totalCount)
            //print(arrImages)
            AmazonServices.sharedInstance.UploadData(awsType: .Image, url: arrImages[count].imgUrl, completionHandler: { (error, urlStr) -> (Void) in
                //print(error ?? "Blank")
                //print(urlStr ?? "Blank")
                if error == nil
                {
                    self.arrImagesUrl.append(urlStr!)
                    self.count = self.count+1
                    DispatchQueue.main.async {
                        self.Submitstep1()
                    }            }
                else
                {
                    self.AlertMessageWithOkAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {
                        self.count = 0
                        self.arrImagesUrl.removeAll()
                        self.arrVideosUrl.removeAll()
                        //
                      //self.SubmitAction(AnyObject.self)
                    })
                }
            } , Target: self,networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
                
          
              
        }
        else if(count < imagescount+videoCount)
        {
             MBProgressHUD.showAdded(to: KappDelegate.window, animated: true).labelText = KUploading + String(self.count+1) + "/" + String(self.totalCount)
            //print(arrVideos)
            AmazonServices.sharedInstance.UploadData(awsType: .Video, url: arrVideos[count-imagescount].videoUrl, completionHandler: { (error, urlStr) -> (Void) in
                //print(error ?? "Blank")
                //print(urlStr ?? "Blank")
                if error == nil
                {
                    self.arrVideosUrl.append(urlStr!)
                
                self.count = self.count+1
                DispatchQueue.main.async {
                    self.Submitstep1()
                }
                }
                else
                {
                    self.AlertMessageWithOkAction(titleStr: KMessage, messageStr:KErroroccuredwhileuploadingPleasetryagain, Target: self, completionResponse: {
                        self.count = 0
                        self.arrImagesUrl.removeAll()
                        self.arrVideosUrl.removeAll()
                        //self.SubmitAction(AnyObject.self)
                    })
                }
                
            }, Target: self,networkError: {(error) in
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        }
        else
        {
            //print(arrImagesUrl)
            //print(arrVideosUrl)
            Submitstep2()
        }
    }
    
    func Submitstep2()
    {
        if let title =  txtFld_Title.text
        {
            AddProuductData.sharedInstance?.title = title
        }
        if let description = txtFld_Descrption.text
        {
            AddProuductData.sharedInstance?.description = description
        }
        if let id = UserDefaults.standard.getUserID()
        {
            AddProuductData.sharedInstance?.id = id
        }
        if let bidTYpe = txtFld_BidType.text
        {
            AddProuductData.sharedInstance?.minBidType = bidTYpe
        }
        if let bidTypeID = strBidTypeID, bidTypeID != ""
        {
            AddProuductData.sharedInstance?.minBidTypeId = Int(bidTypeID)
        }
        if let serviceType = txtFld_Genre.text
        {
            AddProuductData.sharedInstance?.serviceType = serviceType
        }
//        if let GenreID = strGenreID
//        {
//            AddProuductData.sharedInstance?.genreId = Int(GenreID)
//        }
        if let minBid = txtFld_MinBidAmount.text, minBid != ""
        {
            print(minBid)
            print(Float(minBid) as Any)
            print(Double(minBid) as Any)
            AddProuductData.sharedInstance?.minBidAmount = Double(minBid)
            print(Double(minBid))
            print(AddProuductData.sharedInstance?.minBidAmount)
        }
        if let toc = txtFld_TCService.text, toc != ""
        {
            AddProuductData.sharedInstance?.TC = toc
        }
        if arrImages.count>0
        {
            AddProuductData.sharedInstance?.arrImages = arrImages
        }
        if let keywords = txtFld_KeySearchWord.text, !keywords.isEmpty
        {
            AddProuductData.sharedInstance?.keySearchWord = keywords
        }
        if arrVideos.count>0
        {
            AddProuductData.sharedInstance?.arrVideos = arrVideos
        }
        else
        {
            let blank = [VideoData]()
            AddProuductData.sharedInstance?.arrVideos = blank
        }
        if let location = txtFld_Location.text
        {
            AddProuductData.sharedInstance?.location = location
        }
        if let minBidType = txtFld_BidType.text
        {
            AddProuductData.sharedInstance?.minBidType = minBidType
        }
        print(arrRemoveMedia)
        AddProuductData.sharedInstance?.arrRemoveMedia = arrRemoveMedia
        //AddProuductData.sharedInstance?.is_escrow = is_escrow
        //Gagan
         AddProuductData.sharedInstance?.is_escrow = false
        AddProuductData.sharedInstance?.itemType = KServicesType
        KCommonFunctions.PushToContrller(from: self, ToController: .Preview, Data: nil)
    }
    //MARK: - IBActions
    
    //For Open Blockchain link
    @IBAction func onClickOpenLink(_ sender: UIButton) {
        setDataForWebPageViewController()
    }
    //Below is Inaction for picking music from media library
    @IBAction func YesDeleteAction(_ sender: Any) {
        
        switch deleteItemType {
        case .Song?:
           print("song")
        case .Video?:
            let index = DeletedIndex
            self.arrRemoveMedia.append((arrVideos[index].videoUrl?.absoluteString)!)
            print(arrRemoveMedia)
            print(self.arrVideos)
            print(self.arrVideosUrl)
            self.arrVideos.remove(at: index)
            print(self.arrVideos)
            self.CollectionView_Video.reloadData()
        case .Image?:
            let index = DeletedIndex
            print(index)
            if index < arrImages.count
            {
                self.arrRemoveMedia.append((arrImages[index].imgUrl?.absoluteString)!)
                print(arrRemoveMedia)
                print(arrImages)
                self.arrImages.remove(at: index)
                print(arrImages)
                self.CollectionView_Images.reloadData()
            }
        default:
            print("Fail")
        }
        
        ViewDelete.isHidden = true
    }
    @IBAction func NoDeleteAction(_ sender: Any) {
        ViewDelete.isHidden = true
    }
    @IBAction func MapAction(_ sender: Any)
    {
//        //        mapView.isHidden = false
//        //        imgView_Pin.isHidden = false
//        //        btnSave.title = "SAVE"
//        //        btnSave.isEnabled = true
//        let config = GMSPlacePickerConfig(viewport: nil)
//        let placePicker = GMSPlacePickerViewController(config: config)
//        placePicker.delegate = self
//        present(placePicker, animated: true, completion: nil)
//
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchAddressVC") as! SearchAddressVC
        vc.locationDelegate = self;
        self.navigationController?.pushViewController(vc, animated: true)
        //self.present(vc, animated: true, completion: nil)
    }
    //protocol of location 
    func Location(lat: String, lng: String, address: String) {
        print("protcol address")
        print(address)
        print(lat)
        print(lng)
        
        txtFld_Location.text = address
        AddProuductData.sharedInstance?.lat = Float(lat)
        //Float(place.coordinate.latitude)
        AddProuductData.sharedInstance?.lng = Float(lng)
    }
    
    
    @IBAction func YesAction(_ sender: Any) {
        is_escrow = true
        if let image  = UIImage(named: Kselected)
        {
            btnYes.setImage(image, for: .normal)
        }
        if let image  = UIImage(named: Kunselected)
        {
            btnNo.setImage(image, for: .normal)
        }
    }
    @IBAction func NoAction(_ sender: Any) {
        is_escrow = false
        if let image  = UIImage(named: Kselected)
        {
            btnNo.setImage(image, for: .normal)
        }
        if let image  = UIImage(named: Kunselected)
        {
            btnYes.setImage(image, for: .normal)
        }
    }

    @IBAction func ScrollCollectionAction(_ sender: Any)
    {
        //print((sender as AnyObject).tag)
        
        if (sender as AnyObject).tag == 0
        {
            let visibleItems: NSArray = self.CollectionView_Images.indexPathsForVisibleItems as NSArray
            let minIndex = visibleItems.map { ($0 as AnyObject).row }.min()
            let nextItem: IndexPath = IndexPath(item: minIndex! - 1, section: 0)
            //print("nextItem : \(nextItem)")
            if(nextItem.row >= 0)
            {
                //print("LeftScroll")
                self.CollectionView_Images.scrollToItem(at: nextItem, at: .right, animated: true)
            }
        }
        else if  (sender as AnyObject).tag == 1
        {
            let visibleItems: NSArray = self.CollectionView_Images.indexPathsForVisibleItems as NSArray
            let maxIndex = visibleItems.map { ($0 as AnyObject).row }.max()
            let nextItem: IndexPath = IndexPath(item: maxIndex! + 1, section: 0)
            //print(nextItem)
            if(maxNumImages > nextItem.row)
            {
                //print("RightScroll")
                self.CollectionView_Images.scrollToItem(at: nextItem, at: .left, animated: true)
            }
        }
    }
    @IBAction func BackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func SubmitAction(_ sender: Any) {
        
       // removeImage(itemName: Filename)

        self.view.endEditing(true)
        do {
            try Validations()
            Submitstep2()
        } catch let error {
            switch  error {
            case ValidationError.emptyTitle:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseentersongtitle)
            case ValidationError.minTitleLength:
                self.showAlertMessage(titleStr: KMessage, messageStr:KTitleShouldbeof)
            case ValidationError.emptyDescription:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterdescrption)
            case ValidationError.emptyTCService:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterTCservice)
            case ValidationError.emptyGenre:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterservicetype)
            case ValidationError.emptyBidType:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterBidType)
            case ValidationError.emptyBidAmount:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterBidAmount)
            case ValidationError.emptyImages:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseselectatleastoneimage)
            case ValidationError.emptyLocation:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterlocation)
            case ValidationError.emptyKeySearchWord:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleaseenterkeysearchword)

            default:
                self.showAlertMessage(titleStr: KMessage, messageStr:KUnknownerror)
            }
        }
    }
    @IBAction func BidTypeAction(_ sender: Any)
    {
        if(
            arrBidType.count>0)
        {
            isGenrePicker = false
            pickerType = PickerType(rawValue: 2)
            self.arrPicker = self.arrBidType
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(txtFld_BidType.text != "")
            {
                if let value =  self.GetIndexFromArrayForString(arr: arrBidType, str: (txtFld_BidType.text)!, key: Kname,type:.BidTypeData)
                {
                    pickerView.selectRow(value, inComponent: 0, animated: false)
                }
            }
        }
    }
    @IBAction func GenerAction(_ sender: Any)
    {
        if(
            arrGenre.count>0)
        {
            isGenrePicker = true
            pickerType = PickerType(rawValue: 1)
            self.arrPicker = self.arrGenre
            self.pickerView.reloadAllComponents()
            pickerView.selectRow(0, inComponent: 0, animated: false)
            txtFld_forOpeningPickers.becomeFirstResponder()
            if(txtFld_Genre.text != "")
            {
                if let value =  self.GetIndexFromArrayForString(arr: arrGenre, str: (txtFld_Genre.text)!, key: Kname,type:.GenreData)
                {
                    pickerView.selectRow(value, inComponent: 0, animated: false)
                }
            }
        }
    }
    
    @IBAction func AddVideoAction(_ sender: Any) {
        
        if (Int(arrVideos.count) < 1)
        {
            self.OpenGallaryCameraForVideo(pickerController: imagePicker)
        }
        else
        {
            showAlertMessage(titleStr: KMessage, messageStr: "\(KYoucanuploadmaximum) \(KMaxVideo) \(Kvideo)")
        }
    }
    
    @IBAction func AddImageAction(_ sender: Any) {
        if (Int(arrImages.count) < maxNumImages)
        {
            self.OpenGallaryCamera(pickerController: imagePicker)
        }
        else
        {
            showAlertMessage(titleStr: KMessage, messageStr: "\(KYoucanuploadmaximum) \(maxNumImages) \(Kimages)")
        }
    }
    
    //MARK: - CollectionCell Delegate
    func DeleteImage(_ sender: Any) {
        DeletedIndex = (sender as AnyObject).tag
        print(DeletedIndex)
        deleteItemType = .Image
        ShowAlert()
        }
    
    
    func DeleteVideo(_ sender: Any) {
        DeletedIndex = (sender as AnyObject).tag
        print(DeletedIndex)
        deleteItemType = .Video
        ShowAlert()
    }
    
    func DeleteMedia(urlStr:String?, completionResponse:@escaping() -> Void)
    {
        var parm = [String:Any]()
        parm["filename"] = urlStr
        let urlStr = Ksellerdeletemedia
        AddProduct.sharedManager.PostApi(url: urlStr,parameter:parm ,Target: self, completionResponse: { (Response) in
            print(Response)
            completionResponse()
            //self.CollectionView_Images.reloadData()
        }, completionnilResponse: { (Response) in
            print(Response)
            let statusCode = Response[Kstatus] as! Int
            if statusCode == 500
            {
                KCommonFunctions.SessionExpired(Target: self, completionResponse: { (resonse) in
                    self.GetGener()
                })
            }
            else if let msg =  Response[Kmessage]
            {
                self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
            }
        }, completionError: { (error) in
            self.showAlertMessage(titleStr: KMessage, messageStr:KError)
        },networkError: {(error) in
            self.showAlertMessage(titleStr: KMessage, messageStr: error)
        })
        
    }
    
    //MARK: - TextField delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if(textField == txtFld_forOpeningPickers)
        {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.scrollView_main.contentOffset.y = 200
                }, completion: nil)
            }
        }
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        if(textField == txtFld_forOpeningPickers)
        {
            DispatchQueue.main.async {
                UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
                    self.scrollView_main.contentOffset.y = 0
                }, completion: nil)
            }
            
            
        }
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (string == " ") && (textField.text?.count)! == 0
        {
            return false
        }
        if textField == txtFld_Title
        {
            return textField.RestrictMaxCharacter(maxCount: 150, range: range, string: string)
        }
        if textField == txtFld_Descrption
        {
          return textField.RestrictMaxCharacter(maxCount: 1000, range: range, string: string)
        }
        if textField == txtFld_TCService
        {
            return textField.RestrictMaxCharacter(maxCount: 50, range: range, string: string)
        }
        if textField == txtFld_KeySearchWord
        {
            return textField.RestrictMaxCharacter(maxCount: 30, range: range, string: string)
        }
        if textField == txtFld_MinBidAmount
        {
            if textField.RestrictMaxCharacter(maxCount: 10, range: range, string: string)
            {
            if !((textField.text?.contains("."))! && string.contains("."))
            {
                var separator = txtFld_MinBidAmount.text?.components(separatedBy: ".")
                let count = Double((separator?.count)!)
                if count > 1
                {
                    let sepStr1 = "\(separator![1])"
                    if sepStr1.count == 2 && string != ""  {
                        return false
                    }
                }
                if let amount = Double(textField.text! + string)
                {
                    if amount > 970873.77
                    {
                        return false
                    }
                }
                return true
            }
            }
            return false
        }
        
        return true
    }
    
    
    //MARK: - UImage & Video Picker Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        // Handle a movie capture
        if mediaType == kUTTypeMovie {
            guard let videoURL = info[UIImagePickerControllerMediaURL] as? URL else
            {return}
//            SDPhotosHelper.addNewVideo(withFileUrl: videoURL, inAlbum: KAlbumName, onSuccess: { (str) in
//                print(str)
//            }) { (error) in
//                print(error.debugDescription)
//            }
            if let thumbNailImage =  getThumbnailImage(forUrl: videoURL)
            {
                ////print(thumbNailImage.imageOrientation)
                switch thumbNailImage.imageOrientation
                {
                case .up:
                    print("up")
                case .down:
                    print("down")
                case .left:
                    print("left")
                case .right:
                    print("right")
                case .upMirrored:
                    print("upMirrored")
                case .downMirrored:
                    print("downMirrored")
                case .leftMirrored:
                    print("leftMirrored")
                case .rightMirrored:
                    print("rightMirrored")
                }
                if let Data = VideoData(VideoUrl: videoURL, Image: thumbNailImage)
                {
                    //print(videoURL)
                    arrVideos.removeAll()
                    arrVideos.insert(Data, at: 0)
                }
                CollectionView_Video.reloadData()
            }
        }
            // Handle Image Capture
        else{
            
            //print(info)
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
                let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
                // choose a name for your image
                let fileName = "\(Double(Date.timeIntervalSinceReferenceDate * 1000)).jpg"
                //let str = documentsDirectory
             //   Filename = fileName
              //`  print(str)
                // create the destination file url to save your image
                let fileURL = documentsDirectory.appendingPathComponent(fileName)
                // get your UIImage jpeg data representation and check if the destination file url already exists
                if let data = UIImageJPEGRepresentation(pickedImage, 1.0),
                    !FileManager.default.fileExists(atPath: fileURL.path)
                {
                    do {
                        // writes the image data to disk
                        try data.write(to: fileURL)
                        if let Data = ImagesData(ImgUrl: fileURL, Image: pickedImage)
                        {
                            arrImages.insert(Data, at: 0)
                        }
                        //print(arrImages)
                        let indexPath = IndexPath(row: 0, section: 0)
                        self.CollectionView_Images.scrollToItem(at: indexPath, at: .left, animated: true)
                        CollectionView_Images.reloadData()
                        //print("file saved")
                    } catch
                    {
                        //print("error saving file:", error)
                    }

                }
            }
        }
        dismiss(animated:true, completion: nil)
    }
    

    @objc func video(videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        var title = "Success"
        var message = "Video was saved"
        if let _ = error {
            title = "Error"
            message = "Video failed to save"
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated:true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
extension AddSellServiceVC : GMSMapViewDelegate,GMSPlacePickerViewControllerDelegate
{
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("lat \(place.coordinate.latitude)")
        print("lng \(place.coordinate.longitude)")
        print("lng \(place.name)")
        print(place.addressComponents ?? "Blank")
        print("lng \(place.placeID)")
        if let address = place.formattedAddress
        {
            txtFld_Location.text = address
            AddProuductData.sharedInstance?.lat = Float(place.coordinate.latitude)
            AddProuductData.sharedInstance?.lng = Float(place.coordinate.longitude)
            print(AddProuductData.sharedInstance?.lat as Any)
            print(AddProuductData.sharedInstance?.lng as Any)

        }
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        print("No place selected")
    }
    func placePicker(_ viewController: GMSPlacePickerViewController, didFailWithError error: Error) {
        print("this is ur type error : \(error.localizedDescription)")
    }
    
}


extension AddSellServiceVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    //MARK: UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        switch collectionView {
        case CollectionView_Images:
            return maxNumImages
        case CollectionView_Video:
            return 1
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        case CollectionView_Images:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: KImagesCollectionCell, for: indexPath as IndexPath) as? ImagesCollectionCell
            {
                cell.delegate = self
                if(arrImages.count>indexPath.row)
                {
                    cell.LoadData(Data: arrImages, index: indexPath)
                }
                else
                {
                    cell.LoadBlankData(index: indexPath)
                }
                return cell
            }
        case CollectionView_Video:
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: KvideosCollectionCell, for: indexPath as IndexPath) as? VideosCollectionCell
            {
                cell.delegate = self
                if(arrVideos.count>indexPath.row)
                {
                    cell.LoadData(Data: arrVideos, index: indexPath)
                }
                else
                {
                    cell.LoadBlankData(index: indexPath)
                }
                return cell
            }
        default:
            print("nil")
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellsAcross: CGFloat = 3
        let spaceBetweenCells: CGFloat = 0
        let dim = (collectionView.bounds.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
        ImagesCollection_heightConstarints.constant = dim
        return CGSize(width: dim, height: dim)
    }
    
    //    func configureCell(cell: UICollectionViewCell, forItemAtIndexPath: NSIndexPath)
    //        {
    //        cell.backgroundColor = UIColor.black
    //        }
    
    //MARK: UICollectionViewDelegate
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath) {
    }
    
    func collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: IndexPath) {
    }
    
    
}
extension AddSellServiceVC: UIPickerViewDelegate,UIPickerViewDataSource
{
    //MARK: - Picker Datasource and delegate
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrPicker.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dic = arrPicker[row]
        var title = ""
        switch pickerType
        {
        case .GenrePicker?:
            title = (dic as! GenreData).name
        case .BidTypePicker?:
            title = (dic as! BidTypeData).name
        case .none:
            print("none")
        }
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
    }
}


