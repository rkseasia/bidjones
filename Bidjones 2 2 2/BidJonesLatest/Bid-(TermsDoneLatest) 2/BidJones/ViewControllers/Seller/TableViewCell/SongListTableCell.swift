//
//  SongListTableCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 5/23/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class SongListTableCell: UITableViewCell {

    @IBOutlet var title: UILabel!
    @IBOutlet var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func LoadData(songName:String)
    {
        print(songName)
        title.text = songName
    }
}
