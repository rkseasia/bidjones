//
//  BidSentCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 6/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

protocol BidSentCellDelegate: class{
    func CheckOutAction(_ sender: BidSentCell)
}
class BidSentCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet var lblBidType: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDescption: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var btnCheckout: UIButton!
    @IBOutlet weak var btnCancel: CustomButton!
    
    
    weak var delegate: BidSentCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func LoadData(dic:Any,type:CellType){
        
        if let data = dic as? ItemListData{
            print(data.price as Any)
            lblTitle.text = data.title
            lblDescption.text = data.descrption
           
            if data.bid_status_id == 1{ //show both buttons
                self.btnCancel.isUserInteractionEnabled  = true
                self.btnCancel.setTitle("Cancel Request", for: .normal)
                self.btnCheckout.isHidden = false
                self.btnCancel.isHidden  = false
            } else if data.bid_status_id == 2{//accepted
                    self.btnCheckout.isHidden = true
                    self.btnCancel.isHidden  = false
                    self.btnCancel.isEnabled  = false
                    self.btnCancel.setTitle("Accepted", for: .normal)
                   
         
            }else if data.bid_status_id == 3{//canceled
                self.btnCheckout.isHidden = true
                self.btnCancel.isHidden  = false
                self.btnCancel.isEnabled  = false
                self.btnCancel.setTitle("Cancelled", for: .normal)
               
            }
            
           let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            if let date = data.bidDate{
            if let Date = dateFormatter.date(from: date){
            dateFormatter.dateFormat = "MM/dd/yyyy"
            let strDate = dateFormatter.string(from: Date)
            self.lblDate.text = strDate
            
            }
         }
            
        }
        print(dic)
    }
    @IBAction func CheckOutAction(_ sender: Any)
    {
        delegate?.CheckOutAction(self)
    }
}
