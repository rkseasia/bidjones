//
//  CompareCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 6/13/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class CompareCell: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func LoadData(dic:Any,type:CellType)
    {
        if let data = dic as? ItemListData
        {
            imgView.contentMode = .scaleAspectFill
            imgView.clipsToBounds = true
            print(data)
            lblTitle.text = data.title
            lblPrice.text = "$" + data.price!
            imgView.image = UIImage.init(named:"sound_placeholder")
//            switch type
//            {
//            case .Music:
//                imgView.image = UIImage.init(named:"sound_placeholder")
//            case .Book:
//                imgView.image =  UIImage.init(named:"book_placeholder")
//            case .Stuff:
//                imgView.image = UIImage.init(named:"stuff_placeholder")
//            case .SellService:
//                imgView.image = UIImage.init(named:"services_placeholder")
//            }
            if let imgUrlStr = data.ImageUrl
            {
                LoadImage(urlStr:imgUrlStr)
            }
            
        }
        print(dic)
    }
    func LoadImage(urlStr:String)
    {
        print(urlStr)
        var imageCache = SDImageCache.shared().imageFromCache(forKey: urlStr)
        
        if let Image = imageCache {
            print(Image)
            print(imgView)
            imgView.image = Image
        }
        else if let Image = SDImageCache.shared().imageFromDiskCache(forKey: urlStr)
        {
            imgView.image = Image
        }
        else{
            imageCache = SDImageCache.shared().imageFromDiskCache(forKey: urlStr)
            if imageCache != nil {
                imgView.image = imageCache
            }
            else
            {
                SDWebImageManager.shared().loadImage(with: URL(string:urlStr), options: .highPriority , progress: { (receivedSize :Int, ExpectedSize :Int, url : URL?) in
                    
                    
                }, completed: { (image : UIImage?, data : Data?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    
                    DispatchQueue.main.async {
                        self.imgView.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached , completed: nil)
                    }
                })
            }
        }
    }

}
