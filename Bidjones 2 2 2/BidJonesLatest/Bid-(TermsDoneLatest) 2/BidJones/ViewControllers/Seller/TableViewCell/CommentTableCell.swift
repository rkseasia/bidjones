//
//  CommentTableCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 7/17/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit

class CommentTableCell: UITableViewCell {

    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var imgViewUser: UIImageView!
    @IBOutlet var lblComment: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
