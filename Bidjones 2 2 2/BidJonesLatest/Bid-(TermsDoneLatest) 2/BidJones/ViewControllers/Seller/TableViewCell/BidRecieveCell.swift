//
//  BidRecieveCell.swift
//  BidJones
//
//  Created by Rakesh Kumar on 6/18/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit


protocol BidRecieveCellDelegate: class{
    func PaymentRequestAction(_ sender: BidRecieveCell)
}
class BidRecieveCell: UITableViewCell {

    @IBOutlet weak var lbl_description: UILabel!
    //MARK: - Outlets
    @IBOutlet var imgView: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var btnPaymentRequest: CustomButton!
    
    @IBOutlet weak var btnAccept: CustomButton!
    @IBOutlet weak var btnReject: CustomButton!
    
    weak var delegate: BidRecieveCellDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func LoadData(dic:Any,type:CellType)
    {
        print(dic)
        if let data = dic as? ItemListData
        {
            if data.categoryID == 4
            {
                btnPaymentRequest.isHidden = false
            }
            else
            {
                btnPaymentRequest.isHidden = true
            }
            imgView.contentMode = .scaleAspectFill
            imgView.clipsToBounds = true
            print(data)
            lblTitle.text = data.title
            lbl_description.text = data.descrption!
            lblDate.text = data.bidDate
            imgView.image = UIImage.init(named:"sound_placeholder")
           if let categoryID = data.categoryID
            {
                switch categoryID
                {
                case 1:
                    imgView.image =  UIImage.init(named:"book_placeholder")
                case 2:
                    imgView.image = UIImage.init(named:"graphic_2_")
                case 3:
                    imgView.image = UIImage.init(named:"sound_placeholder")
                case 4:
                    imgView.image = UIImage.init(named:"services_placeholder")
                case 5:
                    imgView.image = UIImage.init(named:"stuff_placeholder")
                case 6:
                    imgView.image = UIImage.init(named:"video_2_")
                default:
                    imgView.image = UIImage.init(named:"logo")
                }
            }
           
            if let imgUrlStr = data.ImageUrl
            {
                LoadImage(urlStr:imgUrlStr)
            }
            
            
            if data.bid_status_id == 1{ //show both buttons
                self.btnAccept.isUserInteractionEnabled  = true
                self.btnReject.isUserInteractionEnabled  = true
                self.btnReject.setTitle("Reject", for: .normal)
                 self.btnAccept.setTitle("Accept", for: .normal)
                
            }  else if data.bid_status_id == 2{ //accepted
                self.btnAccept.isUserInteractionEnabled  = false
                self.btnAccept.setTitle("Accepted", for: .normal)
                self.btnReject.isHidden = true
                self.btnAccept.isHidden = false
                
            }
            else if data.bid_status_id == 3{//canceled
                self.btnReject.isUserInteractionEnabled  = false
                self.btnReject.setTitle("Cancelled", for: .normal)
                self.btnAccept.isHidden = true
                self.btnReject.isHidden = false
            }
        }
        print(dic)
    }
    
    func LoadImage(urlStr:String) {
        print(urlStr)
        var imageCache = SDImageCache.shared().imageFromCache(forKey: urlStr)
        
        if let Image = imageCache {
            print(Image)
            print(imgView)
            imgView.image = Image
        }
        else if let Image = SDImageCache.shared().imageFromDiskCache(forKey: urlStr){
            imgView.image = Image
        }
        else{
            imageCache = SDImageCache.shared().imageFromDiskCache(forKey: urlStr)
            if imageCache != nil {
                imgView.image = imageCache
            }
            else {
                SDWebImageManager.shared().loadImage(with: URL(string:urlStr), options: .highPriority , progress: { (receivedSize :Int, ExpectedSize :Int, url : URL?) in
                    
                    
                }, completed: { (image : UIImage?, data : Data?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    
                    DispatchQueue.main.async {
                        self.imgView.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached , completed: nil)
                    }
                })
            }
        }
    }
     
    @IBAction func PaymentRequestAction(_ sender: Any) {
        delegate?.PaymentRequestAction(self)
    }
    

}
