
  
  import UIKit
  
  protocol TradeRequetCellDelegate: class{
    func CheckOutAction(_ sender: TradeRequestCell)
  }
  class TradeRequestCell: UITableViewCell {
    
    enum TypeOfRequest
    {
      case Recieve
      case Send
    }
    
    var requestType:TypeOfRequest?
    
    //MARK: - Outlets
    @IBOutlet var lblBidType: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDescption: UILabel!
   // @IBOutlet var lblDate: UILabel!
    @IBOutlet var btnCheckout: UIButton!
    weak var delegate: TradeRequetCellDelegate?
    
    override func awakeFromNib() {
      super.awakeFromNib()
      // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)
      
      // Configure the view for the selected state
    }
    func LoadData(dic:Any,type:TypeOfRequest)
    {
      if let data = dic as? TradeRequestListData
      {
        switch type
        {
        case .Recieve:
            
            if((data.item_detail) != nil)
            {
          lblTitle.text = data.item_detail?.title
          lblDescption.text = data.item_detail?.description
            }
            else
            {
                lblTitle.text = data.free_item_detail?.title
                lblDescption.text = data.free_item_detail?.description
            }
      
          break
        case .Send:
            if((data.item_detail) != nil)
            {
          lblTitle.text = data.item_detail?.title
          lblDescption.text = data.item_detail?.description
            }
            else
            {
                lblTitle.text = data.free_item_detail?.title
                lblDescption.text = data.free_item_detail?.description
            }
       
          break
        }
        
        
      }
      print(dic)
    }
    @IBAction func CheckOutAction(_ sender: Any)
    {
      delegate?.CheckOutAction(self)
    }
}
