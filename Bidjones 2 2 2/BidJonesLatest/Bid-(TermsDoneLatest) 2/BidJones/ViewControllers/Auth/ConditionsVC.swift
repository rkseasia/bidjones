//
//  ConditionsVC.swift
//  BidJones
//
//  Created by Gagan on 3/12/19.
//  Copyright © 2019 Seasia. All rights reserved.
//

import UIKit
import WebKit
class ConditionsVC: UIViewController,UIWebViewDelegate{
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var WebView: UIWebView!
    var nav_Title: String?
    var delay = 0.0
    var openUrl:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = title
        
        if let vcs = self.navigationController?.viewControllers {
            print(vcs)
            let previousVC = vcs[vcs.count - 2]
            print(previousVC)
            if previousVC is LoginVC {
               self.title = "Stripe"
            }
            else if previousVC is VerifyOTPVC {
                 self.title = "Stripe"
            }else{
                
            }
        }
 
        WebView.delegate = self
        WebView.scrollView.bounces = false
        let Device = UIDevice.current
        WebView.frame=CGRect(x: 0, y: 65, width: 320, height: 510)
        let iosVersion = NSString(string: Device.systemVersion).doubleValue
        
        let screenSize: CGRect = UIScreen.main.bounds
        if((screenSize.width == 375.00) && (screenSize.height == 667.00))
        {
            
            WebView.frame=CGRect(x: 0, y: 65, width: 375, height: 608)
        }
          let url = URL (string: openUrl!);
       
        // DispatchQueue.main.async {
            // Set your view here, which dispatches *back* to the main queue and will not block your UI
            let requestObj = URLRequest(url: url!);
            self.WebView.loadRequest(requestObj);
           
      //  }
      
        
        // Do any additional setup after loading the view.
    }
    

    override func viewWillDisappear(_ animated: Bool) {
        
        URLCache.shared.removeAllCachedResponses()
        WebView = nil
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
   
    func webViewDidStartLoad(_ webview: UIWebView)
    {
        loader.startAnimating()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
//            self.loader.stopAnimating()
//            self.loader.isHidden=true
       // })

    }
    func webViewDidFinishLoad(_ webview: UIWebView)
    {
        loader.stopAnimating()
        loader.isHidden=true
    }
    
    
    
    
    @IBAction func backAction(_ sender: Any) {
//        if let clearURL = URL(string: "about:blank") {
//            WebView.loadRequest(URLRequest(url: clearURL))
//        }
        WebView.stopLoading()
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
