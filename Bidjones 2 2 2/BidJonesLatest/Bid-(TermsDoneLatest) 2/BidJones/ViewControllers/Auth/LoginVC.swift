//
//  LoginVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 3/8/18.
//  Copyright © 2018 Seasia. All rights reserved.
//

import UIKit
//import Crashlytics

class LoginVC: UIViewController,UITextFieldDelegate {
    
    //MARK: - Label Outlets
    //MARK: - TextField Outlets
    
    @IBOutlet var txtFld_Username: UITextField!
    @IBOutlet var txtFld_Password: UITextField!
    //MARK: - TextView Outlets
    //MARK: - UItableView Outlets
    //MARK: - UIButton Outlets
    @IBOutlet var btn_RememberMe: UIButton!
    @IBOutlet var btn_Login: CustomButton!
    @IBOutlet var btn_Register: CustomButton!
    @IBOutlet var btn_Forgot: UIButton!
    
    //MARK: - ScrollView outlets
    @IBOutlet var scrollView_Main: UIScrollView!
    var user_Id:Int?
    //MARK: - Int Variable
    //MARK: - Bool Variable
    //MARK: - String Variable
    //MARK: - Enum

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        //print(user_Id!)
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.title = KLOGIN
         let screenWidth = self.ViewWidth()
         let screenHeight = self.ViewHeight()
        print("Screen width : \(screenWidth) & Screen height : \(screenHeight)")
        if(screenHeight == 736)
        {
            btn_RememberMe.imageEdgeInsets = UIEdgeInsets(top: 17, left:17, bottom: 17, right: 17)
        }
        else if(screenHeight == 568)
        {
            btn_RememberMe.imageEdgeInsets = UIEdgeInsets(top: 14, left:14, bottom: 14, right: 14)
        }
        else if(screenHeight == 667)
        {
            scrollView_Main.isScrollEnabled = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        btn_Forgot.isUserInteractionEnabled = true
        btn_Login.isUserInteractionEnabled = true
        btn_Register.isUserInteractionEnabled = true

        let isRemember : Bool =  UserDefaults.standard.getRememberMe()
        if(isRemember)
        {
            let email = UserDefaults.standard.getEmail()
            let password = UserDefaults.standard.getPassword()
            txtFld_Username.text = email
            txtFld_Password.text = password
            btn_RememberMe.setImage(#imageLiteral(resourceName: "img_check_fill"), for: .normal)
        }
        else
        {
            btn_RememberMe.setImage(#imageLiteral(resourceName: "img_check"), for: .normal)
            txtFld_Username.text = ""
            txtFld_Password.text = ""
            self.view.endEditing(true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Class Functions
    
    func Validations() throws
    {
        guard let username = txtFld_Username.text,  !username.isEmpty, !username.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty else
        {
            throw ValidationError.emptyEmailAddress
        }
        if(!username.isEmail)
        {
            throw ValidationError.invalidEmail
        }
        guard let password  = txtFld_Password.text, !password.isEmpty, !username.trimmingCharacters(in: .whitespaces).isEmpty else
        {
            throw ValidationError.emptyPassword
        }
//        if(!password.passwordMinLength)
//        {
//            throw ValidationError.passwordMinChar
//        }
    }
    
    //MARK: - Button outlet Actions
    @IBAction func ActionForgot(_ sender: Any) {
        btn_Register.isUserInteractionEnabled = false
        btn_Login.isUserInteractionEnabled = false
        KCommonFunctions.PushToContrller(from: self, ToController: .Forgot, Data: nil)
    }
    
    @IBAction func loginAction(_ sender: Any){
        btn_Forgot.isUserInteractionEnabled = false
        btn_Register.isUserInteractionEnabled = false
        self.view.endEditing(true)
        do {
            try Validations()
            let urlStr = KLoginApi
            var parm = [String : Any]()
            parm[Kemail] = txtFld_Username.text!
            parm[Kpassword] = txtFld_Password.text!
            parm[Kdevice_token]  = KDummyDeviceTokken
            if let deviceTokken = UserDefaults.standard.getDeivceTokken()
            {
                parm[Kdevice_token]  = deviceTokken
                print("your device tokken : \(deviceTokken)")
            }
            
            parm[Kdevice_type]   = "1"
            //KDeviceType
            Auth.sharedmanagerAuth.PostApi(url: urlStr, parameter: parm, Target: self, completionResponse: { (Response) in
                print("your whole response data : \(Response)")
                // UserDefaults.standard.setLoggedIn(value: true)
                var result:String?
                let data_For_Check1 = Response as? [String:Any]
                let decide_api_array = Response[Kstatus] as? Int
                //self.is_Account = Response
                if(decide_api_array == 400){
                    result =  "user"
                }else{
                    result =  "result"
                }
                let data_For_check2 = data_For_Check1?[result!] as? [String:Any]
                print(data_For_check2)
                print(data_For_Check1)
                let is_seller = data_For_check2?["is_seller"] as! Int
                let stripe_Id = "\(data_For_check2?["stripe_account_id"])"
                self.user_Id = data_For_check2?[Kid] as? Int
                print(stripe_Id)
                
                //gurleen
                 //remove this below line
                self.go_Dashboard(Response: Response)
                /*
                if(stripe_Id.contains("<null>") && is_seller == 1)
                {
                    //                        if(is_Account == 1){
                    //
                    //                        }else if(is_Account == 0){
                    //                            print("open webpage")
                    //                            self.MakeSellerShowAlert()
                    //                        }else{
                    //                            print("Something Wrong Happend")
                    //                        }
                    
                    
                    self.MakeSellerShowAlert()
                    
                    
                    //                        var urlSting = "https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_EhG8I0NY8m90bn6cdKPnOJGR7L5VTvQ1&scope=read_write&state=\(state_Id)"
                    //                        if let url = URL(string: urlSting), UIApplication.shared.canOpenURL(url) {
                    //                            UIApplication.shared.openURL(url)
                    //                        }
                    // KCommonFunctions.PushToContrller(from: self, ToController: .Terms, Data: url)
                }else if(stripe_Id.contains("<null>") && is_seller == 0){
                    print(stripe_Id)
                    //dashboard
                    
                    self.go_Dashboard(Response: Response)
                    
                }else if(!stripe_Id.contains("<null>")){
                    //open dashboard
                    self.go_Dashboard(Response: Response)
                }
 */
                
            }, completionnilResponse: { (Response) in
                self.btn_Register.isUserInteractionEnabled = true
                self.btn_Forgot.isUserInteractionEnabled = true
                print(Response)
                let statusCode = Response[Kstatus] as! Int
                print(statusCode)
                if statusCode == 400
                {
                    
                    
                    if let msg = Response["message"] as? String
                    {
                        
                        self.showAlertMessage(titleStr: KMessage, messageStr:msg)
                    }
                    
                }
                else if let msg =  Response[Kmessage]
                {
                    self.showAlertMessage(titleStr: KMessage, messageStr:msg as! String)
                }
                
            }, completionError: { (error) in
                self.btn_Register.isUserInteractionEnabled = true
                self.btn_Forgot.isUserInteractionEnabled = true
                self.showAlertMessage(titleStr: KMessage, messageStr: KError)
            },networkError: {(error) in
                self.btn_Register.isUserInteractionEnabled = true
                self.btn_Forgot.isUserInteractionEnabled = true
                self.showAlertMessage(titleStr: KMessage, messageStr: error)
            })
        } catch let error {
            switch  error {
            case ValidationError.emptyEmailAddress:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPleasefillEmailAddress)
            case ValidationError.invalidEmail:
                self.showAlertMessage(titleStr: KMessage, messageStr:KEnvalidemailaddress)
            case ValidationError.emptyPassword:
                self.showAlertMessage(titleStr: KMessage, messageStr: KPleasefillpassword)
            case ValidationError.passwordMinChar:
                self.showAlertMessage(titleStr: KMessage, messageStr:KPasswordshouldbeof)
            default:
                self.showAlertMessage(titleStr: KMessage, messageStr:KEnvalidemailaddress)
            }
            btn_Register.isUserInteractionEnabled = true
            btn_Forgot.isUserInteractionEnabled = true
        }
    }
    func go_Dashboard(Response:[String:Any]){
        
    if let result = Response[Kresult]
    {
       
        ProfieData.sharedInstance.StoreData(json: result as! [String:Any])
        if let userID  = (result as! [String:Any])[Kid]
            {
                UserDefaults.standard.setUserID(value: userID as! Int)
            }
        if let userType  = (result as! [String:Any])[Kis_seller]
        {
                print("userType while login :\(userType)")
                UserDefaults.standard.setUserType(value: userType as! Bool)
                print(UserDefaults.standard.getUserType())
        }
    }
    if let accessTokken  = Response[Kaccess_token]
    {
        UserDefaults.standard.setAccessTokken(value: accessTokken as! String)
        print(UserDefaults.standard.getAccessTokken() ?? "Blank")
    }
    if let refresh_token = Response[Krefresh_token]
    {
        UserDefaults.standard.setRefreshTokken(value: refresh_token as! String)
        print(UserDefaults.standard.getRefreshTokken() ?? "Blank")
    }
        self.btn_Register.isUserInteractionEnabled = true
        self.btn_Forgot.isUserInteractionEnabled = true
        UserDefaults.standard.setLoggedIn(value: true)
    if self.btn_RememberMe.currentImage == #imageLiteral(resourceName: "img_check_fill")
    {
        UserDefaults.standard.setEmail(value: self.txtFld_Username.text!)
        UserDefaults.standard.setPassword(value:self.txtFld_Password.text!)
        UserDefaults.standard.setRememberMe(value: true)
    }
    else
    {
        UserDefaults.standard.setRememberMe(value: false)
        UserDefaults.standard.setPassword(value:self.txtFld_Password.text!)
    }
        KCommonFunctions.SetRootViewController(rootVC:.HomeNavigation)
    }
    @IBAction func ActionRememberMe(_ sender: Any)
    {
        if btn_RememberMe.currentImage == #imageLiteral(resourceName: "img_check")
        {
            btn_RememberMe.setImage(#imageLiteral(resourceName: "img_check_fill"), for: .normal)
        }
        else
        {
            btn_RememberMe.setImage(#imageLiteral(resourceName: "img_check"), for: .normal)
        }
    }
    @IBAction func ActionRegisterButton(_ sender: Any)
    {        btn_Forgot.isUserInteractionEnabled = false
        btn_Login.isUserInteractionEnabled = false
        KCommonFunctions.PushToContrller(from: self, ToController: .Register, Data: nil)
        self.view.endEditing(true)
    }
    @IBAction func ActionRadioButton(_ sender: Any)
    {
    }
    //MARK: - TextField delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool
    {
        return true;

    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        // Do not add a line break
        return true; 
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        
        if (string == " " || string == "  ") &&  range.location == 0
        {
            return false
        }
        if textField == txtFld_Username
        {
            return textField.RestrictMaxCharacter(maxCount: 100, range: range, string: string)
        }
        if textField == txtFld_Password
        {
            return textField.RestrictMaxCharacter(maxCount: 20, range: range, string: string)
        }
        return true
    }
    
    
    
    
    func MakeSellerShowAlert()
    {
        let alertController = UIAlertController(title: KMessage, message: "You must have a stripe account linked with bidjones.Tap Continue to Create.", preferredStyle: .alert)
        
        // Create the actions
        let NoAction = UIAlertAction(title: "Continue", style: UIAlertActionStyle.default) {
            UIAlertAction in
            var urlSting = "https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_EhG8I0NY8m90bn6cdKPnOJGR7L5VTvQ1&scope=read_write&state=\(self.user_Id!)"
            print()
            if let url = URL(string: urlSting), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.openURL(url)
            }
            NSLog("OK Pressed")
        }
        let YesAction = UIAlertAction(title: "Abort", style: UIAlertActionStyle.default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        self.dismiss(animated: true, completion: nil)
        alertController.addAction(NoAction)
        alertController.addAction(YesAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
   
    func AlertWithAction(message: String)  {
        let alertController = UIAlertController(title: KMessage, message: message, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: KOK, style: UIAlertActionStyle.default) {
            UIAlertAction in
            KCommonFunctions.SetRootViewController(rootVC:.HomeNavigation)
            NSLog("OK Pressed")
        }
        //        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
        //            UIAlertAction in
        //            NSLog("Cancel Pressed")
        //        }
        
        // Add the actions
        alertController.addAction(okAction)
        //alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
}



