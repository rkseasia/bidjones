//
//  WebViewVC.swift
//  BidJones
//
//  Created by Rakesh Kumar on 6/10/19.
//  Copyright © 2019 Seasia. All rights reserved.
//

import UIKit
import WebKit

var fromRegister:Bool?


class WebViewVC: UIViewController,WKNavigationDelegate {

    // MARK: - Outlets
    @IBOutlet var webView: WKWebView!
    var UserID:Int?
    var accountCreated:Bool?

    var delegate: CustomStripeDelegate?
    override func viewDidLoad()
    {
        super.viewDidLoad()
        webView.navigationDelegate = self
       // self.navigationController?.navigationBar.isHidden = true
        accountCreated = false
        let urlSting = "https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=ca_EhG8I0NY8m90bn6cdKPnOJGR7L5VTvQ1&scope=read_write&state=\(self.UserID!)"
        let url = URL(string: urlSting)!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
    }
    
    //MARK:- WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
        MBProgressHUD.showAdded(to: KappDelegate.window, animated: true)

    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        MBProgressHUD.hide(for: KappDelegate.window, animated: true)

        
        if(accountCreated == true)
        {
            accountCreated = false
            
            if((delegate) != nil)
            {
                delegate?.StripeAccountCreated()
            }
            
            if(fromRegister == true)
            {
                fromRegister = false
                 self.AlertWithNavigatonPurpose(message: KUserregisteredsuccessfully, navigationType: .root, ViewController: .none, rootViewController: .LoginNavigation,Data:nil)
           
            }
            else
            {
                AlertWithNavigatonPurpose(message: "Your stripe account has been successfully created.", navigationType: .pop, ViewController: .none, rootViewController: .none, Data: nil)
                
            }
            
//            self.AlertWithNavigatonPurpose(message: "Your stripe account has been successfully created.", navigationType: .root, ViewController: .none, rootViewController: .LoginNavigation,Data:nil)
        }
        print("finish to load")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        print(navigationAction.request.url?.host ?? "")
        print(navigationAction.request)
        print(navigationAction.request.url ?? "")


        if let host = navigationAction.request.url?.host {
            if host.contains("dashboard.stripe.com") {
                decisionHandler(.allow)
                return
            }
        }
        if let host = navigationAction.request.url?.host {
            if host.contains("web-application-8ca8d.firebaseapp.com") {
                decisionHandler(.allow)
                accountCreated = true
                return
            }
        }
        
        
        decisionHandler(.allow)
    }
    
    @IBAction func BackAction(_ sender: UIBarButtonItem) {
        delegate?.StripeAccountNotCreated()

        self.navigationController?.popViewController(animated: true);
    }
    
   

}
